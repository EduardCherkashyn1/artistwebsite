<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ArtistRepository")
 */
class Artist implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="artist", cascade={"all"})
     * @Assert\Valid()
     */
    private $videoLinks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Photo", mappedBy="artist", cascade={"all"})
     * @Assert\Valid()
     */
    private $photos;

    /**
     *
     * @Assert\Length(min="3", max="50")
     * @Assert\NotBlank(
     *     message="The name is required")
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $stage_name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $biography;

    /**
     * @ORM\Column(type="boolean")
     */
    private $approved;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @Assert\NotBlank
     * @Assert\Email()
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $number;

    public function __construct()
    {
        $this->videoLinks = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id): ?int
    {
        return $this->id = $id;
    }
    

    /**
     * @return Collection|Link[]
     */
    public function getVideoLinks(): Collection
    {
        return $this->videoLinks;
    }

    public function addVideoLink(Link $videoLink): self
    {
        if (!$this->videoLinks->contains($videoLink)) {
            $this->videoLinks[] = $videoLink;
            $videoLink->setArtist($this);
        }

        return $this;
    }

    public function removeVideoLink(Link $videoLink): self
    {
        if ($this->videoLinks->contains($videoLink)) {
            $this->videoLinks->removeElement($videoLink);
            // set the owning side to null (unless already changed)
            if ($videoLink->getArtist() === $this) {
                $videoLink->setArtist(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setArtist($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            // set the owning side to null (unless already changed)
            if ($photo->getArtist() === $this) {
                $photo->setArtist(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getStageName(): ?string
    {
        return $this->stage_name;
    }

    public function setStageName(string $stage_name): self
    {
        $this->stage_name = $stage_name;

        return $this;
    }

    public function getBiography(): ?string
    {
        return $this->biography;
    }

    public function setBiography(?string $biography): self
    {
        $this->biography = $biography;

        return $this;
    }

    public function getApproved(): ?bool
    {
        return $this->approved;
    }

    public function setApproved(bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'email' => $this->getEmail(),
            'number' => $this->getNumber(),
            'photos' => $this->getPhotos(),
            'videoLinks' => $this->getVideoLinks(),
            'biography' => $this->getBiography(),
            'name' => $this->getName(),
            'country' => $this->getCountry(),
            'stage_name' => $this->getStageName(),
            'approved' => $this->getApproved(),
            'type' => $this->getType()
        ];
    }
}
