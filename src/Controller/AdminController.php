<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Entity\Photo;
use App\Form\Artist1Type;
use App\Service\FileUploader;
use App\Service\LinkService;
use App\Service\PredisService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="artist_index", methods={"GET"})
     */
    public function index(PaginatorInterface $paginator,Request $request): Response
    {
        $query = $this->getDoctrine()->getRepository(Artist::class)->findByDesc();
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('AdminController/index.html.twig', ['artists' => $pagination]);
    }

    /**
     * @Route("/{id}", name="artist_show", methods={"GET"})
     */
    public function show(Artist $artist, LinkService $linkService): Response
    {
        $links = $linkService->linkFormat($artist);
        return $this->render('AdminController/show.html.twig', [
            'artist' => $artist,
            'links' => $links
        ]);
    }

    /**
     * @Route("/{id}/edit", name="artist_edit", methods={"GET","POST"})
     */
    public function edit(Request $request,
                         Artist $artist,
                         FileUploader $fileUploader,
                         PredisService $predisService
    ): Response {
        $form = $this->createForm(Artist1Type::class, $artist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $predisService->clearCache($artist->getId());
            $files = $request->files->get('artist1')['files'];
            /** @var UploadedFile $file */
            foreach ($files as $file) {
                $fileUploader->upload($file, $artist);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('artist_show', ['id' => $artist->getId()]);
        }

        return $this->render('AdminController/edit.html.twig', [
            'artist' => $artist,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="artist_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Artist $artist): Response
    {
        if ($this->isCsrfTokenValid('delete'.$artist->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($artist);
            $entityManager->flush();
        }

        return $this->redirectToRoute('artist_index');
    }

    /**
     * @Route("/admin/fotoDelete", name="fotoDelete", methods={"POST"})
     */
    public function fotoDeleteAction(Request $request, Filesystem $filesystem, ContainerInterface $container)
    {

        if ($request->isXmlHttpRequest()){
            /**
             * @var Photo $photo
             */
        $photo = $this->getDoctrine()->getRepository(Photo::class)->findOneBy(['id' =>$request->get('entityId')]);
        $name = $photo->getPath();
        $root = $container->get('kernel')->getProjectDir();
        $filesystem->remove($root.'/public/uploads/fotos/'.$name);
        $em = $this->getDoctrine()->getManager();
        $em->remove($photo);
        $em->flush();
        }
        return new JsonResponse(['response' => true]);
    }

}
