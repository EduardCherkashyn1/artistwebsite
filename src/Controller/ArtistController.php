<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 12/10/18
 * Time: 10:13 PM
 */

namespace App\Controller;

use App\Entity\Artist;
use App\Entity\Link;
use App\Entity\Photo;
use App\Form\ArtistType;
use App\Service\FileUploader;
use App\Service\LinkService;
use App\Service\PredisService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ArtistController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function indexAction()
    {
       return $this->render('ArtistController/homePage.html.twig');
    }

    /**
     * @Route("/artists/{name}", name="show_category")
     */
    public function typeShowAction($name, PaginatorInterface $paginator, Request $request)
    {
         $query = $this->getDoctrine()->getRepository(Artist::class)->findByOneType($name);
         $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), 9);

          return $this->render('ArtistController/allArtistsOneType.html.twig',[
            'artists' => $pagination,
            'title' =>$name
          ]);
    }


    /**
     * @Route("/artist/{id}", name="artist_info")
     */
    public function infoAction(int $id, LinkService $linkService, PredisService $predisService)
    {
        $artist = $predisService->index($id);
        $links = $linkService->linkFormat($artist);
        $activeLink = array_shift($links);
        $activeFoto = $artist->getPhotos()->first();
        if($activeFoto!=null) {
            $artist->removePhoto($activeFoto);
        }
            $fotos = $artist->getPhotos();

        return $this->render('ArtistController/artistInfo.html.twig',[
            'artist' => $artist,
            'links' => $links,
            'activeLink' => $activeLink,
            'fotos' => $fotos,
            'activeFoto' => $activeFoto
        ]);
    }

    /**
     * @Route("/registration", name="add_artist")
     */
    public function addAction(Request $request, FileUploader $fileUploader)
    {
        $artist = new Artist();
        $link = new Link();
        $artist->addVideoLink($link);
        $photo = new Photo();
        $artist->addPhoto($photo);
        $form = $this->createForm(ArtistType::class, $artist);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $files = $request->files->get('artist')['photos'][0]['file'];
            $artist->removePhoto($photo);
            /** @var UploadedFile $file */
            foreach ($files as $file) {
                $fileUploader->upload($file, $artist);
            }
            $artist->setApproved(false);
            $em = $this->getDoctrine()->getManager();
            $em->persist($artist);
            $em->flush();
            $this->addFlash('success', 'Your data was send successfully! It will appear on the website after admin will approve it!');

            return $this->redirectToRoute('home_page');
            }

        return $this->render('ArtistController/joinUs.html.twig',[
            'show_form' => $form->createView()
        ]);
    }


    /**
     * @Route("/about", name="about")
     */
    public function aboutUsAction()
    {
        return $this->render('ArtistController/aboutUs.html.twig');
    }

    /**
     * @Route("/type/{name}", name="main_type")
     */
    public function showMainTypeAction($name)
    {
        return $this->render('ArtistController/mainType.html.twig',[
            'name' => $name
        ]);
    }

    /**
     * @Route("/contacts", name="contacts")
     */
    public function contactsAction()
    {
        return $this->render('ArtistController/contacts.html.twig');
    }

    /**
     * @Route("/show-all", name="show_all")
     */
    public function showAllAction(PaginatorInterface $paginator, Request $request)
    {
        $query = $this->getDoctrine()->getRepository(Artist::class)->findAllApproved();
        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), 9);

        return $this->render('ArtistController/allArtistsOneType.html.twig',[
            'artists' => $pagination,
            'title' => 'Artists'
        ]);
    }

}
