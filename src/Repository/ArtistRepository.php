<?php

namespace App\Repository;

use App\Entity\Artist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Artist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Artist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Artist[]    findAll()
 * @method Artist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArtistRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Artist::class);
    }

    /**
     * @return Query
     */
    public function findByDesc()
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.id', 'DESC')
            ->getQuery();
    }

    /**
     * @return Query
     */

    public function findByOneType($name)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.approved = :val')
            ->setParameter('val',true)
            ->andWhere('a.type = :val1')
            ->setParameter('val1',$name)
            ->orderBy('a.id', 'DESC')
            ->getQuery();
    }

    /**
     * @return Query
     */
    public function findAll1()
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.id', 'DESC')
            ->getQuery();
    }

    /**
     * @return Query
     */
    public function findAllApproved()
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.approved = :val')
            ->setParameter('val',true)
            ->orderBy('a.id', 'DESC')
            ->getQuery();
    }
}
