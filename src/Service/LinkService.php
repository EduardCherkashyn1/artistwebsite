<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 12/13/18
 * Time: 20:52
 */

namespace App\Service;

use RicardoFiorani\Matcher\VideoServiceMatcher;
use App\Entity\Artist;
use App\Entity\Link;

class LinkService
{
    public function linkFormat(Artist $artist): array
    {
        $vsm = new VideoServiceMatcher();
        $linksFormated = [];
        $links = $artist->getVideoLinks();
        /**
         * @var Link $link
         */
        foreach ($links as $link) {
            $video = $vsm->parse($link->getName());
            $linksFormated[] = $video->getEmbedUrl();
        }

        return $linksFormated;

    }
}
