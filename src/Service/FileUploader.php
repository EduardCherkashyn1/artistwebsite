<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 12/17/18
 * Time: 12:49
 */

namespace App\Service;

use App\Entity\Artist;
use App\Entity\Photo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, Artist $artist) :void
    {
        $photo = new Photo();
        $artist->addPhoto($photo);
        $fileName = md5(uniqid()). '.' . $file->guessExtension();
        $file->move($this->targetDirectory, $fileName);
        $photo->setPath($fileName);
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}