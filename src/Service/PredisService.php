<?php
/**
 * Created by PhpStorm.
 * User: eduardcherkashyn
 * Date: 2019-05-02
 * Time: 13:46
 */

namespace App\Service;

use App\Entity\Artist;
use Doctrine\ORM\EntityManagerInterface;
use Predis\Client;
use Symfony\Component\Serializer\SerializerInterface;

class PredisService
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * PredisService constructor.
     * @param EntityManagerInterface $manager
     * @param SerializerInterface $serializer
     * @param Client $client
     */
    public function __construct(
        EntityManagerInterface $manager,
        SerializerInterface $serializer,
        Client $client
    ) {
        $this->manager = $manager;
        $this->serializer = $serializer;
        $this->client = $client;
    }

    /**
     * @param $key
     * @return Artist
     */
    public function index(int $key): Artist
    {
         if(!$result = $this->client->get('artistId='.$key)){
             $artist = $this->manager->getRepository(Artist::class)->findOneBy(['id' => $key]);
             $data = $this->serializer->serialize($artist, 'json');
             $this->client->set('artistId='.$key, $data);
             $this->client->expire('artistId='.$key, 3600);
             $this->client->save();
         } else {
             $artist = $this->serializer->deserialize($result,Artist::class,'json');
         }
         return $artist;
    }

    /**
     * @param int $key
     */
    public function clearCache(int $key): void
    {
        $this->client->del(['artistId='.$key]);
    }
}
