<?php

namespace App\Form;

use App\Entity\Artist;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Artist1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,[
                'empty_data' => true,
            ])
            ->add('country',TextType::class,[
                'empty_data' => true,
            ])
            ->add('stage_name',TextType::class,[
                'empty_data' => true,
            ])
            ->add('number',TextType::class,[
                'empty_data' => true,
            ])
            ->add('email',EmailType::class,[
                'label' => '*Email:',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('biography',TextareaType::class,[
                'required' => false,
                'empty_data' => true,

            ])
            ->add('approved',CheckboxType::class,[
                    'required' => false,
            ])
            ->add('type',ChoiceType::class,[
                'label' => 'Select Type:',
                'choices' => [
                    'Musician' => 'Musician',
                    'Dancer' => 'Dancer',
                    'Acrobatics' => 'Acrobatics',
                    'Ballroom Couples' => 'Ballroom Couples',
                    'Specialty' => 'Specialty',
                    'Singers' => 'Singers',
                    'Juggling'=> 'Juggling',
                    'Hand-Ballancing' => 'Hand-Ballancing',
                    'Adage' => 'Adage',
                    'Aerial Acts' => 'Aerial Acts',
                    'Magic' => 'Magic',
                    'Ice Skaters'=> 'Ice Skaters',
                    'Headliners' => 'Headliners'
                ]])
            ->add('videoLinks',CollectionType::class,[
                'entry_type' => LinkType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'prototype' => true,
                'attr' => [
                'class' => 'my-selector'
            ]])
            ->add('files',FileType::class,[
                'mapped' => false,
                'label' => false,
                'multiple' => true,
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Artist::class,
            'attr' => ['novalidate' => 'novalidate'],
        ]);
    }
}
