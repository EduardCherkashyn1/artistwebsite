<?php

namespace App\Form;

use App\Entity\Artist;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtistType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,[
                'label' => '*Name:'
                ])
            ->add('country',TextType::class,[
                'required' => false,
                'label' => 'Country:'])
            ->add('stage_name',TextType::class,[
                'required' => false,
                'label' => 'Stage Name:',
                ])
            ->add('email',EmailType::class,[
                'label' => '*Email:',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('type',ChoiceType::class,[
                'label' => 'Select Type:',
                'choices' => [
                    'Ballroom Couples' => 'Ballroom Couples',
                    'Musicians' => 'Musician',
                    'Specialty' => 'Specialty',
                    'Singers' => 'Singers',
                    'Dancers' => 'Dancer',
                    'Acrobatics' => 'Acrobatics',
                    'Jugglers'=> 'Juggling',
                    'Hand Balancing' => 'Hand-Ballancing',
                    'Adagio Dancers' => 'Adage',
                    'Aerial Acts' => 'Aerial Acts',
                    'Magicians' => 'Magic',
                    'Ice Skaters'=> 'Ice Skaters',
                    'Headliners' => 'Headliners'
                ]])
            ->add('videoLinks',CollectionType::class,[
                'entry_type' => LinkType::class,
                'entry_options' => ['label' => '*Youtube,Vimeo Link:'],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'attr' => [
                    'class' => 'my-selector list-group-item'
                ],
                'by_reference' => false,
            ])
            ->add('photos',CollectionType::class,[
                'entry_type' => PhotoType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'required' => true
            ])
            ->add('biography',TextareaType::class,[
                'required' => false,
                'label' => 'Biography:'
               ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Artist::class,
            'attr' => ['novalidate' => 'novalidate'],
        ]);
    }
}
