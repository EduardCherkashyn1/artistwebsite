(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["jquery.collection"],{

/***/ "./assets/js/jquery.collection.js":
/*!****************************************!*\
  !*** ./assets/js/jquery.collection.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*
 * jquery.collection.js
 *
 * Copyright (c) 2042 alain tiemblo <alain at fuz dot org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
;

(function ($) {
  $.fn.collection = function (options) {
    var defaults = {
      container: 'body',
      allow_up: true,
      up: '<a href="#">&#x25B2;</a>',
      before_up: function before_up(collection, element) {
        return true;
      },
      after_up: function after_up(collection, element) {
        return true;
      },
      allow_down: true,
      down: '<a href="#">&#x25BC;</a>',
      before_down: function before_down(collection, element) {
        return true;
      },
      after_down: function after_down(collection, element) {
        return true;
      },
      allow_add: true,
      add: '<a href="#" class="btn btn-warning mb-3" >Add</a>',
      before_add: function before_add(collection, element) {
        return true;
      },
      after_add: function after_add(collection, element) {
        return true;
      },
      allow_remove: true,
      remove: '<a href="#" class="btn btn-danger mr-2 w-25 mb-3">Delete</a>',
      before_remove: function before_remove(collection, element) {
        return confirm('Are you sure to delete this link?');
      },
      after_remove: function after_remove(collection, element) {
        return true;
      },
      allow_duplicate: false,
      duplicate: '<a href="#">[ # ]</a>',
      before_duplicate: function before_duplicate(collection, element) {
        return true;
      },
      after_duplicate: function after_duplicate(collection, element) {
        return true;
      },
      before_init: function before_init(collection) {},
      after_init: function after_init(collection) {},
      min: 1,
      max: 4,
      add_at_the_end: true,
      prefix: 'collection',
      prototype_name: '__name__',
      name_prefix: null,
      elements_selector: '> div, > fieldset',
      elements_parent_selector: '%id%',
      children: null,
      init_with_n_elements: 0,
      hide_useless_buttons: true,
      drag_drop: true,
      drag_drop_options: {
        'placeholder': 'ui-state-highlight'
      },
      drag_drop_start: function drag_drop_start(event, ui) {
        return true;
      },
      drag_drop_update: function drag_drop_update(event, ui) {
        return true;
      },
      custom_add_location: false,
      action_container_tag: 'div',
      fade_in: true,
      fade_out: true,
      position_field_selector: null,
      preserve_names: false
    }; // used to generate random id attributes when required and missing

    var randomNumber = function randomNumber() {
      var rand = '' + Math.random() * 1000 * new Date().getTime();
      return rand.replace('.', '').split('').sort(function () {
        return 0.5 - Math.random();
      }).join('');
    }; // return an element's id, after generating one when missing


    var getOrCreateId = function getOrCreateId(prefix, obj) {
      if (!obj.attr('id')) {
        var generated_id;

        do {
          generated_id = prefix + '_' + randomNumber();
        } while ($('#' + generated_id).length > 0);

        obj.attr('id', generated_id);
      }

      return obj.attr('id');
    }; // return a field value whatever the field type


    var getFieldValue = function getFieldValue(selector) {
      try {
        var jqElem = $(selector);
      } catch (e) {
        return null;
      }

      if (jqElem.length === 0) {
        return null;
      } else if (jqElem.is('input[type="checkbox"]')) {
        return jqElem.prop('checked') === true ? true : false;
      } else if (jqElem.is('input[type="radio"]') && jqElem.attr('name') !== undefined) {
        return $('input[name="' + jqElem.attr('name') + '"]:checked').val();
      } else if (jqElem.prop('value') !== undefined) {
        return jqElem.val();
      } else {
        return jqElem.html();
      }
    }; // set a field value in accordance to the field type


    var putFieldValue = function putFieldValue(selector, value, physical) {
      try {
        var jqElem = $(selector);
      } catch (e) {
        return;
      }

      if (jqElem.length === 0) {
        return;
      } else if (jqElem.is('input[type="checkbox"]')) {
        if (value) {
          jqElem.attr('checked', true);
        } else {
          jqElem.removeAttr('checked');
        }
      } else if (jqElem.prop('value') !== undefined) {
        if (physical) {
          jqElem.attr('value', value);
        } else {
          jqElem.val(value);
        }
      } else {
        jqElem.html(value);
      }
    }; // a callback set in an event will be considered failed if it
    // returns false, null, or 0.


    var trueOrUndefined = function trueOrUndefined(value) {
      return undefined === value || value;
    }; // used to change element indexes in arbitary id attributes


    var pregQuote = function pregQuote(string) {
      return (string + '').replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
    }; // if we need to change CollectionType_field_42_value to CollectionType_field_84_value, this method
    // will change it in id="CollectionType_field_42_value", but also data-id="CollectionType_field_42_value"
    // or anywhere else just in case it could be used otherwise.


    var replaceAttrData = function replaceAttrData(elements, index, toReplace, replaceWith) {
      var replaceAttrDataNode = function replaceAttrDataNode(node) {
        var jqNode = $(node);

        if (_typeof(node) === 'object' && 'attributes' in node) {
          $.each(node.attributes, function (i, attrib) {
            if ($.type(attrib.value) === 'string') {
              jqNode.attr(attrib.name.replace(toReplace, replaceWith), attrib.value.replace(toReplace, replaceWith));
            }
          });
        }

        if (jqNode.length > 0) {
          $.each(jqNode.data(), function (name, value) {
            if ($.type(value) === 'string') {
              jqNode.data(name.replace(toReplace, replaceWith), value.replace(toReplace, replaceWith));
            }
          });
        }
      };

      var element = elements.eq(index);
      replaceAttrDataNode(element[0]);
      element.find('*').each(function () {
        replaceAttrDataNode(this);
      });
    }; // replace element names and indexes in the collection, in Symfony, names are always in format
    // CollectionType[field][42][value] and ids are in format CollectionType_field_42_value;
    // so we need to change both.


    var changeElementIndex = function changeElementIndex(collection, elements, settings, index, oldIndex, newIndex) {
      var toReplace = new RegExp(pregQuote(settings.name_prefix + '[' + oldIndex + ']'), 'g');
      var replaceWith = settings.name_prefix + '[' + newIndex + ']';

      if (settings.children) {
        $.each(settings.children, function (key, child) {
          var childCollection = collection.find(child.selector).eq(index);
          var childSettings = childCollection.data('collection-settings');

          if (childSettings) {
            childSettings.name_prefix = childSettings.name_prefix.replace(toReplace, replaceWith);
            childCollection.data('collection-settings', childSettings);
          }
        });
      }

      replaceAttrData(elements, index, toReplace, replaceWith);
      toReplace = new RegExp(pregQuote(collection.attr('id') + '_' + oldIndex), 'g');
      replaceWith = collection.attr('id') + '_' + newIndex;

      if (settings.children) {
        $.each(settings.children, function (key, child) {
          var childCollection = collection.find(child.selector).eq(index);
          var childSettings = childCollection.data('collection-settings');

          if (childSettings) {
            childSettings.elements_parent_selector = childSettings.elements_parent_selector.replace(toReplace, replaceWith);
            childSettings.elements_selector = childSettings.elements_selector.replace(toReplace, replaceWith);
            childSettings.prefix = childSettings.prefix.replace(toReplace, replaceWith);
            childCollection.data('collection-settings', childSettings);
          }
        });
      }

      replaceAttrData(elements, index, toReplace, replaceWith);
    }; // same as above, but will replace element names and indexes in an html string instead
    // of in a dom element.


    var changeHtmlIndex = function changeHtmlIndex(collection, settings, html, oldIndex, newIndex, oldKey, newKey) {
      var toReplace = new RegExp(pregQuote(settings.name_prefix + '[' + oldKey + ']'), 'g');
      var replaceWith = settings.name_prefix + '[' + newKey + ']';
      html = html.replace(toReplace, replaceWith);
      toReplace = new RegExp(pregQuote(collection.attr('id') + '_' + oldIndex), 'g');
      replaceWith = collection.attr('id') + '_' + newIndex;
      html = html.replace(toReplace, replaceWith);
      return html;
    }; // sometimes, setting a value will only be made in memory and not
    // physically in the dom; and we need the full dom when we want
    // to duplicate a field.


    var putFieldValuesInDom = function putFieldValuesInDom(element) {
      $(element).find(':input').each(function (index, inputObj) {
        putFieldValue(inputObj, getFieldValue(inputObj), true);
      });
    }; // this method does the whole magic: in a collection, if we want to
    // move elements and keep element positions in the backend, we should
    // either move element names or element contents, but not both! thus,
    // if you just move elements in the dom, you keep field names and data
    // attached and nothing will change in the backend.


    var swapElements = function swapElements(collection, elements, oldIndex, newIndex) {
      var settings = collection.data('collection-settings');

      if (!settings.position_field_selector && !settings.preserve_names) {
        changeElementIndex(collection, elements, settings, oldIndex, oldIndex, '__swap__');
        changeElementIndex(collection, elements, settings, newIndex, newIndex, oldIndex);
        changeElementIndex(collection, elements, settings, oldIndex, '__swap__', newIndex);
      }

      elements.eq(oldIndex).insertBefore(elements.eq(newIndex));

      if (newIndex > oldIndex) {
        elements.eq(newIndex).insertBefore(elements.eq(oldIndex));
      } else {
        elements.eq(newIndex).insertAfter(elements.eq(oldIndex));
      }

      return collection.find(settings.elements_selector);
    }; // moving an element down of 3 rows means increasing its index of 3, and
    // decreasing the 2 ones between of 1. Example: 0-A 1-B 2-C 3-D:
    // moving B to 3 becomes 0-A 1-C 2-D 3-B


    var swapElementsUp = function swapElementsUp(collection, elements, settings, oldIndex, newIndex) {
      for (var i = oldIndex + 1; i <= newIndex; i++) {
        elements = swapElements(collection, elements, i, i - 1);
      }

      return collection.find(settings.elements_selector);
    }; // moving an element up of 3 rows means decreasing its index of 3, and
    // increasing the 2 ones between of 1. Example: 0-A 1-B 2-C 3-D:
    // moving D to 1 becomes 0-A 1-D 2-B 3-C


    var swapElementsDown = function swapElementsDown(collection, elements, settings, oldIndex, newIndex) {
      for (var i = oldIndex - 1; i >= newIndex; i--) {
        elements = swapElements(collection, elements, i, i + 1);
      }

      return collection.find(settings.elements_selector);
    }; // if we create an element at position 2, all element indexes from 2 to N
    // should be increased. for example, in 0-A 1-B 2-C 3-D, adding X at position
    // 1 will create 0-A 1-X 2-B 3-C 4-D


    var shiftElementsUp = function shiftElementsUp(collection, elements, settings, index) {
      for (var i = index + 1; i < elements.length; i++) {
        elements = swapElements(collection, elements, i - 1, i);
      }

      return collection.find(settings.elements_selector);
    }; // if we remove an element at position 3, all element indexes from 3 to N
    // should be decreased. for example, in 0-A 1-B 2-C 3-D, removing B will create
    // 0-A 1-C 2-D


    var shiftElementsDown = function shiftElementsDown(collection, elements, settings, index) {
      for (var i = elements.length - 2; i > index; i--) {
        elements = swapElements(collection, elements, i + 1, i);
      }

      return collection.find(settings.elements_selector);
    }; // this method creates buttons for each action, according to all options set
    // (buttons enabled, minimum/maximum of elements not yet reached, rescue
    // button creation when no more elements are remaining...)


    var dumpCollectionActions = function dumpCollectionActions(collection, settings, isInitialization, event) {
      var elementsParent = $(settings.elements_parent_selector);
      var init = elementsParent.find('.' + settings.prefix + '-tmp').length === 0;
      var elements = collection.find(settings.elements_selector); // add a rescue button that will appear only if collection is emptied

      if (settings.allow_add) {
        if (init) {
          elementsParent.append('<span class="' + settings.prefix + '-tmp"></span>');

          if (settings.add) {
            elementsParent.append($(settings.add).addClass(settings.prefix + '-action ' + settings.prefix + '-rescue-add').data('collection', collection.attr('id')));
          }
        }
      } // initializes the collection with a minimal number of elements


      if (isInitialization) {
        collection.data('collection-offset', elements.length);
        var container = $(settings.container);
        var button = collection.find('.' + settings.prefix + '-add, .' + settings.prefix + '-rescue-add, .' + settings.prefix + '-duplicate').first();
        var secure = 0;

        while (elements.length < settings.init_with_n_elements) {
          secure++;
          var element = elements.length > 0 ? elements.last() : undefined;
          var index = elements.length - 1;
          elements = doAdd(container, button, collection, settings, elements, element, index, false);

          if (secure > settings.init_with_n_elements) {
            console.error('Infinite loop, element selector (' + settings.elements_selector + ') not found !');
            break;
          }
        }

        collection.data('collection-offset', elements.length);
      } // make buttons appear/disappear in each elements of the collection according to options
      // (enabled, min/max...) and logic (for example, do not put a move up button on the first
      // element of the collection)


      elements.each(function (index) {
        var element = $(this);

        if (isInitialization) {
          element.data('index', index);
        }

        var actions = element.find('.' + settings.prefix + '-actions').addBack().filter('.' + settings.prefix + '-actions');

        if (actions.length === 0) {
          actions = $('<' + settings.action_container_tag + ' class="' + settings.prefix + '-actions"></' + settings.action_container_tag + '>');
          element.append(actions);
        }

        var delta = 0;

        if (event === 'remove' && settings.fade_out) {
          delta = 1;
        }

        var buttons = [{
          'enabled': settings.allow_remove,
          'selector': settings.prefix + '-remove',
          'html': settings.remove,
          'condition': elements.length - delta > settings.min
        }, {
          'enabled': settings.allow_up,
          'selector': settings.prefix + '-up',
          'html': settings.up,
          'condition': elements.length - delta > 1 && elements.index(element) - delta > 0
        }, {
          'enabled': settings.allow_down,
          'selector': settings.prefix + '-down',
          'html': settings.down,
          'condition': elements.length - delta > 1 && elements.index(element) !== elements.length - 1
        }, {
          'enabled': settings.allow_add && !settings.add_at_the_end && !settings.custom_add_location,
          'selector': settings.prefix + '-add',
          'html': settings.add,
          'condition': elements.length - delta < settings.max
        }, {
          'enabled': settings.allow_duplicate,
          'selector': settings.prefix + '-duplicate',
          'html': settings.duplicate,
          'condition': elements.length - delta < settings.max
        }];
        $.each(buttons, function (i, button) {
          if (button.enabled) {
            var action = element.find('.' + button.selector);

            if (action.length === 0 && button.html) {
              action = $(button.html).appendTo(actions).addClass(button.selector);
            }

            if (button.condition) {
              action.removeClass(settings.prefix + '-action-disabled');

              if (settings.hide_useless_buttons) {
                action.css('display', '');
              }
            } else {
              action.addClass(settings.prefix + '-action-disabled');

              if (settings.hide_useless_buttons) {
                action.css('display', 'none');
              }
            }

            action.addClass(settings.prefix + '-action').data('collection', collection.attr('id')).data('element', getOrCreateId(collection.attr('id') + '_' + index, element));
          } else {
            element.find('.' + button.selector).css('display', 'none');
          }
        });
      }); // elements.each
      // make the rescue button appear / disappear according to options (add_at_the_end) and
      // logic (no more elements on the collection)

      if (settings.allow_add) {
        var delta = 0;

        if (event === 'remove' && settings.fade_out) {
          delta = 1;
        }

        var rescueAdd = collection.find('.' + settings.prefix + '-rescue-add').css('display', '').removeClass(settings.prefix + '-action-disabled');
        var adds = collection.find('.' + settings.prefix + '-add');

        if (!settings.add_at_the_end && adds.length > delta || settings.custom_add_location) {
          rescueAdd.css('display', 'none');
        } else if (event === 'remove' && settings.fade_out) {
          rescueAdd.css('display', 'none');
          rescueAdd.fadeIn('fast');
        }

        if (elements.length - delta >= settings.max) {
          rescueAdd.addClass(settings.prefix + '-action-disabled');

          if (settings.hide_useless_buttons) {
            collection.find('.' + settings.prefix + '-add, .' + settings.prefix + '-rescue-add, .' + settings.prefix + '-duplicate').css('display', 'none');
          }
        }
      }
    }; // dumpCollectionActions
    // this plugin supports nested collections, and this method enables them when the
    // parent collection is initialized. see
    // http://symfony-collection.fuz.org/symfony3/advanced/collectionOfCollections


    var enableChildrenCollections = function enableChildrenCollections(collection, element, settings) {
      if (settings.children) {
        $.each(settings.children, function (index, childrenSettings) {
          if (!childrenSettings.selector) {
            console.log("jquery.collection.js: given collection " + collection.attr('id') + " has children collections, but children's root selector is undefined.");
            return true;
          }

          if (element !== null) {
            element.find(childrenSettings.selector).collection(childrenSettings);
          } else {
            collection.find(childrenSettings.selector).collection(childrenSettings);
          }
        });
      }
    }; // this method handles a click on "add" buttons, it increases all following element indexes of
    // 1 position and insert a new one in the index that becomes free. if click has been made on a
    // "duplicate" button, all element values are then inserted. finally, callbacks let user cancel
    // those actions if needed.


    var doAdd = function doAdd(container, that, collection, settings, elements, element, index, isDuplicate) {
      if (elements.length < settings.max && (isDuplicate && trueOrUndefined(settings.before_duplicate(collection, element)) || trueOrUndefined(settings.before_add(collection, element)))) {
        var prototype = collection.data('prototype');
        var freeIndex = collection.data('collection-offset');
        collection.data('collection-offset', freeIndex + 1);

        if (index === -1) {
          index = elements.length - 1;
        }

        var regexp = new RegExp(pregQuote(settings.prototype_name), 'g');
        var freeKey = freeIndex;

        if (settings.preserve_names) {
          freeKey = collection.data('collection-free-key');

          if (freeKey === undefined) {
            freeKey = findFreeNumericKey(settings, elements);
          }

          collection.data('collection-free-key', freeKey + 1);
        }

        var code = $(prototype.replace(regexp, freeKey)).data('index', freeIndex);
        setRightPrefix(settings, code);
        var elementsParent = $(settings.elements_parent_selector);
        var tmp = elementsParent.find('> .' + settings.prefix + '-tmp');
        var id = $(code).find('[id]').first().attr('id');

        if (isDuplicate) {
          var oldElement = elements.eq(index);
          putFieldValuesInDom(oldElement);
          var oldHtml = $("<div/>").append(oldElement.clone()).html();
          var oldIndex = settings.preserve_names || settings.position_field_selector ? oldElement.data('index') : index;
          var oldKey = settings.preserve_names ? getElementKey(settings, oldElement) : oldIndex;
          var newHtml = changeHtmlIndex(collection, settings, oldHtml, oldIndex, freeIndex, oldKey, freeKey);
          code = $('<div/>').html(newHtml).contents().data('index', freeIndex);

          if (settings.fade_in) {
            code.hide();
          }

          tmp.before(code).find(settings.prefix + '-actions').remove();
        } else {
          if (settings.fade_in) {
            code.hide();
          }

          tmp.before(code);
        }

        elements = collection.find(settings.elements_selector);
        var action = code.find('.' + settings.prefix + '-add, .' + settings.prefix + '-duplicate');

        if (action.length > 0) {
          action.addClass(settings.prefix + '-action').data('collection', collection.attr('id'));
        }

        if (!settings.add_at_the_end && index + 1 !== freeIndex) {
          elements = doMove(collection, settings, elements, code, freeIndex, index + 1);
        } else {
          dumpCollectionActions(collection, settings, false);
        }

        enableChildrenCollections(collection, code, settings);

        if (isDuplicate && !trueOrUndefined(settings.after_duplicate(collection, code)) || !trueOrUndefined(settings.after_add(collection, code))) {
          if (index !== -1) {
            elements = shiftElementsUp(collection, elements, settings, index + 1);
          }

          code.remove();
        }
      }

      if (code !== undefined && settings.fade_in) {
        code.fadeIn('fast', function () {
          if (settings.position_field_selector) {
            doRewritePositions(settings, elements);
          }
        });
      } else {
        if (settings.position_field_selector) {
          return doRewritePositions(settings, elements);
        }
      }

      return elements;
    }; // removes the current element when clicking on a "delete" button and decrease all following
    // indexes from 1 position.


    var doDelete = function doDelete(collection, settings, elements, element, index) {
      if (elements.length > settings.min && trueOrUndefined(settings.before_remove(collection, element))) {
        var deletion = function deletion() {
          var toDelete = element;

          if (!settings.preserve_names) {
            elements = shiftElementsUp(collection, elements, settings, index);
            toDelete = elements.last();
          }

          var backup = toDelete.clone({
            withDataAndEvents: true
          }).show();
          toDelete.remove();

          if (!trueOrUndefined(settings.after_remove(collection, backup))) {
            var elementsParent = $(settings.elements_parent_selector);
            elementsParent.find('> .' + settings.prefix + '-tmp').before(backup);
            elements = collection.find(settings.elements_selector);
            elements = shiftElementsDown(collection, elements, settings, index - 1);
          }

          if (settings.position_field_selector) {
            doRewritePositions(settings, elements);
          }
        };

        if (settings.fade_out) {
          element.fadeOut('fast', function () {
            deletion();
          });
        } else {
          deletion();
        }
      }

      return elements;
    }; // reverse current element and the previous one (so the current element
    // appears one place higher)


    var doUp = function doUp(collection, settings, elements, element, index) {
      if (index !== 0 && trueOrUndefined(settings.before_up(collection, element))) {
        elements = swapElements(collection, elements, index, index - 1);

        if (!trueOrUndefined(settings.after_up(collection, element))) {
          elements = swapElements(collection, elements, index - 1, index);
        }
      }

      if (settings.position_field_selector) {
        return doRewritePositions(settings, elements);
      }

      return elements;
    }; // reverse the current element and the next one (so the current element
    // appears one place lower)


    var doDown = function doDown(collection, settings, elements, element, index) {
      if (index !== elements.length - 1 && trueOrUndefined(settings.before_down(collection, element))) {
        elements = swapElements(collection, elements, index, index + 1);

        if (!trueOrUndefined(settings.after_down(collection, elements))) {
          elements = swapElements(collection, elements, index + 1, index);
        }
      }

      if (settings.position_field_selector) {
        return doRewritePositions(settings, elements);
      }

      return elements;
    }; // move an element from a position to an arbitrary new position


    var doMove = function doMove(collection, settings, elements, element, oldIndex, newIndex) {
      if (1 === Math.abs(newIndex - oldIndex)) {
        elements = swapElements(collection, elements, oldIndex, newIndex);

        if (!(newIndex - oldIndex > 0 ? trueOrUndefined(settings.after_up(collection, element)) : trueOrUndefined(settings.after_down(collection, element)))) {
          elements = swapElements(collection, elements, newIndex, oldIndex);
        }
      } else {
        if (oldIndex < newIndex) {
          elements = swapElementsUp(collection, elements, settings, oldIndex, newIndex);

          if (!(newIndex - oldIndex > 0 ? trueOrUndefined(settings.after_up(collection, element)) : trueOrUndefined(settings.after_down(collection, element)))) {
            elements = swapElementsDown(collection, elements, settings, newIndex, oldIndex);
          }
        } else {
          elements = swapElementsDown(collection, elements, settings, oldIndex, newIndex);

          if (!(newIndex - oldIndex > 0 ? trueOrUndefined(settings.after_up(collection, element)) : trueOrUndefined(settings.after_down(collection, element)))) {
            elements = swapElementsUp(collection, elements, settings, newIndex, oldIndex);
          }
        }
      }

      dumpCollectionActions(collection, settings, false);

      if (settings.position_field_selector) {
        return doRewritePositions(settings, elements);
      }

      return elements;
    };

    var doRewritePositions = function doRewritePositions(settings, elements) {
      $(elements).each(function () {
        var element = $(this);
        putFieldValue(element.find(settings.position_field_selector), elements.index(element));
      });
      return elements;
    };

    var getElementKey = function getElementKey(settings, element) {
      var name = element.find(':input[name^="' + settings.name_prefix + '["]').attr('name');
      return name.slice(settings.name_prefix.length + 1).split(']', 1)[0];
    };

    var findFreeNumericKey = function findFreeNumericKey(settings, elements) {
      var freeKey = 0;
      elements.each(function () {
        var key = getElementKey(settings, $(this));

        if (/^0|[1-9]\d*$/.test(key) && key >= freeKey) {
          freeKey = Number(key) + 1;
        }
      });
      return freeKey;
    };

    var setRightPrefix = function setRightPrefix(settings, container) {
      var suffixes = ['-action', '-action-disabled', '-actions', '-add', '-down', '-duplicate', '-remove', '-rescue-add', '-tmp', '-up'];
      $.each(suffixes, function () {
        var suffix = this;
        container.each(function () {
          var that = $(this);

          if (that.hasClass(settings.user_prefix + suffix)) {
            that.addClass(settings.prefix + suffix);
          }

          that.find('*').each(function () {
            var here = $(this);

            if (here.hasClass(settings.user_prefix + suffix)) {
              here.addClass(settings.prefix + suffix);
            }
          });
        });
      });
    }; // we're in a $.fn., so in $('.collection').collection(), $(this) equals $('.collection')


    var elems = $(this); // at least one, but why not several collections should be raised

    if (elems.length === 0) {
      console.log("jquery.collection.js: given collection selector does not exist.");
      return false;
    }

    elems.each(function () {
      var settings = $.extend(true, {}, defaults, options); // usage of $.fn.on events using a static container just in case there would be some
      // ajax interactions inside the collection

      if ($(settings.container).length === 0) {
        console.log("jquery.collection.js: a container should exist to handle events (basically, a <body> tag).");
        return false;
      } // it is possible to use this plugin with a selector that will contain the collection id
      // in a data attribute


      var elem = $(this);

      if (elem.data('collection') !== undefined) {
        var collection = $('#' + elem.data('collection'));

        if (collection.length === 0) {
          console.log("jquery.collection.js: given collection id does not exist.");
          return true;
        }
      } else {
        collection = elem;
      }

      collection = $(collection); // when adding elements to a collection, we should be aware of the node that will contain them

      settings.elements_parent_selector = settings.elements_parent_selector.replace('%id%', '#' + getOrCreateId('', collection));

      if (!settings.elements_parent_selector) {
        settings.elements_parent_selector = '#' + getOrCreateId('', collection);

        if ($(settings.elements_parent_selector).length === 0) {
          console.log("jquery.collection.js: given elements parent selector does not return any object.");
          return true;
        }
      } // On nested collections, prefix is the same for all children leading to very
      // random and unexepcted issues, so we merge prefix with current collection id.


      settings.user_prefix = settings.prefix;
      settings.prefix = collection.attr('id') + '-' + settings.user_prefix;
      setRightPrefix(settings, collection); // enforcing logic between options

      if (!settings.allow_add) {
        settings.allow_duplicate = false;
        settings.add_at_the_end = false;
      }

      if (settings.init_with_n_elements > settings.max) {
        settings.init_with_n_elements = settings.max;
      }

      if (settings.min && (!settings.init_with_n_elements || settings.init_with_n_elements < settings.min)) {
        settings.init_with_n_elements = settings.min;
      }

      if (!settings.action_container_tag) {
        console.log("jquery.collection.js: action_container_tag needs to be set.");
        return true;
      } // user callback


      settings.before_init(collection); // prototype required to create new elements in the collection

      if (collection.data('prototype') === null) {
        console.log("jquery.collection.js: given collection field has no prototype, check that your field has the prototype option set to true.");
        return true;
      } // all the following data attributes are automatically available thanks to
      // jquery.collection.html.twig form theme


      if (collection.data('prototype-name') !== undefined) {
        settings.prototype_name = collection.data('prototype-name');
      }

      if (collection.data('allow-add') !== undefined) {
        settings.allow_add = collection.data('allow-add');
        settings.allow_duplicate = collection.data('allow-add') ? settings.allow_duplicate : false;
      }

      if (collection.data('allow-remove') !== undefined) {
        settings.allow_remove = collection.data('allow-remove');
      }

      if (collection.data('name-prefix') !== undefined) {
        settings.name_prefix = collection.data('name-prefix');
      } // prototype-name required for nested collections, where collection id prefix
      // isn't guessable (see https://github.com/symfony/symfony/issues/13837)


      if (!settings.name_prefix) {
        console.log("jquery.collection.js: the prefix used in descendant field names is mandatory, you can set it using 2 ways:");
        console.log("jquery.collection.js: - use the form theme given with this plugin source");
        console.log("jquery.collection.js: - set name_prefix option to  '{{ formView.myCollectionField.vars.full_name }}'");
        return true;
      } // if preserve_names option is set, we should enforce many options to avoid
      // having inconsistencies between the UI and the Symfony result


      if (settings.preserve_names) {
        settings.allow_up = false;
        settings.allow_down = false;
        settings.drag_drop = false;
        settings.add_at_the_end = true;
      } // drag & drop support: this is a bit more complex than pressing "up" or
      // "down" buttons because we can move elements more than one place ahead
      // or below...


      if (typeof jQuery.ui !== 'undefined' && typeof jQuery.ui.sortable !== 'undefined' && collection.data('sortable')) {
        collection.sortable('disable');
      }

      if (settings.drag_drop && settings.allow_up && settings.allow_down) {
        var oldPosition;
        var newPosition;

        if (typeof jQuery.ui === 'undefined' || typeof jQuery.ui.sortable === 'undefined') {
          settings.drag_drop = false;
        } else {
          collection.sortable($.extend(true, {}, {
            start: function start(event, ui) {
              var elements = collection.find(settings.elements_selector);
              var element = ui.item;
              var that = $(this);

              if (!trueOrUndefined(settings.drag_drop_start(event, ui, elements, element))) {
                that.sortable("cancel");
                return;
              }

              ui.placeholder.height(ui.item.height());
              ui.placeholder.width(ui.item.width());
              oldPosition = elements.index(element);
            },
            update: function update(event, ui) {
              var elements = collection.find(settings.elements_selector);
              var element = ui.item;
              var that = $(this);
              that.sortable("cancel");

              if (false === settings.drag_drop_update(event, ui, elements, element) || !(newPosition - oldPosition > 0 ? trueOrUndefined(settings.before_up(collection, element)) : trueOrUndefined(settings.before_down(collection, element)))) {
                return;
              }

              newPosition = elements.index(element);
              elements = collection.find(settings.elements_selector);
              doMove(collection, settings, elements, element, oldPosition, newPosition);
            }
          }, settings.drag_drop_options));
        }
      }

      collection.data('collection-settings', settings); // events on buttons using a "static" container so even newly
      // created/ajax downloaded buttons doesn't need further initialization

      var container = $(settings.container);
      container.off('click', '.' + settings.prefix + '-action').on('click', '.' + settings.prefix + '-action', function (e) {
        var that = $(this);
        var collection = $('#' + that.data('collection'));
        var settings = collection.data('collection-settings');

        if (undefined === settings) {
          var collection = $('#' + that.data('collection')).find('.' + that.data('collection') + '-collection');
          var settings = collection.data('collection-settings');

          if (undefined === settings) {
            throw "Can't find collection: " + that.data('collection');
          }
        }

        var elements = collection.find(settings.elements_selector);
        var element = that.data('element') ? $('#' + that.data('element')) : undefined;
        var index = element && element.length ? elements.index(element) : -1;
        var event = null;
        var isDuplicate = that.is('.' + settings.prefix + '-duplicate');

        if ((that.is('.' + settings.prefix + '-add') || that.is('.' + settings.prefix + '-rescue-add') || isDuplicate) && settings.allow_add) {
          event = 'add';
          elements = doAdd(container, that, collection, settings, elements, element, index, isDuplicate);
        }

        if (that.is('.' + settings.prefix + '-remove') && settings.allow_remove) {
          event = 'remove';
          elements = doDelete(collection, settings, elements, element, index);
        }

        if (that.is('.' + settings.prefix + '-up') && settings.allow_up) {
          event = 'up';
          elements = doUp(collection, settings, elements, element, index);
        }

        if (that.is('.' + settings.prefix + '-down') && settings.allow_down) {
          event = 'down';
          elements = doDown(collection, settings, elements, element, index);
        }

        dumpCollectionActions(collection, settings, false, event);
        e.preventDefault();
      }); // .on

      dumpCollectionActions(collection, settings, true);
      enableChildrenCollections(collection, null, settings); // if collection elements are given in the wrong order, plugin
      // must reorder them graphically

      if (settings.position_field_selector) {
        var array = [];
        var elements = collection.find(settings.elements_selector);
        elements.each(function (index) {
          var that = $(this);
          array.push({
            position: parseFloat(getFieldValue(that.find(settings.position_field_selector))),
            element: that
          });
        });

        var sorter = function sorter(a, b) {
          return a.position < b.position ? -1 : a.position > b.position ? 1 : 0;
        };

        array.sort(sorter);
        $.each(array, function (newIndex, object) {
          var ids = [];
          $(elements).each(function (index) {
            ids.push($(this).attr('id'));
          });
          var element = object.element;
          var oldIndex = $.inArray(element.attr('id'), ids);

          if (newIndex !== oldIndex) {
            elements = doMove(collection, settings, elements, element, oldIndex, newIndex);
            putFieldValue(element.find(settings.position_field_selector), elements.index(element));
          }
        });
      } // if (settings.position_field_selector) {


      settings.after_init(collection);
    }); // elem.each

    return true;
  }; // $.fn.collection

})(jQuery);

/***/ })

},[["./assets/js/jquery.collection.js","runtime"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvanF1ZXJ5LmNvbGxlY3Rpb24uanMiXSwibmFtZXMiOlsiJCIsImZuIiwiY29sbGVjdGlvbiIsIm9wdGlvbnMiLCJkZWZhdWx0cyIsImNvbnRhaW5lciIsImFsbG93X3VwIiwidXAiLCJiZWZvcmVfdXAiLCJlbGVtZW50IiwiYWZ0ZXJfdXAiLCJhbGxvd19kb3duIiwiZG93biIsImJlZm9yZV9kb3duIiwiYWZ0ZXJfZG93biIsImFsbG93X2FkZCIsImFkZCIsImJlZm9yZV9hZGQiLCJhZnRlcl9hZGQiLCJhbGxvd19yZW1vdmUiLCJyZW1vdmUiLCJiZWZvcmVfcmVtb3ZlIiwiY29uZmlybSIsImFmdGVyX3JlbW92ZSIsImFsbG93X2R1cGxpY2F0ZSIsImR1cGxpY2F0ZSIsImJlZm9yZV9kdXBsaWNhdGUiLCJhZnRlcl9kdXBsaWNhdGUiLCJiZWZvcmVfaW5pdCIsImFmdGVyX2luaXQiLCJtaW4iLCJtYXgiLCJhZGRfYXRfdGhlX2VuZCIsInByZWZpeCIsInByb3RvdHlwZV9uYW1lIiwibmFtZV9wcmVmaXgiLCJlbGVtZW50c19zZWxlY3RvciIsImVsZW1lbnRzX3BhcmVudF9zZWxlY3RvciIsImNoaWxkcmVuIiwiaW5pdF93aXRoX25fZWxlbWVudHMiLCJoaWRlX3VzZWxlc3NfYnV0dG9ucyIsImRyYWdfZHJvcCIsImRyYWdfZHJvcF9vcHRpb25zIiwiZHJhZ19kcm9wX3N0YXJ0IiwiZXZlbnQiLCJ1aSIsImRyYWdfZHJvcF91cGRhdGUiLCJjdXN0b21fYWRkX2xvY2F0aW9uIiwiYWN0aW9uX2NvbnRhaW5lcl90YWciLCJmYWRlX2luIiwiZmFkZV9vdXQiLCJwb3NpdGlvbl9maWVsZF9zZWxlY3RvciIsInByZXNlcnZlX25hbWVzIiwicmFuZG9tTnVtYmVyIiwicmFuZCIsIk1hdGgiLCJyYW5kb20iLCJEYXRlIiwiZ2V0VGltZSIsInJlcGxhY2UiLCJzcGxpdCIsInNvcnQiLCJqb2luIiwiZ2V0T3JDcmVhdGVJZCIsIm9iaiIsImF0dHIiLCJnZW5lcmF0ZWRfaWQiLCJsZW5ndGgiLCJnZXRGaWVsZFZhbHVlIiwic2VsZWN0b3IiLCJqcUVsZW0iLCJlIiwiaXMiLCJwcm9wIiwidW5kZWZpbmVkIiwidmFsIiwiaHRtbCIsInB1dEZpZWxkVmFsdWUiLCJ2YWx1ZSIsInBoeXNpY2FsIiwicmVtb3ZlQXR0ciIsInRydWVPclVuZGVmaW5lZCIsInByZWdRdW90ZSIsInN0cmluZyIsInJlcGxhY2VBdHRyRGF0YSIsImVsZW1lbnRzIiwiaW5kZXgiLCJ0b1JlcGxhY2UiLCJyZXBsYWNlV2l0aCIsInJlcGxhY2VBdHRyRGF0YU5vZGUiLCJub2RlIiwianFOb2RlIiwiZWFjaCIsImF0dHJpYnV0ZXMiLCJpIiwiYXR0cmliIiwidHlwZSIsIm5hbWUiLCJkYXRhIiwiZXEiLCJmaW5kIiwiY2hhbmdlRWxlbWVudEluZGV4Iiwic2V0dGluZ3MiLCJvbGRJbmRleCIsIm5ld0luZGV4IiwiUmVnRXhwIiwia2V5IiwiY2hpbGQiLCJjaGlsZENvbGxlY3Rpb24iLCJjaGlsZFNldHRpbmdzIiwiY2hhbmdlSHRtbEluZGV4Iiwib2xkS2V5IiwibmV3S2V5IiwicHV0RmllbGRWYWx1ZXNJbkRvbSIsImlucHV0T2JqIiwic3dhcEVsZW1lbnRzIiwiaW5zZXJ0QmVmb3JlIiwiaW5zZXJ0QWZ0ZXIiLCJzd2FwRWxlbWVudHNVcCIsInN3YXBFbGVtZW50c0Rvd24iLCJzaGlmdEVsZW1lbnRzVXAiLCJzaGlmdEVsZW1lbnRzRG93biIsImR1bXBDb2xsZWN0aW9uQWN0aW9ucyIsImlzSW5pdGlhbGl6YXRpb24iLCJlbGVtZW50c1BhcmVudCIsImluaXQiLCJhcHBlbmQiLCJhZGRDbGFzcyIsImJ1dHRvbiIsImZpcnN0Iiwic2VjdXJlIiwibGFzdCIsImRvQWRkIiwiY29uc29sZSIsImVycm9yIiwiYWN0aW9ucyIsImFkZEJhY2siLCJmaWx0ZXIiLCJkZWx0YSIsImJ1dHRvbnMiLCJlbmFibGVkIiwiYWN0aW9uIiwiYXBwZW5kVG8iLCJjb25kaXRpb24iLCJyZW1vdmVDbGFzcyIsImNzcyIsInJlc2N1ZUFkZCIsImFkZHMiLCJmYWRlSW4iLCJlbmFibGVDaGlsZHJlbkNvbGxlY3Rpb25zIiwiY2hpbGRyZW5TZXR0aW5ncyIsImxvZyIsInRoYXQiLCJpc0R1cGxpY2F0ZSIsInByb3RvdHlwZSIsImZyZWVJbmRleCIsInJlZ2V4cCIsImZyZWVLZXkiLCJmaW5kRnJlZU51bWVyaWNLZXkiLCJjb2RlIiwic2V0UmlnaHRQcmVmaXgiLCJ0bXAiLCJpZCIsIm9sZEVsZW1lbnQiLCJvbGRIdG1sIiwiY2xvbmUiLCJnZXRFbGVtZW50S2V5IiwibmV3SHRtbCIsImNvbnRlbnRzIiwiaGlkZSIsImJlZm9yZSIsImRvTW92ZSIsImRvUmV3cml0ZVBvc2l0aW9ucyIsImRvRGVsZXRlIiwiZGVsZXRpb24iLCJ0b0RlbGV0ZSIsImJhY2t1cCIsIndpdGhEYXRhQW5kRXZlbnRzIiwic2hvdyIsImZhZGVPdXQiLCJkb1VwIiwiZG9Eb3duIiwiYWJzIiwic2xpY2UiLCJ0ZXN0IiwiTnVtYmVyIiwic3VmZml4ZXMiLCJzdWZmaXgiLCJoYXNDbGFzcyIsInVzZXJfcHJlZml4IiwiaGVyZSIsImVsZW1zIiwiZXh0ZW5kIiwiZWxlbSIsImpRdWVyeSIsInNvcnRhYmxlIiwib2xkUG9zaXRpb24iLCJuZXdQb3NpdGlvbiIsInN0YXJ0IiwiaXRlbSIsInBsYWNlaG9sZGVyIiwiaGVpZ2h0Iiwid2lkdGgiLCJ1cGRhdGUiLCJvZmYiLCJvbiIsInByZXZlbnREZWZhdWx0IiwiYXJyYXkiLCJwdXNoIiwicG9zaXRpb24iLCJwYXJzZUZsb2F0Iiwic29ydGVyIiwiYSIsImIiLCJvYmplY3QiLCJpZHMiLCJpbkFycmF5Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTs7QUFDQSxDQUFDLFVBQVVBLENBQVYsRUFBYTtBQUVWQSxHQUFDLENBQUNDLEVBQUYsQ0FBS0MsVUFBTCxHQUFrQixVQUFVQyxPQUFWLEVBQW1CO0FBRWpDLFFBQUlDLFFBQVEsR0FBRztBQUNYQyxlQUFTLEVBQUUsTUFEQTtBQUVYQyxjQUFRLEVBQUUsSUFGQztBQUdYQyxRQUFFLEVBQUUsMEJBSE87QUFJWEMsZUFBUyxFQUFFLG1CQUFVTixVQUFWLEVBQXNCTyxPQUF0QixFQUErQjtBQUN0QyxlQUFPLElBQVA7QUFDSCxPQU5VO0FBT1hDLGNBQVEsRUFBRSxrQkFBVVIsVUFBVixFQUFzQk8sT0FBdEIsRUFBK0I7QUFDckMsZUFBTyxJQUFQO0FBQ0gsT0FUVTtBQVVYRSxnQkFBVSxFQUFFLElBVkQ7QUFXWEMsVUFBSSxFQUFFLDBCQVhLO0FBWVhDLGlCQUFXLEVBQUUscUJBQVVYLFVBQVYsRUFBc0JPLE9BQXRCLEVBQStCO0FBQ3hDLGVBQU8sSUFBUDtBQUNILE9BZFU7QUFlWEssZ0JBQVUsRUFBRSxvQkFBVVosVUFBVixFQUFzQk8sT0FBdEIsRUFBK0I7QUFDdkMsZUFBTyxJQUFQO0FBQ0gsT0FqQlU7QUFrQlhNLGVBQVMsRUFBRSxJQWxCQTtBQW1CWEMsU0FBRyxFQUFFLG1EQW5CTTtBQW9CWEMsZ0JBQVUsRUFBRSxvQkFBVWYsVUFBVixFQUFzQk8sT0FBdEIsRUFBK0I7QUFDdkMsZUFBTyxJQUFQO0FBQ0gsT0F0QlU7QUF1QlhTLGVBQVMsRUFBRSxtQkFBVWhCLFVBQVYsRUFBc0JPLE9BQXRCLEVBQStCO0FBQ3RDLGVBQU8sSUFBUDtBQUNILE9BekJVO0FBMEJYVSxrQkFBWSxFQUFFLElBMUJIO0FBMkJYQyxZQUFNLEVBQUUsOERBM0JHO0FBNEJYQyxtQkFBYSxFQUFFLHVCQUFVbkIsVUFBVixFQUFzQk8sT0FBdEIsRUFBK0I7QUFFMUMsZUFBT2EsT0FBTyxDQUFDLG1DQUFELENBQWQ7QUFDSCxPQS9CVTtBQWdDWEMsa0JBQVksRUFBRSxzQkFBVXJCLFVBQVYsRUFBc0JPLE9BQXRCLEVBQStCO0FBQ3pDLGVBQU8sSUFBUDtBQUNILE9BbENVO0FBbUNYZSxxQkFBZSxFQUFFLEtBbkNOO0FBb0NYQyxlQUFTLEVBQUUsdUJBcENBO0FBcUNYQyxzQkFBZ0IsRUFBRSwwQkFBVXhCLFVBQVYsRUFBc0JPLE9BQXRCLEVBQStCO0FBQzdDLGVBQU8sSUFBUDtBQUNILE9BdkNVO0FBd0NYa0IscUJBQWUsRUFBRSx5QkFBVXpCLFVBQVYsRUFBc0JPLE9BQXRCLEVBQStCO0FBQzVDLGVBQU8sSUFBUDtBQUNILE9BMUNVO0FBMkNYbUIsaUJBQVcsRUFBRSxxQkFBVTFCLFVBQVYsRUFBc0IsQ0FDbEMsQ0E1Q1U7QUE2Q1gyQixnQkFBVSxFQUFFLG9CQUFVM0IsVUFBVixFQUFzQixDQUNqQyxDQTlDVTtBQStDWDRCLFNBQUcsRUFBRSxDQS9DTTtBQWdEWEMsU0FBRyxFQUFFLENBaERNO0FBaURYQyxvQkFBYyxFQUFFLElBakRMO0FBa0RYQyxZQUFNLEVBQUUsWUFsREc7QUFtRFhDLG9CQUFjLEVBQUUsVUFuREw7QUFvRFhDLGlCQUFXLEVBQUUsSUFwREY7QUFxRFhDLHVCQUFpQixFQUFFLG1CQXJEUjtBQXNEWEMsOEJBQXdCLEVBQUUsTUF0RGY7QUF1RFhDLGNBQVEsRUFBRSxJQXZEQztBQXdEWEMsMEJBQW9CLEVBQUUsQ0F4RFg7QUF5RFhDLDBCQUFvQixFQUFFLElBekRYO0FBMERYQyxlQUFTLEVBQUUsSUExREE7QUEyRFhDLHVCQUFpQixFQUFFO0FBQ2YsdUJBQWU7QUFEQSxPQTNEUjtBQThEWEMscUJBQWUsRUFBRSx5QkFBVUMsS0FBVixFQUFpQkMsRUFBakIsRUFBcUI7QUFDbEMsZUFBTyxJQUFQO0FBQ0gsT0FoRVU7QUFpRVhDLHNCQUFnQixFQUFFLDBCQUFVRixLQUFWLEVBQWlCQyxFQUFqQixFQUFxQjtBQUNuQyxlQUFPLElBQVA7QUFDSCxPQW5FVTtBQW9FWEUseUJBQW1CLEVBQUUsS0FwRVY7QUFxRVhDLDBCQUFvQixFQUFFLEtBckVYO0FBc0VYQyxhQUFPLEVBQUUsSUF0RUU7QUF1RVhDLGNBQVEsRUFBRSxJQXZFQztBQXdFWEMsNkJBQXVCLEVBQUUsSUF4RWQ7QUF5RVhDLG9CQUFjLEVBQUU7QUF6RUwsS0FBZixDQUZpQyxDQThFakM7O0FBQ0EsUUFBSUMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsR0FBWTtBQUMzQixVQUFJQyxJQUFJLEdBQUcsS0FBS0MsSUFBSSxDQUFDQyxNQUFMLEtBQWdCLElBQWhCLEdBQXVCLElBQUlDLElBQUosR0FBV0MsT0FBWCxFQUF2QztBQUNBLGFBQU9KLElBQUksQ0FBQ0ssT0FBTCxDQUFhLEdBQWIsRUFBa0IsRUFBbEIsRUFBc0JDLEtBQXRCLENBQTRCLEVBQTVCLEVBQWdDQyxJQUFoQyxDQUFxQyxZQUFZO0FBQ3BELGVBQU8sTUFBTU4sSUFBSSxDQUFDQyxNQUFMLEVBQWI7QUFDSCxPQUZNLEVBRUpNLElBRkksQ0FFQyxFQUZELENBQVA7QUFHSCxLQUxELENBL0VpQyxDQXNGakM7OztBQUNBLFFBQUlDLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBVTlCLE1BQVYsRUFBa0IrQixHQUFsQixFQUF1QjtBQUN2QyxVQUFJLENBQUNBLEdBQUcsQ0FBQ0MsSUFBSixDQUFTLElBQVQsQ0FBTCxFQUFxQjtBQUNqQixZQUFJQyxZQUFKOztBQUNBLFdBQUc7QUFDQ0Esc0JBQVksR0FBR2pDLE1BQU0sR0FBRyxHQUFULEdBQWVvQixZQUFZLEVBQTFDO0FBQ0gsU0FGRCxRQUVTckQsQ0FBQyxDQUFDLE1BQU1rRSxZQUFQLENBQUQsQ0FBc0JDLE1BQXRCLEdBQStCLENBRnhDOztBQUdBSCxXQUFHLENBQUNDLElBQUosQ0FBUyxJQUFULEVBQWVDLFlBQWY7QUFDSDs7QUFDRCxhQUFPRixHQUFHLENBQUNDLElBQUosQ0FBUyxJQUFULENBQVA7QUFDSCxLQVRELENBdkZpQyxDQWtHakM7OztBQUNBLFFBQUlHLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBVUMsUUFBVixFQUFvQjtBQUNwQyxVQUFJO0FBQ0EsWUFBSUMsTUFBTSxHQUFHdEUsQ0FBQyxDQUFDcUUsUUFBRCxDQUFkO0FBQ0gsT0FGRCxDQUVFLE9BQU9FLENBQVAsRUFBVTtBQUNSLGVBQU8sSUFBUDtBQUNIOztBQUNELFVBQUlELE1BQU0sQ0FBQ0gsTUFBUCxLQUFrQixDQUF0QixFQUF5QjtBQUNyQixlQUFPLElBQVA7QUFDSCxPQUZELE1BRU8sSUFBSUcsTUFBTSxDQUFDRSxFQUFQLENBQVUsd0JBQVYsQ0FBSixFQUF5QztBQUM1QyxlQUFRRixNQUFNLENBQUNHLElBQVAsQ0FBWSxTQUFaLE1BQTJCLElBQTNCLEdBQWtDLElBQWxDLEdBQXlDLEtBQWpEO0FBQ0gsT0FGTSxNQUVBLElBQUlILE1BQU0sQ0FBQ0UsRUFBUCxDQUFVLHFCQUFWLEtBQW9DRixNQUFNLENBQUNMLElBQVAsQ0FBWSxNQUFaLE1BQXdCUyxTQUFoRSxFQUEyRTtBQUM5RSxlQUFPMUUsQ0FBQyxDQUFDLGlCQUFpQnNFLE1BQU0sQ0FBQ0wsSUFBUCxDQUFZLE1BQVosQ0FBakIsR0FBdUMsWUFBeEMsQ0FBRCxDQUF1RFUsR0FBdkQsRUFBUDtBQUNILE9BRk0sTUFFQSxJQUFJTCxNQUFNLENBQUNHLElBQVAsQ0FBWSxPQUFaLE1BQXlCQyxTQUE3QixFQUF3QztBQUMzQyxlQUFPSixNQUFNLENBQUNLLEdBQVAsRUFBUDtBQUNILE9BRk0sTUFFQTtBQUNILGVBQU9MLE1BQU0sQ0FBQ00sSUFBUCxFQUFQO0FBQ0g7QUFDSixLQWpCRCxDQW5HaUMsQ0FzSGpDOzs7QUFDQSxRQUFJQyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLENBQVVSLFFBQVYsRUFBb0JTLEtBQXBCLEVBQTJCQyxRQUEzQixFQUFxQztBQUNyRCxVQUFJO0FBQ0EsWUFBSVQsTUFBTSxHQUFHdEUsQ0FBQyxDQUFDcUUsUUFBRCxDQUFkO0FBQ0gsT0FGRCxDQUVFLE9BQU9FLENBQVAsRUFBVTtBQUNSO0FBQ0g7O0FBQ0QsVUFBSUQsTUFBTSxDQUFDSCxNQUFQLEtBQWtCLENBQXRCLEVBQXlCO0FBQ3JCO0FBQ0gsT0FGRCxNQUVPLElBQUlHLE1BQU0sQ0FBQ0UsRUFBUCxDQUFVLHdCQUFWLENBQUosRUFBeUM7QUFDNUMsWUFBSU0sS0FBSixFQUFXO0FBQ1BSLGdCQUFNLENBQUNMLElBQVAsQ0FBWSxTQUFaLEVBQXVCLElBQXZCO0FBQ0gsU0FGRCxNQUVPO0FBQ0hLLGdCQUFNLENBQUNVLFVBQVAsQ0FBa0IsU0FBbEI7QUFDSDtBQUNKLE9BTk0sTUFNQSxJQUFJVixNQUFNLENBQUNHLElBQVAsQ0FBWSxPQUFaLE1BQXlCQyxTQUE3QixFQUF3QztBQUMzQyxZQUFJSyxRQUFKLEVBQWM7QUFDVlQsZ0JBQU0sQ0FBQ0wsSUFBUCxDQUFZLE9BQVosRUFBcUJhLEtBQXJCO0FBQ0gsU0FGRCxNQUVPO0FBQ0hSLGdCQUFNLENBQUNLLEdBQVAsQ0FBV0csS0FBWDtBQUNIO0FBQ0osT0FOTSxNQU1BO0FBQ0hSLGNBQU0sQ0FBQ00sSUFBUCxDQUFZRSxLQUFaO0FBQ0g7QUFDSixLQXZCRCxDQXZIaUMsQ0FnSmpDO0FBQ0E7OztBQUNBLFFBQUlHLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsQ0FBVUgsS0FBVixFQUFpQjtBQUNuQyxhQUFPSixTQUFTLEtBQUtJLEtBQWQsSUFBdUJBLEtBQTlCO0FBQ0gsS0FGRCxDQWxKaUMsQ0FzSmpDOzs7QUFDQSxRQUFJSSxTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFVQyxNQUFWLEVBQWtCO0FBQzlCLGFBQU8sQ0FBQ0EsTUFBTSxHQUFHLEVBQVYsRUFBY3hCLE9BQWQsQ0FBc0Isc0JBQXRCLEVBQThDLE1BQTlDLENBQVA7QUFDSCxLQUZELENBdkppQyxDQTJKakM7QUFDQTtBQUNBOzs7QUFDQSxRQUFJeUIsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFVQyxRQUFWLEVBQW9CQyxLQUFwQixFQUEyQkMsU0FBM0IsRUFBc0NDLFdBQXRDLEVBQW1EO0FBRXJFLFVBQUlDLG1CQUFtQixHQUFHLFNBQXRCQSxtQkFBc0IsQ0FBVUMsSUFBVixFQUFnQjtBQUN0QyxZQUFJQyxNQUFNLEdBQUczRixDQUFDLENBQUMwRixJQUFELENBQWQ7O0FBQ0EsWUFBSSxRQUFPQSxJQUFQLE1BQWdCLFFBQWhCLElBQTRCLGdCQUFnQkEsSUFBaEQsRUFBc0Q7QUFDbEQxRixXQUFDLENBQUM0RixJQUFGLENBQU9GLElBQUksQ0FBQ0csVUFBWixFQUF3QixVQUFVQyxDQUFWLEVBQWFDLE1BQWIsRUFBcUI7QUFDekMsZ0JBQUkvRixDQUFDLENBQUNnRyxJQUFGLENBQU9ELE1BQU0sQ0FBQ2pCLEtBQWQsTUFBeUIsUUFBN0IsRUFBdUM7QUFDbkNhLG9CQUFNLENBQUMxQixJQUFQLENBQVk4QixNQUFNLENBQUNFLElBQVAsQ0FBWXRDLE9BQVosQ0FBb0I0QixTQUFwQixFQUErQkMsV0FBL0IsQ0FBWixFQUF5RE8sTUFBTSxDQUFDakIsS0FBUCxDQUFhbkIsT0FBYixDQUFxQjRCLFNBQXJCLEVBQWdDQyxXQUFoQyxDQUF6RDtBQUNIO0FBQ0osV0FKRDtBQUtIOztBQUNELFlBQUlHLE1BQU0sQ0FBQ3hCLE1BQVAsR0FBZ0IsQ0FBcEIsRUFBdUI7QUFDbkJuRSxXQUFDLENBQUM0RixJQUFGLENBQU9ELE1BQU0sQ0FBQ08sSUFBUCxFQUFQLEVBQXNCLFVBQVVELElBQVYsRUFBZ0JuQixLQUFoQixFQUF1QjtBQUN6QyxnQkFBSTlFLENBQUMsQ0FBQ2dHLElBQUYsQ0FBT2xCLEtBQVAsTUFBa0IsUUFBdEIsRUFBZ0M7QUFDNUJhLG9CQUFNLENBQUNPLElBQVAsQ0FBWUQsSUFBSSxDQUFDdEMsT0FBTCxDQUFhNEIsU0FBYixFQUF3QkMsV0FBeEIsQ0FBWixFQUFrRFYsS0FBSyxDQUFDbkIsT0FBTixDQUFjNEIsU0FBZCxFQUF5QkMsV0FBekIsQ0FBbEQ7QUFDSDtBQUNKLFdBSkQ7QUFLSDtBQUNKLE9BaEJEOztBQWtCQSxVQUFJL0UsT0FBTyxHQUFHNEUsUUFBUSxDQUFDYyxFQUFULENBQVliLEtBQVosQ0FBZDtBQUNBRyx5QkFBbUIsQ0FBQ2hGLE9BQU8sQ0FBQyxDQUFELENBQVIsQ0FBbkI7QUFDQUEsYUFBTyxDQUFDMkYsSUFBUixDQUFhLEdBQWIsRUFBa0JSLElBQWxCLENBQXVCLFlBQVk7QUFDL0JILDJCQUFtQixDQUFDLElBQUQsQ0FBbkI7QUFDSCxPQUZEO0FBR0gsS0F6QkQsQ0E5SmlDLENBeUxqQztBQUNBO0FBQ0E7OztBQUNBLFFBQUlZLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBVW5HLFVBQVYsRUFBc0JtRixRQUF0QixFQUFnQ2lCLFFBQWhDLEVBQTBDaEIsS0FBMUMsRUFBaURpQixRQUFqRCxFQUEyREMsUUFBM0QsRUFBcUU7QUFDMUYsVUFBSWpCLFNBQVMsR0FBRyxJQUFJa0IsTUFBSixDQUFXdkIsU0FBUyxDQUFDb0IsUUFBUSxDQUFDbkUsV0FBVCxHQUF1QixHQUF2QixHQUE2Qm9FLFFBQTdCLEdBQXdDLEdBQXpDLENBQXBCLEVBQW1FLEdBQW5FLENBQWhCO0FBQ0EsVUFBSWYsV0FBVyxHQUFHYyxRQUFRLENBQUNuRSxXQUFULEdBQXVCLEdBQXZCLEdBQTZCcUUsUUFBN0IsR0FBd0MsR0FBMUQ7O0FBRUEsVUFBSUYsUUFBUSxDQUFDaEUsUUFBYixFQUF1QjtBQUNuQnRDLFNBQUMsQ0FBQzRGLElBQUYsQ0FBT1UsUUFBUSxDQUFDaEUsUUFBaEIsRUFBMEIsVUFBVW9FLEdBQVYsRUFBZUMsS0FBZixFQUFzQjtBQUM1QyxjQUFJQyxlQUFlLEdBQUcxRyxVQUFVLENBQUNrRyxJQUFYLENBQWdCTyxLQUFLLENBQUN0QyxRQUF0QixFQUFnQzhCLEVBQWhDLENBQW1DYixLQUFuQyxDQUF0QjtBQUNBLGNBQUl1QixhQUFhLEdBQUdELGVBQWUsQ0FBQ1YsSUFBaEIsQ0FBcUIscUJBQXJCLENBQXBCOztBQUNBLGNBQUlXLGFBQUosRUFBbUI7QUFDZkEseUJBQWEsQ0FBQzFFLFdBQWQsR0FBNEIwRSxhQUFhLENBQUMxRSxXQUFkLENBQTBCd0IsT0FBMUIsQ0FBa0M0QixTQUFsQyxFQUE2Q0MsV0FBN0MsQ0FBNUI7QUFDQW9CLDJCQUFlLENBQUNWLElBQWhCLENBQXFCLHFCQUFyQixFQUE0Q1csYUFBNUM7QUFDSDtBQUNKLFNBUEQ7QUFRSDs7QUFFRHpCLHFCQUFlLENBQUNDLFFBQUQsRUFBV0MsS0FBWCxFQUFrQkMsU0FBbEIsRUFBNkJDLFdBQTdCLENBQWY7QUFFQUQsZUFBUyxHQUFHLElBQUlrQixNQUFKLENBQVd2QixTQUFTLENBQUNoRixVQUFVLENBQUMrRCxJQUFYLENBQWdCLElBQWhCLElBQXdCLEdBQXhCLEdBQThCc0MsUUFBL0IsQ0FBcEIsRUFBOEQsR0FBOUQsQ0FBWjtBQUNBZixpQkFBVyxHQUFHdEYsVUFBVSxDQUFDK0QsSUFBWCxDQUFnQixJQUFoQixJQUF3QixHQUF4QixHQUE4QnVDLFFBQTVDOztBQUVBLFVBQUlGLFFBQVEsQ0FBQ2hFLFFBQWIsRUFBdUI7QUFDbkJ0QyxTQUFDLENBQUM0RixJQUFGLENBQU9VLFFBQVEsQ0FBQ2hFLFFBQWhCLEVBQTBCLFVBQVVvRSxHQUFWLEVBQWVDLEtBQWYsRUFBc0I7QUFDNUMsY0FBSUMsZUFBZSxHQUFHMUcsVUFBVSxDQUFDa0csSUFBWCxDQUFnQk8sS0FBSyxDQUFDdEMsUUFBdEIsRUFBZ0M4QixFQUFoQyxDQUFtQ2IsS0FBbkMsQ0FBdEI7QUFDQSxjQUFJdUIsYUFBYSxHQUFHRCxlQUFlLENBQUNWLElBQWhCLENBQXFCLHFCQUFyQixDQUFwQjs7QUFDQSxjQUFJVyxhQUFKLEVBQW1CO0FBQ2ZBLHlCQUFhLENBQUN4RSx3QkFBZCxHQUF5Q3dFLGFBQWEsQ0FBQ3hFLHdCQUFkLENBQXVDc0IsT0FBdkMsQ0FBK0M0QixTQUEvQyxFQUEwREMsV0FBMUQsQ0FBekM7QUFDQXFCLHlCQUFhLENBQUN6RSxpQkFBZCxHQUFrQ3lFLGFBQWEsQ0FBQ3pFLGlCQUFkLENBQWdDdUIsT0FBaEMsQ0FBd0M0QixTQUF4QyxFQUFtREMsV0FBbkQsQ0FBbEM7QUFDQXFCLHlCQUFhLENBQUM1RSxNQUFkLEdBQXVCNEUsYUFBYSxDQUFDNUUsTUFBZCxDQUFxQjBCLE9BQXJCLENBQTZCNEIsU0FBN0IsRUFBd0NDLFdBQXhDLENBQXZCO0FBQ0FvQiwyQkFBZSxDQUFDVixJQUFoQixDQUFxQixxQkFBckIsRUFBNENXLGFBQTVDO0FBQ0g7QUFDSixTQVREO0FBVUg7O0FBRUR6QixxQkFBZSxDQUFDQyxRQUFELEVBQVdDLEtBQVgsRUFBa0JDLFNBQWxCLEVBQTZCQyxXQUE3QixDQUFmO0FBQ0gsS0FsQ0QsQ0E1TGlDLENBZ09qQztBQUNBOzs7QUFDQSxRQUFJc0IsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFVNUcsVUFBVixFQUFzQm9HLFFBQXRCLEVBQWdDMUIsSUFBaEMsRUFBc0MyQixRQUF0QyxFQUFnREMsUUFBaEQsRUFBMERPLE1BQTFELEVBQWtFQyxNQUFsRSxFQUEwRTtBQUM1RixVQUFJekIsU0FBUyxHQUFHLElBQUlrQixNQUFKLENBQVd2QixTQUFTLENBQUNvQixRQUFRLENBQUNuRSxXQUFULEdBQXVCLEdBQXZCLEdBQTZCNEUsTUFBN0IsR0FBc0MsR0FBdkMsQ0FBcEIsRUFBaUUsR0FBakUsQ0FBaEI7QUFDQSxVQUFJdkIsV0FBVyxHQUFHYyxRQUFRLENBQUNuRSxXQUFULEdBQXVCLEdBQXZCLEdBQTZCNkUsTUFBN0IsR0FBc0MsR0FBeEQ7QUFDQXBDLFVBQUksR0FBR0EsSUFBSSxDQUFDakIsT0FBTCxDQUFhNEIsU0FBYixFQUF3QkMsV0FBeEIsQ0FBUDtBQUVBRCxlQUFTLEdBQUcsSUFBSWtCLE1BQUosQ0FBV3ZCLFNBQVMsQ0FBQ2hGLFVBQVUsQ0FBQytELElBQVgsQ0FBZ0IsSUFBaEIsSUFBd0IsR0FBeEIsR0FBOEJzQyxRQUEvQixDQUFwQixFQUE4RCxHQUE5RCxDQUFaO0FBQ0FmLGlCQUFXLEdBQUd0RixVQUFVLENBQUMrRCxJQUFYLENBQWdCLElBQWhCLElBQXdCLEdBQXhCLEdBQThCdUMsUUFBNUM7QUFDQTVCLFVBQUksR0FBR0EsSUFBSSxDQUFDakIsT0FBTCxDQUFhNEIsU0FBYixFQUF3QkMsV0FBeEIsQ0FBUDtBQUVBLGFBQU9aLElBQVA7QUFDSCxLQVZELENBbE9pQyxDQThPakM7QUFDQTtBQUNBOzs7QUFDQSxRQUFJcUMsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFVeEcsT0FBVixFQUFtQjtBQUN6Q1QsT0FBQyxDQUFDUyxPQUFELENBQUQsQ0FBVzJGLElBQVgsQ0FBZ0IsUUFBaEIsRUFBMEJSLElBQTFCLENBQStCLFVBQVVOLEtBQVYsRUFBaUI0QixRQUFqQixFQUEyQjtBQUN0RHJDLHFCQUFhLENBQUNxQyxRQUFELEVBQVc5QyxhQUFhLENBQUM4QyxRQUFELENBQXhCLEVBQW9DLElBQXBDLENBQWI7QUFDSCxPQUZEO0FBR0gsS0FKRCxDQWpQaUMsQ0F1UGpDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFFBQUlDLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQVVqSCxVQUFWLEVBQXNCbUYsUUFBdEIsRUFBZ0NrQixRQUFoQyxFQUEwQ0MsUUFBMUMsRUFBb0Q7QUFFbkUsVUFBSUYsUUFBUSxHQUFHcEcsVUFBVSxDQUFDZ0csSUFBWCxDQUFnQixxQkFBaEIsQ0FBZjs7QUFFQSxVQUFJLENBQUNJLFFBQVEsQ0FBQ25ELHVCQUFWLElBQXFDLENBQUNtRCxRQUFRLENBQUNsRCxjQUFuRCxFQUFtRTtBQUMvRGlELDBCQUFrQixDQUFDbkcsVUFBRCxFQUFhbUYsUUFBYixFQUF1QmlCLFFBQXZCLEVBQWlDQyxRQUFqQyxFQUEyQ0EsUUFBM0MsRUFBcUQsVUFBckQsQ0FBbEI7QUFDQUYsMEJBQWtCLENBQUNuRyxVQUFELEVBQWFtRixRQUFiLEVBQXVCaUIsUUFBdkIsRUFBaUNFLFFBQWpDLEVBQTJDQSxRQUEzQyxFQUFxREQsUUFBckQsQ0FBbEI7QUFDQUYsMEJBQWtCLENBQUNuRyxVQUFELEVBQWFtRixRQUFiLEVBQXVCaUIsUUFBdkIsRUFBaUNDLFFBQWpDLEVBQTJDLFVBQTNDLEVBQXVEQyxRQUF2RCxDQUFsQjtBQUNIOztBQUVEbkIsY0FBUSxDQUFDYyxFQUFULENBQVlJLFFBQVosRUFBc0JhLFlBQXRCLENBQW1DL0IsUUFBUSxDQUFDYyxFQUFULENBQVlLLFFBQVosQ0FBbkM7O0FBQ0EsVUFBSUEsUUFBUSxHQUFHRCxRQUFmLEVBQXlCO0FBQ3JCbEIsZ0JBQVEsQ0FBQ2MsRUFBVCxDQUFZSyxRQUFaLEVBQXNCWSxZQUF0QixDQUFtQy9CLFFBQVEsQ0FBQ2MsRUFBVCxDQUFZSSxRQUFaLENBQW5DO0FBQ0gsT0FGRCxNQUVPO0FBQ0hsQixnQkFBUSxDQUFDYyxFQUFULENBQVlLLFFBQVosRUFBc0JhLFdBQXRCLENBQWtDaEMsUUFBUSxDQUFDYyxFQUFULENBQVlJLFFBQVosQ0FBbEM7QUFDSDs7QUFFRCxhQUFPckcsVUFBVSxDQUFDa0csSUFBWCxDQUFnQkUsUUFBUSxDQUFDbEUsaUJBQXpCLENBQVA7QUFDSCxLQWxCRCxDQTVQaUMsQ0FnUmpDO0FBQ0E7QUFDQTs7O0FBQ0EsUUFBSWtGLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBVXBILFVBQVYsRUFBc0JtRixRQUF0QixFQUFnQ2lCLFFBQWhDLEVBQTBDQyxRQUExQyxFQUFvREMsUUFBcEQsRUFBOEQ7QUFDL0UsV0FBSyxJQUFJVixDQUFDLEdBQUdTLFFBQVEsR0FBRyxDQUF4QixFQUE0QlQsQ0FBQyxJQUFJVSxRQUFqQyxFQUE0Q1YsQ0FBQyxFQUE3QyxFQUFpRDtBQUM3Q1QsZ0JBQVEsR0FBRzhCLFlBQVksQ0FBQ2pILFVBQUQsRUFBYW1GLFFBQWIsRUFBdUJTLENBQXZCLEVBQTBCQSxDQUFDLEdBQUcsQ0FBOUIsQ0FBdkI7QUFDSDs7QUFDRCxhQUFPNUYsVUFBVSxDQUFDa0csSUFBWCxDQUFnQkUsUUFBUSxDQUFDbEUsaUJBQXpCLENBQVA7QUFDSCxLQUxELENBblJpQyxDQTBSakM7QUFDQTtBQUNBOzs7QUFDQSxRQUFJbUYsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixDQUFVckgsVUFBVixFQUFzQm1GLFFBQXRCLEVBQWdDaUIsUUFBaEMsRUFBMENDLFFBQTFDLEVBQW9EQyxRQUFwRCxFQUE4RDtBQUNqRixXQUFLLElBQUlWLENBQUMsR0FBR1MsUUFBUSxHQUFHLENBQXhCLEVBQTRCVCxDQUFDLElBQUlVLFFBQWpDLEVBQTRDVixDQUFDLEVBQTdDLEVBQWlEO0FBQzdDVCxnQkFBUSxHQUFHOEIsWUFBWSxDQUFDakgsVUFBRCxFQUFhbUYsUUFBYixFQUF1QlMsQ0FBdkIsRUFBMEJBLENBQUMsR0FBRyxDQUE5QixDQUF2QjtBQUNIOztBQUNELGFBQU81RixVQUFVLENBQUNrRyxJQUFYLENBQWdCRSxRQUFRLENBQUNsRSxpQkFBekIsQ0FBUDtBQUNILEtBTEQsQ0E3UmlDLENBb1NqQztBQUNBO0FBQ0E7OztBQUNBLFFBQUlvRixlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQVV0SCxVQUFWLEVBQXNCbUYsUUFBdEIsRUFBZ0NpQixRQUFoQyxFQUEwQ2hCLEtBQTFDLEVBQWlEO0FBQ25FLFdBQUssSUFBSVEsQ0FBQyxHQUFHUixLQUFLLEdBQUcsQ0FBckIsRUFBd0JRLENBQUMsR0FBR1QsUUFBUSxDQUFDbEIsTUFBckMsRUFBNkMyQixDQUFDLEVBQTlDLEVBQWtEO0FBQzlDVCxnQkFBUSxHQUFHOEIsWUFBWSxDQUFDakgsVUFBRCxFQUFhbUYsUUFBYixFQUF1QlMsQ0FBQyxHQUFHLENBQTNCLEVBQThCQSxDQUE5QixDQUF2QjtBQUNIOztBQUNELGFBQU81RixVQUFVLENBQUNrRyxJQUFYLENBQWdCRSxRQUFRLENBQUNsRSxpQkFBekIsQ0FBUDtBQUNILEtBTEQsQ0F2U2lDLENBOFNqQztBQUNBO0FBQ0E7OztBQUNBLFFBQUlxRixpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQVV2SCxVQUFWLEVBQXNCbUYsUUFBdEIsRUFBZ0NpQixRQUFoQyxFQUEwQ2hCLEtBQTFDLEVBQWlEO0FBQ3JFLFdBQUssSUFBSVEsQ0FBQyxHQUFHVCxRQUFRLENBQUNsQixNQUFULEdBQWtCLENBQS9CLEVBQWtDMkIsQ0FBQyxHQUFHUixLQUF0QyxFQUE2Q1EsQ0FBQyxFQUE5QyxFQUFrRDtBQUM5Q1QsZ0JBQVEsR0FBRzhCLFlBQVksQ0FBQ2pILFVBQUQsRUFBYW1GLFFBQWIsRUFBdUJTLENBQUMsR0FBRyxDQUEzQixFQUE4QkEsQ0FBOUIsQ0FBdkI7QUFDSDs7QUFDRCxhQUFPNUYsVUFBVSxDQUFDa0csSUFBWCxDQUFnQkUsUUFBUSxDQUFDbEUsaUJBQXpCLENBQVA7QUFDSCxLQUxELENBalRpQyxDQXdUakM7QUFDQTtBQUNBOzs7QUFDQSxRQUFJc0YscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixDQUFVeEgsVUFBVixFQUFzQm9HLFFBQXRCLEVBQWdDcUIsZ0JBQWhDLEVBQWtEL0UsS0FBbEQsRUFBeUQ7QUFDakYsVUFBSWdGLGNBQWMsR0FBRzVILENBQUMsQ0FBQ3NHLFFBQVEsQ0FBQ2pFLHdCQUFWLENBQXRCO0FBQ0EsVUFBSXdGLElBQUksR0FBR0QsY0FBYyxDQUFDeEIsSUFBZixDQUFvQixNQUFNRSxRQUFRLENBQUNyRSxNQUFmLEdBQXdCLE1BQTVDLEVBQW9Ea0MsTUFBcEQsS0FBK0QsQ0FBMUU7QUFDQSxVQUFJa0IsUUFBUSxHQUFHbkYsVUFBVSxDQUFDa0csSUFBWCxDQUFnQkUsUUFBUSxDQUFDbEUsaUJBQXpCLENBQWYsQ0FIaUYsQ0FLakY7O0FBQ0EsVUFBSWtFLFFBQVEsQ0FBQ3ZGLFNBQWIsRUFBd0I7QUFDcEIsWUFBSThHLElBQUosRUFBVTtBQUNORCx3QkFBYyxDQUFDRSxNQUFmLENBQXNCLGtCQUFrQnhCLFFBQVEsQ0FBQ3JFLE1BQTNCLEdBQW9DLGVBQTFEOztBQUNBLGNBQUlxRSxRQUFRLENBQUN0RixHQUFiLEVBQWtCO0FBQ2Q0RywwQkFBYyxDQUFDRSxNQUFmLENBQ0k5SCxDQUFDLENBQUNzRyxRQUFRLENBQUN0RixHQUFWLENBQUQsQ0FDSytHLFFBREwsQ0FDY3pCLFFBQVEsQ0FBQ3JFLE1BQVQsR0FBa0IsVUFBbEIsR0FBK0JxRSxRQUFRLENBQUNyRSxNQUF4QyxHQUFpRCxhQUQvRCxFQUVLaUUsSUFGTCxDQUVVLFlBRlYsRUFFd0JoRyxVQUFVLENBQUMrRCxJQUFYLENBQWdCLElBQWhCLENBRnhCLENBREo7QUFLSDtBQUNKO0FBQ0osT0FqQmdGLENBbUJqRjs7O0FBQ0EsVUFBSTBELGdCQUFKLEVBQXNCO0FBQ2xCekgsa0JBQVUsQ0FBQ2dHLElBQVgsQ0FBZ0IsbUJBQWhCLEVBQXFDYixRQUFRLENBQUNsQixNQUE5QztBQUVBLFlBQUk5RCxTQUFTLEdBQUdMLENBQUMsQ0FBQ3NHLFFBQVEsQ0FBQ2pHLFNBQVYsQ0FBakI7QUFDQSxZQUFJMkgsTUFBTSxHQUFHOUgsVUFBVSxDQUFDa0csSUFBWCxDQUFnQixNQUFNRSxRQUFRLENBQUNyRSxNQUFmLEdBQXdCLFNBQXhCLEdBQW9DcUUsUUFBUSxDQUFDckUsTUFBN0MsR0FBc0QsZ0JBQXRELEdBQXlFcUUsUUFBUSxDQUFDckUsTUFBbEYsR0FBMkYsWUFBM0csRUFBeUhnRyxLQUF6SCxFQUFiO0FBQ0EsWUFBSUMsTUFBTSxHQUFHLENBQWI7O0FBQ0EsZUFBTzdDLFFBQVEsQ0FBQ2xCLE1BQVQsR0FBa0JtQyxRQUFRLENBQUMvRCxvQkFBbEMsRUFBd0Q7QUFDcEQyRixnQkFBTTtBQUNOLGNBQUl6SCxPQUFPLEdBQUc0RSxRQUFRLENBQUNsQixNQUFULEdBQWtCLENBQWxCLEdBQXNCa0IsUUFBUSxDQUFDOEMsSUFBVCxFQUF0QixHQUF3Q3pELFNBQXREO0FBQ0EsY0FBSVksS0FBSyxHQUFHRCxRQUFRLENBQUNsQixNQUFULEdBQWtCLENBQTlCO0FBQ0FrQixrQkFBUSxHQUFHK0MsS0FBSyxDQUFDL0gsU0FBRCxFQUFZMkgsTUFBWixFQUFvQjlILFVBQXBCLEVBQWdDb0csUUFBaEMsRUFBMENqQixRQUExQyxFQUFvRDVFLE9BQXBELEVBQTZENkUsS0FBN0QsRUFBb0UsS0FBcEUsQ0FBaEI7O0FBQ0EsY0FBSTRDLE1BQU0sR0FBRzVCLFFBQVEsQ0FBQy9ELG9CQUF0QixFQUE0QztBQUN4QzhGLG1CQUFPLENBQUNDLEtBQVIsQ0FBYyxzQ0FBc0NoQyxRQUFRLENBQUNsRSxpQkFBL0MsR0FBbUUsZUFBakY7QUFDQTtBQUNIO0FBQ0o7O0FBRURsQyxrQkFBVSxDQUFDZ0csSUFBWCxDQUFnQixtQkFBaEIsRUFBcUNiLFFBQVEsQ0FBQ2xCLE1BQTlDO0FBQ0gsT0F0Q2dGLENBd0NqRjtBQUNBO0FBQ0E7OztBQUNBa0IsY0FBUSxDQUFDTyxJQUFULENBQWMsVUFBVU4sS0FBVixFQUFpQjtBQUMzQixZQUFJN0UsT0FBTyxHQUFHVCxDQUFDLENBQUMsSUFBRCxDQUFmOztBQUVBLFlBQUkySCxnQkFBSixFQUFzQjtBQUNsQmxILGlCQUFPLENBQUN5RixJQUFSLENBQWEsT0FBYixFQUFzQlosS0FBdEI7QUFDSDs7QUFFRCxZQUFJaUQsT0FBTyxHQUFHOUgsT0FBTyxDQUFDMkYsSUFBUixDQUFhLE1BQU1FLFFBQVEsQ0FBQ3JFLE1BQWYsR0FBd0IsVUFBckMsRUFBaUR1RyxPQUFqRCxHQUEyREMsTUFBM0QsQ0FBa0UsTUFBTW5DLFFBQVEsQ0FBQ3JFLE1BQWYsR0FBd0IsVUFBMUYsQ0FBZDs7QUFDQSxZQUFJc0csT0FBTyxDQUFDcEUsTUFBUixLQUFtQixDQUF2QixFQUEwQjtBQUN0Qm9FLGlCQUFPLEdBQUd2SSxDQUFDLENBQUMsTUFBTXNHLFFBQVEsQ0FBQ3RELG9CQUFmLEdBQXNDLFVBQXRDLEdBQW1Ec0QsUUFBUSxDQUFDckUsTUFBNUQsR0FBcUUsY0FBckUsR0FBc0ZxRSxRQUFRLENBQUN0RCxvQkFBL0YsR0FBc0gsR0FBdkgsQ0FBWDtBQUVBdkMsaUJBQU8sQ0FBQ3FILE1BQVIsQ0FBZVMsT0FBZjtBQUNIOztBQUVELFlBQUlHLEtBQUssR0FBRyxDQUFaOztBQUNBLFlBQUk5RixLQUFLLEtBQUssUUFBVixJQUFzQjBELFFBQVEsQ0FBQ3BELFFBQW5DLEVBQTZDO0FBQ3pDd0YsZUFBSyxHQUFHLENBQVI7QUFDSDs7QUFFRCxZQUFJQyxPQUFPLEdBQUcsQ0FDVjtBQUNJLHFCQUFXckMsUUFBUSxDQUFDbkYsWUFEeEI7QUFFSSxzQkFBWW1GLFFBQVEsQ0FBQ3JFLE1BQVQsR0FBa0IsU0FGbEM7QUFHSSxrQkFBUXFFLFFBQVEsQ0FBQ2xGLE1BSHJCO0FBSUksdUJBQWFpRSxRQUFRLENBQUNsQixNQUFULEdBQWtCdUUsS0FBbEIsR0FBMEJwQyxRQUFRLENBQUN4RTtBQUpwRCxTQURVLEVBTVA7QUFDQyxxQkFBV3dFLFFBQVEsQ0FBQ2hHLFFBRHJCO0FBRUMsc0JBQVlnRyxRQUFRLENBQUNyRSxNQUFULEdBQWtCLEtBRi9CO0FBR0Msa0JBQVFxRSxRQUFRLENBQUMvRixFQUhsQjtBQUlDLHVCQUFhOEUsUUFBUSxDQUFDbEIsTUFBVCxHQUFrQnVFLEtBQWxCLEdBQTBCLENBQTFCLElBQStCckQsUUFBUSxDQUFDQyxLQUFULENBQWU3RSxPQUFmLElBQTBCaUksS0FBMUIsR0FBa0M7QUFKL0UsU0FOTyxFQVdQO0FBQ0MscUJBQVdwQyxRQUFRLENBQUMzRixVQURyQjtBQUVDLHNCQUFZMkYsUUFBUSxDQUFDckUsTUFBVCxHQUFrQixPQUYvQjtBQUdDLGtCQUFRcUUsUUFBUSxDQUFDMUYsSUFIbEI7QUFJQyx1QkFBYXlFLFFBQVEsQ0FBQ2xCLE1BQVQsR0FBa0J1RSxLQUFsQixHQUEwQixDQUExQixJQUErQnJELFFBQVEsQ0FBQ0MsS0FBVCxDQUFlN0UsT0FBZixNQUE0QjRFLFFBQVEsQ0FBQ2xCLE1BQVQsR0FBa0I7QUFKM0YsU0FYTyxFQWdCUDtBQUNDLHFCQUFXbUMsUUFBUSxDQUFDdkYsU0FBVCxJQUFzQixDQUFDdUYsUUFBUSxDQUFDdEUsY0FBaEMsSUFBa0QsQ0FBQ3NFLFFBQVEsQ0FBQ3ZELG1CQUR4RTtBQUVDLHNCQUFZdUQsUUFBUSxDQUFDckUsTUFBVCxHQUFrQixNQUYvQjtBQUdDLGtCQUFRcUUsUUFBUSxDQUFDdEYsR0FIbEI7QUFJQyx1QkFBYXFFLFFBQVEsQ0FBQ2xCLE1BQVQsR0FBa0J1RSxLQUFsQixHQUEwQnBDLFFBQVEsQ0FBQ3ZFO0FBSmpELFNBaEJPLEVBcUJQO0FBQ0MscUJBQVd1RSxRQUFRLENBQUM5RSxlQURyQjtBQUVDLHNCQUFZOEUsUUFBUSxDQUFDckUsTUFBVCxHQUFrQixZQUYvQjtBQUdDLGtCQUFRcUUsUUFBUSxDQUFDN0UsU0FIbEI7QUFJQyx1QkFBYTRELFFBQVEsQ0FBQ2xCLE1BQVQsR0FBa0J1RSxLQUFsQixHQUEwQnBDLFFBQVEsQ0FBQ3ZFO0FBSmpELFNBckJPLENBQWQ7QUE2QkEvQixTQUFDLENBQUM0RixJQUFGLENBQU8rQyxPQUFQLEVBQWdCLFVBQVU3QyxDQUFWLEVBQWFrQyxNQUFiLEVBQXFCO0FBQ2pDLGNBQUlBLE1BQU0sQ0FBQ1ksT0FBWCxFQUFvQjtBQUNoQixnQkFBSUMsTUFBTSxHQUFHcEksT0FBTyxDQUFDMkYsSUFBUixDQUFhLE1BQU00QixNQUFNLENBQUMzRCxRQUExQixDQUFiOztBQUNBLGdCQUFJd0UsTUFBTSxDQUFDMUUsTUFBUCxLQUFrQixDQUFsQixJQUF1QjZELE1BQU0sQ0FBQ3BELElBQWxDLEVBQXdDO0FBQ3BDaUUsb0JBQU0sR0FBRzdJLENBQUMsQ0FBQ2dJLE1BQU0sQ0FBQ3BELElBQVIsQ0FBRCxDQUNKa0UsUUFESSxDQUNLUCxPQURMLEVBRUpSLFFBRkksQ0FFS0MsTUFBTSxDQUFDM0QsUUFGWixDQUFUO0FBR0g7O0FBQ0QsZ0JBQUkyRCxNQUFNLENBQUNlLFNBQVgsRUFBc0I7QUFDbEJGLG9CQUFNLENBQUNHLFdBQVAsQ0FBbUIxQyxRQUFRLENBQUNyRSxNQUFULEdBQWtCLGtCQUFyQzs7QUFDQSxrQkFBSXFFLFFBQVEsQ0FBQzlELG9CQUFiLEVBQW1DO0FBQy9CcUcsc0JBQU0sQ0FBQ0ksR0FBUCxDQUFXLFNBQVgsRUFBc0IsRUFBdEI7QUFDSDtBQUNKLGFBTEQsTUFLTztBQUNISixvQkFBTSxDQUFDZCxRQUFQLENBQWdCekIsUUFBUSxDQUFDckUsTUFBVCxHQUFrQixrQkFBbEM7O0FBQ0Esa0JBQUlxRSxRQUFRLENBQUM5RCxvQkFBYixFQUFtQztBQUMvQnFHLHNCQUFNLENBQUNJLEdBQVAsQ0FBVyxTQUFYLEVBQXNCLE1BQXRCO0FBQ0g7QUFDSjs7QUFDREosa0JBQU0sQ0FDRGQsUUFETCxDQUNjekIsUUFBUSxDQUFDckUsTUFBVCxHQUFrQixTQURoQyxFQUVLaUUsSUFGTCxDQUVVLFlBRlYsRUFFd0JoRyxVQUFVLENBQUMrRCxJQUFYLENBQWdCLElBQWhCLENBRnhCLEVBR0tpQyxJQUhMLENBR1UsU0FIVixFQUdxQm5DLGFBQWEsQ0FBQzdELFVBQVUsQ0FBQytELElBQVgsQ0FBZ0IsSUFBaEIsSUFBd0IsR0FBeEIsR0FBOEJxQixLQUEvQixFQUFzQzdFLE9BQXRDLENBSGxDO0FBSUgsV0F0QkQsTUFzQk87QUFDSEEsbUJBQU8sQ0FBQzJGLElBQVIsQ0FBYSxNQUFNNEIsTUFBTSxDQUFDM0QsUUFBMUIsRUFBb0M0RSxHQUFwQyxDQUF3QyxTQUF4QyxFQUFtRCxNQUFuRDtBQUNIO0FBQ0osU0ExQkQ7QUE0QkgsT0E1RUQsRUEzQ2lGLENBdUg3RTtBQUVKO0FBQ0E7O0FBQ0EsVUFBSTNDLFFBQVEsQ0FBQ3ZGLFNBQWIsRUFBd0I7QUFFcEIsWUFBSTJILEtBQUssR0FBRyxDQUFaOztBQUNBLFlBQUk5RixLQUFLLEtBQUssUUFBVixJQUFzQjBELFFBQVEsQ0FBQ3BELFFBQW5DLEVBQTZDO0FBQ3pDd0YsZUFBSyxHQUFHLENBQVI7QUFDSDs7QUFFRCxZQUFJUSxTQUFTLEdBQUdoSixVQUFVLENBQUNrRyxJQUFYLENBQWdCLE1BQU1FLFFBQVEsQ0FBQ3JFLE1BQWYsR0FBd0IsYUFBeEMsRUFBdURnSCxHQUF2RCxDQUEyRCxTQUEzRCxFQUFzRSxFQUF0RSxFQUEwRUQsV0FBMUUsQ0FBc0YxQyxRQUFRLENBQUNyRSxNQUFULEdBQWtCLGtCQUF4RyxDQUFoQjtBQUNBLFlBQUlrSCxJQUFJLEdBQUdqSixVQUFVLENBQUNrRyxJQUFYLENBQWdCLE1BQU1FLFFBQVEsQ0FBQ3JFLE1BQWYsR0FBd0IsTUFBeEMsQ0FBWDs7QUFDQSxZQUFJLENBQUNxRSxRQUFRLENBQUN0RSxjQUFWLElBQTRCbUgsSUFBSSxDQUFDaEYsTUFBTCxHQUFjdUUsS0FBMUMsSUFBbURwQyxRQUFRLENBQUN2RCxtQkFBaEUsRUFBcUY7QUFDakZtRyxtQkFBUyxDQUFDRCxHQUFWLENBQWMsU0FBZCxFQUF5QixNQUF6QjtBQUNILFNBRkQsTUFFTyxJQUFJckcsS0FBSyxLQUFLLFFBQVYsSUFBc0IwRCxRQUFRLENBQUNwRCxRQUFuQyxFQUE2QztBQUNoRGdHLG1CQUFTLENBQUNELEdBQVYsQ0FBYyxTQUFkLEVBQXlCLE1BQXpCO0FBQ0FDLG1CQUFTLENBQUNFLE1BQVYsQ0FBaUIsTUFBakI7QUFDSDs7QUFDRCxZQUFJL0QsUUFBUSxDQUFDbEIsTUFBVCxHQUFrQnVFLEtBQWxCLElBQTJCcEMsUUFBUSxDQUFDdkUsR0FBeEMsRUFBNkM7QUFDekNtSCxtQkFBUyxDQUFDbkIsUUFBVixDQUFtQnpCLFFBQVEsQ0FBQ3JFLE1BQVQsR0FBa0Isa0JBQXJDOztBQUNBLGNBQUlxRSxRQUFRLENBQUM5RCxvQkFBYixFQUFtQztBQUMvQnRDLHNCQUFVLENBQUNrRyxJQUFYLENBQWdCLE1BQU1FLFFBQVEsQ0FBQ3JFLE1BQWYsR0FBd0IsU0FBeEIsR0FBb0NxRSxRQUFRLENBQUNyRSxNQUE3QyxHQUFzRCxnQkFBdEQsR0FBeUVxRSxRQUFRLENBQUNyRSxNQUFsRixHQUEyRixZQUEzRyxFQUF5SGdILEdBQXpILENBQTZILFNBQTdILEVBQXdJLE1BQXhJO0FBQ0g7QUFDSjtBQUNKO0FBRUosS0FsSkQsQ0EzVGlDLENBNmM5QjtBQUVIO0FBQ0E7QUFDQTs7O0FBQ0EsUUFBSUkseUJBQXlCLEdBQUcsU0FBNUJBLHlCQUE0QixDQUFVbkosVUFBVixFQUFzQk8sT0FBdEIsRUFBK0I2RixRQUEvQixFQUF5QztBQUNyRSxVQUFJQSxRQUFRLENBQUNoRSxRQUFiLEVBQXVCO0FBQ25CdEMsU0FBQyxDQUFDNEYsSUFBRixDQUFPVSxRQUFRLENBQUNoRSxRQUFoQixFQUEwQixVQUFVZ0QsS0FBVixFQUFpQmdFLGdCQUFqQixFQUFtQztBQUN6RCxjQUFJLENBQUNBLGdCQUFnQixDQUFDakYsUUFBdEIsRUFBZ0M7QUFDNUJnRSxtQkFBTyxDQUFDa0IsR0FBUixDQUFZLDRDQUE0Q3JKLFVBQVUsQ0FBQytELElBQVgsQ0FBZ0IsSUFBaEIsQ0FBNUMsR0FBb0UsdUVBQWhGO0FBQ0EsbUJBQU8sSUFBUDtBQUNIOztBQUNELGNBQUl4RCxPQUFPLEtBQUssSUFBaEIsRUFBc0I7QUFDbEJBLG1CQUFPLENBQUMyRixJQUFSLENBQWFrRCxnQkFBZ0IsQ0FBQ2pGLFFBQTlCLEVBQXdDbkUsVUFBeEMsQ0FBbURvSixnQkFBbkQ7QUFDSCxXQUZELE1BRU87QUFDSHBKLHNCQUFVLENBQUNrRyxJQUFYLENBQWdCa0QsZ0JBQWdCLENBQUNqRixRQUFqQyxFQUEyQ25FLFVBQTNDLENBQXNEb0osZ0JBQXREO0FBQ0g7QUFDSixTQVZEO0FBV0g7QUFDSixLQWRELENBbGRpQyxDQWtlakM7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFFBQUlsQixLQUFLLEdBQUcsU0FBUkEsS0FBUSxDQUFVL0gsU0FBVixFQUFxQm1KLElBQXJCLEVBQTJCdEosVUFBM0IsRUFBdUNvRyxRQUF2QyxFQUFpRGpCLFFBQWpELEVBQTJENUUsT0FBM0QsRUFBb0U2RSxLQUFwRSxFQUEyRW1FLFdBQTNFLEVBQXdGO0FBQ2hHLFVBQUlwRSxRQUFRLENBQUNsQixNQUFULEdBQWtCbUMsUUFBUSxDQUFDdkUsR0FBM0IsS0FBbUMwSCxXQUFXLElBQUl4RSxlQUFlLENBQUNxQixRQUFRLENBQUM1RSxnQkFBVCxDQUEwQnhCLFVBQTFCLEVBQXNDTyxPQUF0QyxDQUFELENBQTlCLElBQWtGd0UsZUFBZSxDQUFDcUIsUUFBUSxDQUFDckYsVUFBVCxDQUFvQmYsVUFBcEIsRUFBZ0NPLE9BQWhDLENBQUQsQ0FBcEksQ0FBSixFQUFxTDtBQUNqTCxZQUFJaUosU0FBUyxHQUFHeEosVUFBVSxDQUFDZ0csSUFBWCxDQUFnQixXQUFoQixDQUFoQjtBQUNBLFlBQUl5RCxTQUFTLEdBQUd6SixVQUFVLENBQUNnRyxJQUFYLENBQWdCLG1CQUFoQixDQUFoQjtBQUVBaEcsa0JBQVUsQ0FBQ2dHLElBQVgsQ0FBZ0IsbUJBQWhCLEVBQXFDeUQsU0FBUyxHQUFHLENBQWpEOztBQUVBLFlBQUlyRSxLQUFLLEtBQUssQ0FBQyxDQUFmLEVBQWtCO0FBQ2RBLGVBQUssR0FBR0QsUUFBUSxDQUFDbEIsTUFBVCxHQUFrQixDQUExQjtBQUNIOztBQUNELFlBQUl5RixNQUFNLEdBQUcsSUFBSW5ELE1BQUosQ0FBV3ZCLFNBQVMsQ0FBQ29CLFFBQVEsQ0FBQ3BFLGNBQVYsQ0FBcEIsRUFBK0MsR0FBL0MsQ0FBYjtBQUNBLFlBQUkySCxPQUFPLEdBQUdGLFNBQWQ7O0FBRUEsWUFBSXJELFFBQVEsQ0FBQ2xELGNBQWIsRUFBNkI7QUFDekJ5RyxpQkFBTyxHQUFHM0osVUFBVSxDQUFDZ0csSUFBWCxDQUFnQixxQkFBaEIsQ0FBVjs7QUFFQSxjQUFJMkQsT0FBTyxLQUFLbkYsU0FBaEIsRUFBMkI7QUFDdkJtRixtQkFBTyxHQUFHQyxrQkFBa0IsQ0FBQ3hELFFBQUQsRUFBV2pCLFFBQVgsQ0FBNUI7QUFDSDs7QUFFRG5GLG9CQUFVLENBQUNnRyxJQUFYLENBQWdCLHFCQUFoQixFQUF1QzJELE9BQU8sR0FBRyxDQUFqRDtBQUNIOztBQUVELFlBQUlFLElBQUksR0FBRy9KLENBQUMsQ0FBQzBKLFNBQVMsQ0FBQy9GLE9BQVYsQ0FBa0JpRyxNQUFsQixFQUEwQkMsT0FBMUIsQ0FBRCxDQUFELENBQXNDM0QsSUFBdEMsQ0FBMkMsT0FBM0MsRUFBb0R5RCxTQUFwRCxDQUFYO0FBQ0FLLHNCQUFjLENBQUMxRCxRQUFELEVBQVd5RCxJQUFYLENBQWQ7QUFFQSxZQUFJbkMsY0FBYyxHQUFHNUgsQ0FBQyxDQUFDc0csUUFBUSxDQUFDakUsd0JBQVYsQ0FBdEI7QUFDQSxZQUFJNEgsR0FBRyxHQUFHckMsY0FBYyxDQUFDeEIsSUFBZixDQUFvQixRQUFRRSxRQUFRLENBQUNyRSxNQUFqQixHQUEwQixNQUE5QyxDQUFWO0FBQ0EsWUFBSWlJLEVBQUUsR0FBR2xLLENBQUMsQ0FBQytKLElBQUQsQ0FBRCxDQUFRM0QsSUFBUixDQUFhLE1BQWIsRUFBcUI2QixLQUFyQixHQUE2QmhFLElBQTdCLENBQWtDLElBQWxDLENBQVQ7O0FBRUEsWUFBSXdGLFdBQUosRUFBaUI7QUFDYixjQUFJVSxVQUFVLEdBQUc5RSxRQUFRLENBQUNjLEVBQVQsQ0FBWWIsS0FBWixDQUFqQjtBQUNBMkIsNkJBQW1CLENBQUNrRCxVQUFELENBQW5CO0FBQ0EsY0FBSUMsT0FBTyxHQUFHcEssQ0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZOEgsTUFBWixDQUFtQnFDLFVBQVUsQ0FBQ0UsS0FBWCxFQUFuQixFQUF1Q3pGLElBQXZDLEVBQWQ7QUFDQSxjQUFJMkIsUUFBUSxHQUFHRCxRQUFRLENBQUNsRCxjQUFULElBQTJCa0QsUUFBUSxDQUFDbkQsdUJBQXBDLEdBQThEZ0gsVUFBVSxDQUFDakUsSUFBWCxDQUFnQixPQUFoQixDQUE5RCxHQUF5RlosS0FBeEc7QUFDQSxjQUFJeUIsTUFBTSxHQUFHVCxRQUFRLENBQUNsRCxjQUFULEdBQTBCa0gsYUFBYSxDQUFDaEUsUUFBRCxFQUFXNkQsVUFBWCxDQUF2QyxHQUFnRTVELFFBQTdFO0FBQ0EsY0FBSWdFLE9BQU8sR0FBR3pELGVBQWUsQ0FBQzVHLFVBQUQsRUFBYW9HLFFBQWIsRUFBdUI4RCxPQUF2QixFQUFnQzdELFFBQWhDLEVBQTBDb0QsU0FBMUMsRUFBcUQ1QyxNQUFyRCxFQUE2RDhDLE9BQTdELENBQTdCO0FBRUFFLGNBQUksR0FBRy9KLENBQUMsQ0FBQyxRQUFELENBQUQsQ0FBWTRFLElBQVosQ0FBaUIyRixPQUFqQixFQUEwQkMsUUFBMUIsR0FBcUN0RSxJQUFyQyxDQUEwQyxPQUExQyxFQUFtRHlELFNBQW5ELENBQVA7O0FBQ0EsY0FBSXJELFFBQVEsQ0FBQ3JELE9BQWIsRUFBc0I7QUFDbEI4RyxnQkFBSSxDQUFDVSxJQUFMO0FBQ0g7O0FBQ0RSLGFBQUcsQ0FBQ1MsTUFBSixDQUFXWCxJQUFYLEVBQWlCM0QsSUFBakIsQ0FBc0JFLFFBQVEsQ0FBQ3JFLE1BQVQsR0FBa0IsVUFBeEMsRUFBb0RiLE1BQXBEO0FBQ0gsU0FiRCxNQWFPO0FBQ0gsY0FBSWtGLFFBQVEsQ0FBQ3JELE9BQWIsRUFBc0I7QUFDbEI4RyxnQkFBSSxDQUFDVSxJQUFMO0FBQ0g7O0FBRURSLGFBQUcsQ0FBQ1MsTUFBSixDQUFXWCxJQUFYO0FBQ0g7O0FBRUQxRSxnQkFBUSxHQUFHbkYsVUFBVSxDQUFDa0csSUFBWCxDQUFnQkUsUUFBUSxDQUFDbEUsaUJBQXpCLENBQVg7QUFFQSxZQUFJeUcsTUFBTSxHQUFHa0IsSUFBSSxDQUFDM0QsSUFBTCxDQUFVLE1BQU1FLFFBQVEsQ0FBQ3JFLE1BQWYsR0FBd0IsU0FBeEIsR0FBb0NxRSxRQUFRLENBQUNyRSxNQUE3QyxHQUFzRCxZQUFoRSxDQUFiOztBQUNBLFlBQUk0RyxNQUFNLENBQUMxRSxNQUFQLEdBQWdCLENBQXBCLEVBQXVCO0FBQ25CMEUsZ0JBQU0sQ0FBQ2QsUUFBUCxDQUFnQnpCLFFBQVEsQ0FBQ3JFLE1BQVQsR0FBa0IsU0FBbEMsRUFBNkNpRSxJQUE3QyxDQUFrRCxZQUFsRCxFQUFnRWhHLFVBQVUsQ0FBQytELElBQVgsQ0FBZ0IsSUFBaEIsQ0FBaEU7QUFDSDs7QUFFRCxZQUFJLENBQUNxQyxRQUFRLENBQUN0RSxjQUFWLElBQTRCc0QsS0FBSyxHQUFHLENBQVIsS0FBY3FFLFNBQTlDLEVBQXlEO0FBQ3JEdEUsa0JBQVEsR0FBR3NGLE1BQU0sQ0FBQ3pLLFVBQUQsRUFBYW9HLFFBQWIsRUFBdUJqQixRQUF2QixFQUFpQzBFLElBQWpDLEVBQXVDSixTQUF2QyxFQUFrRHJFLEtBQUssR0FBRyxDQUExRCxDQUFqQjtBQUNILFNBRkQsTUFFTztBQUNIb0MsK0JBQXFCLENBQUN4SCxVQUFELEVBQWFvRyxRQUFiLEVBQXVCLEtBQXZCLENBQXJCO0FBQ0g7O0FBRUQrQyxpQ0FBeUIsQ0FBQ25KLFVBQUQsRUFBYTZKLElBQWIsRUFBbUJ6RCxRQUFuQixDQUF6Qjs7QUFFQSxZQUFLbUQsV0FBVyxJQUFJLENBQUN4RSxlQUFlLENBQUNxQixRQUFRLENBQUMzRSxlQUFULENBQXlCekIsVUFBekIsRUFBcUM2SixJQUFyQyxDQUFELENBQWhDLElBQWlGLENBQUM5RSxlQUFlLENBQUNxQixRQUFRLENBQUNwRixTQUFULENBQW1CaEIsVUFBbkIsRUFBK0I2SixJQUEvQixDQUFELENBQXJHLEVBQTZJO0FBQ3pJLGNBQUl6RSxLQUFLLEtBQUssQ0FBQyxDQUFmLEVBQWtCO0FBQ2RELG9CQUFRLEdBQUdtQyxlQUFlLENBQUN0SCxVQUFELEVBQWFtRixRQUFiLEVBQXVCaUIsUUFBdkIsRUFBaUNoQixLQUFLLEdBQUcsQ0FBekMsQ0FBMUI7QUFDSDs7QUFDRHlFLGNBQUksQ0FBQzNJLE1BQUw7QUFDSDtBQUNKOztBQUVELFVBQUkySSxJQUFJLEtBQUtyRixTQUFULElBQXNCNEIsUUFBUSxDQUFDckQsT0FBbkMsRUFBNEM7QUFDeEM4RyxZQUFJLENBQUNYLE1BQUwsQ0FBWSxNQUFaLEVBQW9CLFlBQVk7QUFDNUIsY0FBSTlDLFFBQVEsQ0FBQ25ELHVCQUFiLEVBQXNDO0FBQ2xDeUgsOEJBQWtCLENBQUN0RSxRQUFELEVBQVdqQixRQUFYLENBQWxCO0FBQ0g7QUFDSixTQUpEO0FBS0gsT0FORCxNQU1PO0FBQ0gsWUFBSWlCLFFBQVEsQ0FBQ25ELHVCQUFiLEVBQXNDO0FBQ2xDLGlCQUFPeUgsa0JBQWtCLENBQUN0RSxRQUFELEVBQVdqQixRQUFYLENBQXpCO0FBQ0g7QUFDSjs7QUFFRCxhQUFPQSxRQUFQO0FBQ0gsS0F2RkQsQ0F0ZWlDLENBK2pCakM7QUFDQTs7O0FBQ0EsUUFBSXdGLFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQVUzSyxVQUFWLEVBQXNCb0csUUFBdEIsRUFBZ0NqQixRQUFoQyxFQUEwQzVFLE9BQTFDLEVBQW1ENkUsS0FBbkQsRUFBMEQ7QUFDckUsVUFBSUQsUUFBUSxDQUFDbEIsTUFBVCxHQUFrQm1DLFFBQVEsQ0FBQ3hFLEdBQTNCLElBQWtDbUQsZUFBZSxDQUFDcUIsUUFBUSxDQUFDakYsYUFBVCxDQUF1Qm5CLFVBQXZCLEVBQW1DTyxPQUFuQyxDQUFELENBQXJELEVBQW9HO0FBQ2hHLFlBQUlxSyxRQUFRLEdBQUcsU0FBWEEsUUFBVyxHQUFZO0FBQ3ZCLGNBQUlDLFFBQVEsR0FBR3RLLE9BQWY7O0FBQ0EsY0FBSSxDQUFDNkYsUUFBUSxDQUFDbEQsY0FBZCxFQUE4QjtBQUMxQmlDLG9CQUFRLEdBQUdtQyxlQUFlLENBQUN0SCxVQUFELEVBQWFtRixRQUFiLEVBQXVCaUIsUUFBdkIsRUFBaUNoQixLQUFqQyxDQUExQjtBQUNBeUYsb0JBQVEsR0FBRzFGLFFBQVEsQ0FBQzhDLElBQVQsRUFBWDtBQUNIOztBQUNELGNBQUk2QyxNQUFNLEdBQUdELFFBQVEsQ0FBQ1YsS0FBVCxDQUFlO0FBQUNZLDZCQUFpQixFQUFFO0FBQXBCLFdBQWYsRUFBMENDLElBQTFDLEVBQWI7QUFDQUgsa0JBQVEsQ0FBQzNKLE1BQVQ7O0FBQ0EsY0FBSSxDQUFDNkQsZUFBZSxDQUFDcUIsUUFBUSxDQUFDL0UsWUFBVCxDQUFzQnJCLFVBQXRCLEVBQWtDOEssTUFBbEMsQ0FBRCxDQUFwQixFQUFpRTtBQUM3RCxnQkFBSXBELGNBQWMsR0FBRzVILENBQUMsQ0FBQ3NHLFFBQVEsQ0FBQ2pFLHdCQUFWLENBQXRCO0FBQ0F1RiwwQkFBYyxDQUFDeEIsSUFBZixDQUFvQixRQUFRRSxRQUFRLENBQUNyRSxNQUFqQixHQUEwQixNQUE5QyxFQUFzRHlJLE1BQXRELENBQTZETSxNQUE3RDtBQUNBM0Ysb0JBQVEsR0FBR25GLFVBQVUsQ0FBQ2tHLElBQVgsQ0FBZ0JFLFFBQVEsQ0FBQ2xFLGlCQUF6QixDQUFYO0FBQ0FpRCxvQkFBUSxHQUFHb0MsaUJBQWlCLENBQUN2SCxVQUFELEVBQWFtRixRQUFiLEVBQXVCaUIsUUFBdkIsRUFBaUNoQixLQUFLLEdBQUcsQ0FBekMsQ0FBNUI7QUFDSDs7QUFDRCxjQUFJZ0IsUUFBUSxDQUFDbkQsdUJBQWIsRUFBc0M7QUFDbEN5SCw4QkFBa0IsQ0FBQ3RFLFFBQUQsRUFBV2pCLFFBQVgsQ0FBbEI7QUFDSDtBQUNKLFNBakJEOztBQWtCQSxZQUFJaUIsUUFBUSxDQUFDcEQsUUFBYixFQUF1QjtBQUNuQnpDLGlCQUFPLENBQUMwSyxPQUFSLENBQWdCLE1BQWhCLEVBQXdCLFlBQVk7QUFDaENMLG9CQUFRO0FBQ1gsV0FGRDtBQUdILFNBSkQsTUFJTztBQUNIQSxrQkFBUTtBQUNYO0FBQ0o7O0FBRUQsYUFBT3pGLFFBQVA7QUFDSCxLQTlCRCxDQWprQmlDLENBaW1CakM7QUFDQTs7O0FBQ0EsUUFBSStGLElBQUksR0FBRyxTQUFQQSxJQUFPLENBQVVsTCxVQUFWLEVBQXNCb0csUUFBdEIsRUFBZ0NqQixRQUFoQyxFQUEwQzVFLE9BQTFDLEVBQW1ENkUsS0FBbkQsRUFBMEQ7QUFDakUsVUFBSUEsS0FBSyxLQUFLLENBQVYsSUFBZUwsZUFBZSxDQUFDcUIsUUFBUSxDQUFDOUYsU0FBVCxDQUFtQk4sVUFBbkIsRUFBK0JPLE9BQS9CLENBQUQsQ0FBbEMsRUFBNkU7QUFDekU0RSxnQkFBUSxHQUFHOEIsWUFBWSxDQUFDakgsVUFBRCxFQUFhbUYsUUFBYixFQUF1QkMsS0FBdkIsRUFBOEJBLEtBQUssR0FBRyxDQUF0QyxDQUF2Qjs7QUFDQSxZQUFJLENBQUNMLGVBQWUsQ0FBQ3FCLFFBQVEsQ0FBQzVGLFFBQVQsQ0FBa0JSLFVBQWxCLEVBQThCTyxPQUE5QixDQUFELENBQXBCLEVBQThEO0FBQzFENEUsa0JBQVEsR0FBRzhCLFlBQVksQ0FBQ2pILFVBQUQsRUFBYW1GLFFBQWIsRUFBdUJDLEtBQUssR0FBRyxDQUEvQixFQUFrQ0EsS0FBbEMsQ0FBdkI7QUFDSDtBQUNKOztBQUVELFVBQUlnQixRQUFRLENBQUNuRCx1QkFBYixFQUFzQztBQUNsQyxlQUFPeUgsa0JBQWtCLENBQUN0RSxRQUFELEVBQVdqQixRQUFYLENBQXpCO0FBQ0g7O0FBRUQsYUFBT0EsUUFBUDtBQUNILEtBYkQsQ0FubUJpQyxDQWtuQmpDO0FBQ0E7OztBQUNBLFFBQUlnRyxNQUFNLEdBQUcsU0FBVEEsTUFBUyxDQUFVbkwsVUFBVixFQUFzQm9HLFFBQXRCLEVBQWdDakIsUUFBaEMsRUFBMEM1RSxPQUExQyxFQUFtRDZFLEtBQW5ELEVBQTBEO0FBQ25FLFVBQUlBLEtBQUssS0FBTUQsUUFBUSxDQUFDbEIsTUFBVCxHQUFrQixDQUE3QixJQUFtQ2MsZUFBZSxDQUFDcUIsUUFBUSxDQUFDekYsV0FBVCxDQUFxQlgsVUFBckIsRUFBaUNPLE9BQWpDLENBQUQsQ0FBdEQsRUFBbUc7QUFDL0Y0RSxnQkFBUSxHQUFHOEIsWUFBWSxDQUFDakgsVUFBRCxFQUFhbUYsUUFBYixFQUF1QkMsS0FBdkIsRUFBOEJBLEtBQUssR0FBRyxDQUF0QyxDQUF2Qjs7QUFDQSxZQUFJLENBQUNMLGVBQWUsQ0FBQ3FCLFFBQVEsQ0FBQ3hGLFVBQVQsQ0FBb0JaLFVBQXBCLEVBQWdDbUYsUUFBaEMsQ0FBRCxDQUFwQixFQUFpRTtBQUM3REEsa0JBQVEsR0FBRzhCLFlBQVksQ0FBQ2pILFVBQUQsRUFBYW1GLFFBQWIsRUFBdUJDLEtBQUssR0FBRyxDQUEvQixFQUFrQ0EsS0FBbEMsQ0FBdkI7QUFDSDtBQUNKOztBQUVELFVBQUlnQixRQUFRLENBQUNuRCx1QkFBYixFQUFzQztBQUNsQyxlQUFPeUgsa0JBQWtCLENBQUN0RSxRQUFELEVBQVdqQixRQUFYLENBQXpCO0FBQ0g7O0FBRUQsYUFBT0EsUUFBUDtBQUNILEtBYkQsQ0FwbkJpQyxDQW1vQmpDOzs7QUFDQSxRQUFJc0YsTUFBTSxHQUFHLFNBQVRBLE1BQVMsQ0FBVXpLLFVBQVYsRUFBc0JvRyxRQUF0QixFQUFnQ2pCLFFBQWhDLEVBQTBDNUUsT0FBMUMsRUFBbUQ4RixRQUFuRCxFQUE2REMsUUFBN0QsRUFBdUU7QUFDaEYsVUFBSSxNQUFNakQsSUFBSSxDQUFDK0gsR0FBTCxDQUFTOUUsUUFBUSxHQUFHRCxRQUFwQixDQUFWLEVBQXlDO0FBQ3JDbEIsZ0JBQVEsR0FBRzhCLFlBQVksQ0FBQ2pILFVBQUQsRUFBYW1GLFFBQWIsRUFBdUJrQixRQUF2QixFQUFpQ0MsUUFBakMsQ0FBdkI7O0FBQ0EsWUFBSSxFQUFFQSxRQUFRLEdBQUdELFFBQVgsR0FBc0IsQ0FBdEIsR0FBMEJ0QixlQUFlLENBQUNxQixRQUFRLENBQUM1RixRQUFULENBQWtCUixVQUFsQixFQUE4Qk8sT0FBOUIsQ0FBRCxDQUF6QyxHQUFvRndFLGVBQWUsQ0FBQ3FCLFFBQVEsQ0FBQ3hGLFVBQVQsQ0FBb0JaLFVBQXBCLEVBQWdDTyxPQUFoQyxDQUFELENBQXJHLENBQUosRUFBc0o7QUFDbEo0RSxrQkFBUSxHQUFHOEIsWUFBWSxDQUFDakgsVUFBRCxFQUFhbUYsUUFBYixFQUF1Qm1CLFFBQXZCLEVBQWlDRCxRQUFqQyxDQUF2QjtBQUNIO0FBQ0osT0FMRCxNQUtPO0FBQ0gsWUFBSUEsUUFBUSxHQUFHQyxRQUFmLEVBQXlCO0FBQ3JCbkIsa0JBQVEsR0FBR2lDLGNBQWMsQ0FBQ3BILFVBQUQsRUFBYW1GLFFBQWIsRUFBdUJpQixRQUF2QixFQUFpQ0MsUUFBakMsRUFBMkNDLFFBQTNDLENBQXpCOztBQUNBLGNBQUksRUFBRUEsUUFBUSxHQUFHRCxRQUFYLEdBQXNCLENBQXRCLEdBQTBCdEIsZUFBZSxDQUFDcUIsUUFBUSxDQUFDNUYsUUFBVCxDQUFrQlIsVUFBbEIsRUFBOEJPLE9BQTlCLENBQUQsQ0FBekMsR0FBb0Z3RSxlQUFlLENBQUNxQixRQUFRLENBQUN4RixVQUFULENBQW9CWixVQUFwQixFQUFnQ08sT0FBaEMsQ0FBRCxDQUFyRyxDQUFKLEVBQXNKO0FBQ2xKNEUsb0JBQVEsR0FBR2tDLGdCQUFnQixDQUFDckgsVUFBRCxFQUFhbUYsUUFBYixFQUF1QmlCLFFBQXZCLEVBQWlDRSxRQUFqQyxFQUEyQ0QsUUFBM0MsQ0FBM0I7QUFDSDtBQUNKLFNBTEQsTUFLTztBQUNIbEIsa0JBQVEsR0FBR2tDLGdCQUFnQixDQUFDckgsVUFBRCxFQUFhbUYsUUFBYixFQUF1QmlCLFFBQXZCLEVBQWlDQyxRQUFqQyxFQUEyQ0MsUUFBM0MsQ0FBM0I7O0FBQ0EsY0FBSSxFQUFFQSxRQUFRLEdBQUdELFFBQVgsR0FBc0IsQ0FBdEIsR0FBMEJ0QixlQUFlLENBQUNxQixRQUFRLENBQUM1RixRQUFULENBQWtCUixVQUFsQixFQUE4Qk8sT0FBOUIsQ0FBRCxDQUF6QyxHQUFvRndFLGVBQWUsQ0FBQ3FCLFFBQVEsQ0FBQ3hGLFVBQVQsQ0FBb0JaLFVBQXBCLEVBQWdDTyxPQUFoQyxDQUFELENBQXJHLENBQUosRUFBc0o7QUFDbEo0RSxvQkFBUSxHQUFHaUMsY0FBYyxDQUFDcEgsVUFBRCxFQUFhbUYsUUFBYixFQUF1QmlCLFFBQXZCLEVBQWlDRSxRQUFqQyxFQUEyQ0QsUUFBM0MsQ0FBekI7QUFDSDtBQUNKO0FBQ0o7O0FBQ0RtQiwyQkFBcUIsQ0FBQ3hILFVBQUQsRUFBYW9HLFFBQWIsRUFBdUIsS0FBdkIsQ0FBckI7O0FBRUEsVUFBSUEsUUFBUSxDQUFDbkQsdUJBQWIsRUFBc0M7QUFDbEMsZUFBT3lILGtCQUFrQixDQUFDdEUsUUFBRCxFQUFXakIsUUFBWCxDQUF6QjtBQUNIOztBQUVELGFBQU9BLFFBQVA7QUFDSCxLQTFCRDs7QUE0QkEsUUFBSXVGLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBVXRFLFFBQVYsRUFBb0JqQixRQUFwQixFQUE4QjtBQUNuRHJGLE9BQUMsQ0FBQ3FGLFFBQUQsQ0FBRCxDQUFZTyxJQUFaLENBQWlCLFlBQVk7QUFDekIsWUFBSW5GLE9BQU8sR0FBR1QsQ0FBQyxDQUFDLElBQUQsQ0FBZjtBQUNBNkUscUJBQWEsQ0FBQ3BFLE9BQU8sQ0FBQzJGLElBQVIsQ0FBYUUsUUFBUSxDQUFDbkQsdUJBQXRCLENBQUQsRUFBaURrQyxRQUFRLENBQUNDLEtBQVQsQ0FBZTdFLE9BQWYsQ0FBakQsQ0FBYjtBQUNILE9BSEQ7QUFLQSxhQUFPNEUsUUFBUDtBQUNILEtBUEQ7O0FBU0EsUUFBSWlGLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBVWhFLFFBQVYsRUFBb0I3RixPQUFwQixFQUE2QjtBQUM3QyxVQUFJd0YsSUFBSSxHQUFHeEYsT0FBTyxDQUFDMkYsSUFBUixDQUFhLG1CQUFtQkUsUUFBUSxDQUFDbkUsV0FBNUIsR0FBMEMsS0FBdkQsRUFBOEQ4QixJQUE5RCxDQUFtRSxNQUFuRSxDQUFYO0FBRUEsYUFBT2dDLElBQUksQ0FBQ3NGLEtBQUwsQ0FBV2pGLFFBQVEsQ0FBQ25FLFdBQVQsQ0FBcUJnQyxNQUFyQixHQUE4QixDQUF6QyxFQUE0Q1AsS0FBNUMsQ0FBa0QsR0FBbEQsRUFBdUQsQ0FBdkQsRUFBMEQsQ0FBMUQsQ0FBUDtBQUNILEtBSkQ7O0FBTUEsUUFBSWtHLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBVXhELFFBQVYsRUFBb0JqQixRQUFwQixFQUE4QjtBQUNuRCxVQUFJd0UsT0FBTyxHQUFHLENBQWQ7QUFFQXhFLGNBQVEsQ0FBQ08sSUFBVCxDQUFjLFlBQVk7QUFDdEIsWUFBSWMsR0FBRyxHQUFHNEQsYUFBYSxDQUFDaEUsUUFBRCxFQUFXdEcsQ0FBQyxDQUFDLElBQUQsQ0FBWixDQUF2Qjs7QUFFQSxZQUFJLGVBQWV3TCxJQUFmLENBQW9COUUsR0FBcEIsS0FBNEJBLEdBQUcsSUFBSW1ELE9BQXZDLEVBQWdEO0FBQzVDQSxpQkFBTyxHQUFHNEIsTUFBTSxDQUFDL0UsR0FBRCxDQUFOLEdBQWMsQ0FBeEI7QUFDSDtBQUNKLE9BTkQ7QUFRQSxhQUFPbUQsT0FBUDtBQUNILEtBWkQ7O0FBY0EsUUFBSUcsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFVMUQsUUFBVixFQUFvQmpHLFNBQXBCLEVBQStCO0FBQ2hELFVBQUlxTCxRQUFRLEdBQUcsQ0FDWCxTQURXLEVBRVgsa0JBRlcsRUFHWCxVQUhXLEVBSVgsTUFKVyxFQUtYLE9BTFcsRUFNWCxZQU5XLEVBT1gsU0FQVyxFQVFYLGFBUlcsRUFTWCxNQVRXLEVBVVgsS0FWVyxDQUFmO0FBYUExTCxPQUFDLENBQUM0RixJQUFGLENBQU84RixRQUFQLEVBQWlCLFlBQVk7QUFDekIsWUFBSUMsTUFBTSxHQUFHLElBQWI7QUFDQXRMLGlCQUFTLENBQUN1RixJQUFWLENBQWUsWUFBWTtBQUN2QixjQUFJNEQsSUFBSSxHQUFHeEosQ0FBQyxDQUFDLElBQUQsQ0FBWjs7QUFDQSxjQUFJd0osSUFBSSxDQUFDb0MsUUFBTCxDQUFjdEYsUUFBUSxDQUFDdUYsV0FBVCxHQUF1QkYsTUFBckMsQ0FBSixFQUFrRDtBQUM5Q25DLGdCQUFJLENBQUN6QixRQUFMLENBQWN6QixRQUFRLENBQUNyRSxNQUFULEdBQWtCMEosTUFBaEM7QUFDSDs7QUFDRG5DLGNBQUksQ0FBQ3BELElBQUwsQ0FBVSxHQUFWLEVBQWVSLElBQWYsQ0FBb0IsWUFBWTtBQUM1QixnQkFBSWtHLElBQUksR0FBRzlMLENBQUMsQ0FBQyxJQUFELENBQVo7O0FBQ0EsZ0JBQUk4TCxJQUFJLENBQUNGLFFBQUwsQ0FBY3RGLFFBQVEsQ0FBQ3VGLFdBQVQsR0FBdUJGLE1BQXJDLENBQUosRUFBa0Q7QUFDOUNHLGtCQUFJLENBQUMvRCxRQUFMLENBQWN6QixRQUFRLENBQUNyRSxNQUFULEdBQWtCMEosTUFBaEM7QUFDSDtBQUNKLFdBTEQ7QUFNSCxTQVhEO0FBWUgsT0FkRDtBQWVILEtBN0JELENBN3JCaUMsQ0E0dEJqQzs7O0FBQ0EsUUFBSUksS0FBSyxHQUFHL0wsQ0FBQyxDQUFDLElBQUQsQ0FBYixDQTd0QmlDLENBK3RCakM7O0FBQ0EsUUFBSStMLEtBQUssQ0FBQzVILE1BQU4sS0FBaUIsQ0FBckIsRUFBd0I7QUFDcEJrRSxhQUFPLENBQUNrQixHQUFSLENBQVksaUVBQVo7QUFDQSxhQUFPLEtBQVA7QUFDSDs7QUFFRHdDLFNBQUssQ0FBQ25HLElBQU4sQ0FBVyxZQUFZO0FBRW5CLFVBQUlVLFFBQVEsR0FBR3RHLENBQUMsQ0FBQ2dNLE1BQUYsQ0FBUyxJQUFULEVBQWUsRUFBZixFQUFtQjVMLFFBQW5CLEVBQTZCRCxPQUE3QixDQUFmLENBRm1CLENBSW5CO0FBQ0E7O0FBQ0EsVUFBSUgsQ0FBQyxDQUFDc0csUUFBUSxDQUFDakcsU0FBVixDQUFELENBQXNCOEQsTUFBdEIsS0FBaUMsQ0FBckMsRUFBd0M7QUFDcENrRSxlQUFPLENBQUNrQixHQUFSLENBQVksNEZBQVo7QUFDQSxlQUFPLEtBQVA7QUFDSCxPQVRrQixDQVduQjtBQUNBOzs7QUFDQSxVQUFJMEMsSUFBSSxHQUFHak0sQ0FBQyxDQUFDLElBQUQsQ0FBWjs7QUFDQSxVQUFJaU0sSUFBSSxDQUFDL0YsSUFBTCxDQUFVLFlBQVYsTUFBNEJ4QixTQUFoQyxFQUEyQztBQUN2QyxZQUFJeEUsVUFBVSxHQUFHRixDQUFDLENBQUMsTUFBTWlNLElBQUksQ0FBQy9GLElBQUwsQ0FBVSxZQUFWLENBQVAsQ0FBbEI7O0FBQ0EsWUFBSWhHLFVBQVUsQ0FBQ2lFLE1BQVgsS0FBc0IsQ0FBMUIsRUFBNkI7QUFDekJrRSxpQkFBTyxDQUFDa0IsR0FBUixDQUFZLDJEQUFaO0FBQ0EsaUJBQU8sSUFBUDtBQUNIO0FBQ0osT0FORCxNQU1PO0FBQ0hySixrQkFBVSxHQUFHK0wsSUFBYjtBQUNIOztBQUNEL0wsZ0JBQVUsR0FBR0YsQ0FBQyxDQUFDRSxVQUFELENBQWQsQ0F2Qm1CLENBeUJuQjs7QUFDQW9HLGNBQVEsQ0FBQ2pFLHdCQUFULEdBQW9DaUUsUUFBUSxDQUFDakUsd0JBQVQsQ0FBa0NzQixPQUFsQyxDQUEwQyxNQUExQyxFQUFrRCxNQUFNSSxhQUFhLENBQUMsRUFBRCxFQUFLN0QsVUFBTCxDQUFyRSxDQUFwQzs7QUFDQSxVQUFJLENBQUNvRyxRQUFRLENBQUNqRSx3QkFBZCxFQUF3QztBQUNwQ2lFLGdCQUFRLENBQUNqRSx3QkFBVCxHQUFvQyxNQUFNMEIsYUFBYSxDQUFDLEVBQUQsRUFBSzdELFVBQUwsQ0FBdkQ7O0FBQ0EsWUFBSUYsQ0FBQyxDQUFDc0csUUFBUSxDQUFDakUsd0JBQVYsQ0FBRCxDQUFxQzhCLE1BQXJDLEtBQWdELENBQXBELEVBQXVEO0FBQ25Ea0UsaUJBQU8sQ0FBQ2tCLEdBQVIsQ0FBWSxrRkFBWjtBQUNBLGlCQUFPLElBQVA7QUFDSDtBQUNKLE9BakNrQixDQW1DbkI7QUFDQTs7O0FBQ0FqRCxjQUFRLENBQUN1RixXQUFULEdBQXVCdkYsUUFBUSxDQUFDckUsTUFBaEM7QUFDQXFFLGNBQVEsQ0FBQ3JFLE1BQVQsR0FBa0IvQixVQUFVLENBQUMrRCxJQUFYLENBQWdCLElBQWhCLElBQXdCLEdBQXhCLEdBQThCcUMsUUFBUSxDQUFDdUYsV0FBekQ7QUFDQTdCLG9CQUFjLENBQUMxRCxRQUFELEVBQVdwRyxVQUFYLENBQWQsQ0F2Q21CLENBeUNuQjs7QUFDQSxVQUFJLENBQUNvRyxRQUFRLENBQUN2RixTQUFkLEVBQXlCO0FBQ3JCdUYsZ0JBQVEsQ0FBQzlFLGVBQVQsR0FBMkIsS0FBM0I7QUFDQThFLGdCQUFRLENBQUN0RSxjQUFULEdBQTBCLEtBQTFCO0FBQ0g7O0FBQ0QsVUFBSXNFLFFBQVEsQ0FBQy9ELG9CQUFULEdBQWdDK0QsUUFBUSxDQUFDdkUsR0FBN0MsRUFBa0Q7QUFDOUN1RSxnQkFBUSxDQUFDL0Qsb0JBQVQsR0FBZ0MrRCxRQUFRLENBQUN2RSxHQUF6QztBQUNIOztBQUNELFVBQUl1RSxRQUFRLENBQUN4RSxHQUFULEtBQWlCLENBQUN3RSxRQUFRLENBQUMvRCxvQkFBVixJQUFrQytELFFBQVEsQ0FBQy9ELG9CQUFULEdBQWdDK0QsUUFBUSxDQUFDeEUsR0FBNUYsQ0FBSixFQUFzRztBQUNsR3dFLGdCQUFRLENBQUMvRCxvQkFBVCxHQUFnQytELFFBQVEsQ0FBQ3hFLEdBQXpDO0FBQ0g7O0FBRUQsVUFBSSxDQUFDd0UsUUFBUSxDQUFDdEQsb0JBQWQsRUFBb0M7QUFDaENxRixlQUFPLENBQUNrQixHQUFSLENBQVksNkRBQVo7QUFDQSxlQUFPLElBQVA7QUFDSCxPQXhEa0IsQ0EwRG5COzs7QUFDQWpELGNBQVEsQ0FBQzFFLFdBQVQsQ0FBcUIxQixVQUFyQixFQTNEbUIsQ0E2RG5COztBQUNBLFVBQUlBLFVBQVUsQ0FBQ2dHLElBQVgsQ0FBZ0IsV0FBaEIsTUFBaUMsSUFBckMsRUFBMkM7QUFDdkNtQyxlQUFPLENBQUNrQixHQUFSLENBQVksNEhBQVo7QUFDQSxlQUFPLElBQVA7QUFDSCxPQWpFa0IsQ0FtRW5CO0FBQ0E7OztBQUNBLFVBQUlySixVQUFVLENBQUNnRyxJQUFYLENBQWdCLGdCQUFoQixNQUFzQ3hCLFNBQTFDLEVBQXFEO0FBQ2pENEIsZ0JBQVEsQ0FBQ3BFLGNBQVQsR0FBMEJoQyxVQUFVLENBQUNnRyxJQUFYLENBQWdCLGdCQUFoQixDQUExQjtBQUNIOztBQUNELFVBQUloRyxVQUFVLENBQUNnRyxJQUFYLENBQWdCLFdBQWhCLE1BQWlDeEIsU0FBckMsRUFBZ0Q7QUFDNUM0QixnQkFBUSxDQUFDdkYsU0FBVCxHQUFxQmIsVUFBVSxDQUFDZ0csSUFBWCxDQUFnQixXQUFoQixDQUFyQjtBQUNBSSxnQkFBUSxDQUFDOUUsZUFBVCxHQUEyQnRCLFVBQVUsQ0FBQ2dHLElBQVgsQ0FBZ0IsV0FBaEIsSUFBK0JJLFFBQVEsQ0FBQzlFLGVBQXhDLEdBQTBELEtBQXJGO0FBQ0g7O0FBQ0QsVUFBSXRCLFVBQVUsQ0FBQ2dHLElBQVgsQ0FBZ0IsY0FBaEIsTUFBb0N4QixTQUF4QyxFQUFtRDtBQUMvQzRCLGdCQUFRLENBQUNuRixZQUFULEdBQXdCakIsVUFBVSxDQUFDZ0csSUFBWCxDQUFnQixjQUFoQixDQUF4QjtBQUNIOztBQUNELFVBQUloRyxVQUFVLENBQUNnRyxJQUFYLENBQWdCLGFBQWhCLE1BQW1DeEIsU0FBdkMsRUFBa0Q7QUFDOUM0QixnQkFBUSxDQUFDbkUsV0FBVCxHQUF1QmpDLFVBQVUsQ0FBQ2dHLElBQVgsQ0FBZ0IsYUFBaEIsQ0FBdkI7QUFDSCxPQWpGa0IsQ0FtRm5CO0FBQ0E7OztBQUNBLFVBQUksQ0FBQ0ksUUFBUSxDQUFDbkUsV0FBZCxFQUEyQjtBQUN2QmtHLGVBQU8sQ0FBQ2tCLEdBQVIsQ0FBWSw0R0FBWjtBQUNBbEIsZUFBTyxDQUFDa0IsR0FBUixDQUFZLDBFQUFaO0FBQ0FsQixlQUFPLENBQUNrQixHQUFSLENBQVksc0dBQVo7QUFDQSxlQUFPLElBQVA7QUFDSCxPQTFGa0IsQ0E0Rm5CO0FBQ0E7OztBQUNBLFVBQUlqRCxRQUFRLENBQUNsRCxjQUFiLEVBQTZCO0FBQ3pCa0QsZ0JBQVEsQ0FBQ2hHLFFBQVQsR0FBb0IsS0FBcEI7QUFDQWdHLGdCQUFRLENBQUMzRixVQUFULEdBQXNCLEtBQXRCO0FBQ0EyRixnQkFBUSxDQUFDN0QsU0FBVCxHQUFxQixLQUFyQjtBQUNBNkQsZ0JBQVEsQ0FBQ3RFLGNBQVQsR0FBMEIsSUFBMUI7QUFDSCxPQW5Ha0IsQ0FxR25CO0FBQ0E7QUFDQTs7O0FBQ0EsVUFBSyxPQUFPa0ssTUFBTSxDQUFDckosRUFBZCxLQUFxQixXQUFyQixJQUFvQyxPQUFPcUosTUFBTSxDQUFDckosRUFBUCxDQUFVc0osUUFBakIsS0FBOEIsV0FBbkUsSUFDR2pNLFVBQVUsQ0FBQ2dHLElBQVgsQ0FBZ0IsVUFBaEIsQ0FEUCxFQUNvQztBQUNoQ2hHLGtCQUFVLENBQUNpTSxRQUFYLENBQW9CLFNBQXBCO0FBQ0g7O0FBQ0QsVUFBSTdGLFFBQVEsQ0FBQzdELFNBQVQsSUFBc0I2RCxRQUFRLENBQUNoRyxRQUEvQixJQUEyQ2dHLFFBQVEsQ0FBQzNGLFVBQXhELEVBQW9FO0FBQ2hFLFlBQUl5TCxXQUFKO0FBQ0EsWUFBSUMsV0FBSjs7QUFDQSxZQUFJLE9BQU9ILE1BQU0sQ0FBQ3JKLEVBQWQsS0FBcUIsV0FBckIsSUFBb0MsT0FBT3FKLE1BQU0sQ0FBQ3JKLEVBQVAsQ0FBVXNKLFFBQWpCLEtBQThCLFdBQXRFLEVBQW1GO0FBQy9FN0Ysa0JBQVEsQ0FBQzdELFNBQVQsR0FBcUIsS0FBckI7QUFDSCxTQUZELE1BRU87QUFDSHZDLG9CQUFVLENBQUNpTSxRQUFYLENBQW9Cbk0sQ0FBQyxDQUFDZ00sTUFBRixDQUFTLElBQVQsRUFBZSxFQUFmLEVBQW1CO0FBQ25DTSxpQkFBSyxFQUFFLGVBQVUxSixLQUFWLEVBQWlCQyxFQUFqQixFQUFxQjtBQUN4QixrQkFBSXdDLFFBQVEsR0FBR25GLFVBQVUsQ0FBQ2tHLElBQVgsQ0FBZ0JFLFFBQVEsQ0FBQ2xFLGlCQUF6QixDQUFmO0FBQ0Esa0JBQUkzQixPQUFPLEdBQUdvQyxFQUFFLENBQUMwSixJQUFqQjtBQUNBLGtCQUFJL0MsSUFBSSxHQUFHeEosQ0FBQyxDQUFDLElBQUQsQ0FBWjs7QUFDQSxrQkFBSSxDQUFDaUYsZUFBZSxDQUFDcUIsUUFBUSxDQUFDM0QsZUFBVCxDQUF5QkMsS0FBekIsRUFBZ0NDLEVBQWhDLEVBQW9Dd0MsUUFBcEMsRUFBOEM1RSxPQUE5QyxDQUFELENBQXBCLEVBQThFO0FBQzFFK0ksb0JBQUksQ0FBQzJDLFFBQUwsQ0FBYyxRQUFkO0FBQ0E7QUFDSDs7QUFDRHRKLGdCQUFFLENBQUMySixXQUFILENBQWVDLE1BQWYsQ0FBc0I1SixFQUFFLENBQUMwSixJQUFILENBQVFFLE1BQVIsRUFBdEI7QUFDQTVKLGdCQUFFLENBQUMySixXQUFILENBQWVFLEtBQWYsQ0FBcUI3SixFQUFFLENBQUMwSixJQUFILENBQVFHLEtBQVIsRUFBckI7QUFDQU4seUJBQVcsR0FBRy9HLFFBQVEsQ0FBQ0MsS0FBVCxDQUFlN0UsT0FBZixDQUFkO0FBQ0gsYUFaa0M7QUFhbkNrTSxrQkFBTSxFQUFFLGdCQUFVL0osS0FBVixFQUFpQkMsRUFBakIsRUFBcUI7QUFDekIsa0JBQUl3QyxRQUFRLEdBQUduRixVQUFVLENBQUNrRyxJQUFYLENBQWdCRSxRQUFRLENBQUNsRSxpQkFBekIsQ0FBZjtBQUNBLGtCQUFJM0IsT0FBTyxHQUFHb0MsRUFBRSxDQUFDMEosSUFBakI7QUFDQSxrQkFBSS9DLElBQUksR0FBR3hKLENBQUMsQ0FBQyxJQUFELENBQVo7QUFDQXdKLGtCQUFJLENBQUMyQyxRQUFMLENBQWMsUUFBZDs7QUFDQSxrQkFBSSxVQUFVN0YsUUFBUSxDQUFDeEQsZ0JBQVQsQ0FBMEJGLEtBQTFCLEVBQWlDQyxFQUFqQyxFQUFxQ3dDLFFBQXJDLEVBQStDNUUsT0FBL0MsQ0FBVixJQUFxRSxFQUFFNEwsV0FBVyxHQUFHRCxXQUFkLEdBQTRCLENBQTVCLEdBQWdDbkgsZUFBZSxDQUFDcUIsUUFBUSxDQUFDOUYsU0FBVCxDQUFtQk4sVUFBbkIsRUFBK0JPLE9BQS9CLENBQUQsQ0FBL0MsR0FBMkZ3RSxlQUFlLENBQUNxQixRQUFRLENBQUN6RixXQUFULENBQXFCWCxVQUFyQixFQUFpQ08sT0FBakMsQ0FBRCxDQUE1RyxDQUF6RSxFQUFtTztBQUMvTjtBQUNIOztBQUNENEwseUJBQVcsR0FBR2hILFFBQVEsQ0FBQ0MsS0FBVCxDQUFlN0UsT0FBZixDQUFkO0FBQ0E0RSxzQkFBUSxHQUFHbkYsVUFBVSxDQUFDa0csSUFBWCxDQUFnQkUsUUFBUSxDQUFDbEUsaUJBQXpCLENBQVg7QUFDQXVJLG9CQUFNLENBQUN6SyxVQUFELEVBQWFvRyxRQUFiLEVBQXVCakIsUUFBdkIsRUFBaUM1RSxPQUFqQyxFQUEwQzJMLFdBQTFDLEVBQXVEQyxXQUF2RCxDQUFOO0FBQ0g7QUF4QmtDLFdBQW5CLEVBeUJqQi9GLFFBQVEsQ0FBQzVELGlCQXpCUSxDQUFwQjtBQTBCSDtBQUNKOztBQUVEeEMsZ0JBQVUsQ0FBQ2dHLElBQVgsQ0FBZ0IscUJBQWhCLEVBQXVDSSxRQUF2QyxFQS9JbUIsQ0FpSm5CO0FBQ0E7O0FBQ0EsVUFBSWpHLFNBQVMsR0FBR0wsQ0FBQyxDQUFDc0csUUFBUSxDQUFDakcsU0FBVixDQUFqQjtBQUNBQSxlQUFTLENBQ0p1TSxHQURMLENBQ1MsT0FEVCxFQUNrQixNQUFNdEcsUUFBUSxDQUFDckUsTUFBZixHQUF3QixTQUQxQyxFQUVLNEssRUFGTCxDQUVRLE9BRlIsRUFFaUIsTUFBTXZHLFFBQVEsQ0FBQ3JFLE1BQWYsR0FBd0IsU0FGekMsRUFFb0QsVUFBVXNDLENBQVYsRUFBYTtBQUV6RCxZQUFJaUYsSUFBSSxHQUFHeEosQ0FBQyxDQUFDLElBQUQsQ0FBWjtBQUVBLFlBQUlFLFVBQVUsR0FBR0YsQ0FBQyxDQUFDLE1BQU13SixJQUFJLENBQUN0RCxJQUFMLENBQVUsWUFBVixDQUFQLENBQWxCO0FBQ0EsWUFBSUksUUFBUSxHQUFHcEcsVUFBVSxDQUFDZ0csSUFBWCxDQUFnQixxQkFBaEIsQ0FBZjs7QUFFQSxZQUFJeEIsU0FBUyxLQUFLNEIsUUFBbEIsRUFBNEI7QUFDeEIsY0FBSXBHLFVBQVUsR0FBR0YsQ0FBQyxDQUFDLE1BQU13SixJQUFJLENBQUN0RCxJQUFMLENBQVUsWUFBVixDQUFQLENBQUQsQ0FBaUNFLElBQWpDLENBQXNDLE1BQU1vRCxJQUFJLENBQUN0RCxJQUFMLENBQVUsWUFBVixDQUFOLEdBQWdDLGFBQXRFLENBQWpCO0FBQ0EsY0FBSUksUUFBUSxHQUFHcEcsVUFBVSxDQUFDZ0csSUFBWCxDQUFnQixxQkFBaEIsQ0FBZjs7QUFDQSxjQUFJeEIsU0FBUyxLQUFLNEIsUUFBbEIsRUFBNEI7QUFDeEIsa0JBQU0sNEJBQTRCa0QsSUFBSSxDQUFDdEQsSUFBTCxDQUFVLFlBQVYsQ0FBbEM7QUFDSDtBQUNKOztBQUVELFlBQUliLFFBQVEsR0FBR25GLFVBQVUsQ0FBQ2tHLElBQVgsQ0FBZ0JFLFFBQVEsQ0FBQ2xFLGlCQUF6QixDQUFmO0FBQ0EsWUFBSTNCLE9BQU8sR0FBRytJLElBQUksQ0FBQ3RELElBQUwsQ0FBVSxTQUFWLElBQXVCbEcsQ0FBQyxDQUFDLE1BQU13SixJQUFJLENBQUN0RCxJQUFMLENBQVUsU0FBVixDQUFQLENBQXhCLEdBQXVEeEIsU0FBckU7QUFDQSxZQUFJWSxLQUFLLEdBQUc3RSxPQUFPLElBQUlBLE9BQU8sQ0FBQzBELE1BQW5CLEdBQTRCa0IsUUFBUSxDQUFDQyxLQUFULENBQWU3RSxPQUFmLENBQTVCLEdBQXNELENBQUMsQ0FBbkU7QUFDQSxZQUFJbUMsS0FBSyxHQUFHLElBQVo7QUFFQSxZQUFJNkcsV0FBVyxHQUFHRCxJQUFJLENBQUNoRixFQUFMLENBQVEsTUFBTThCLFFBQVEsQ0FBQ3JFLE1BQWYsR0FBd0IsWUFBaEMsQ0FBbEI7O0FBQ0EsWUFBSSxDQUFDdUgsSUFBSSxDQUFDaEYsRUFBTCxDQUFRLE1BQU04QixRQUFRLENBQUNyRSxNQUFmLEdBQXdCLE1BQWhDLEtBQTJDdUgsSUFBSSxDQUFDaEYsRUFBTCxDQUFRLE1BQU04QixRQUFRLENBQUNyRSxNQUFmLEdBQXdCLGFBQWhDLENBQTNDLElBQTZGd0gsV0FBOUYsS0FBOEduRCxRQUFRLENBQUN2RixTQUEzSCxFQUFzSTtBQUNsSTZCLGVBQUssR0FBRyxLQUFSO0FBQ0F5QyxrQkFBUSxHQUFHK0MsS0FBSyxDQUFDL0gsU0FBRCxFQUFZbUosSUFBWixFQUFrQnRKLFVBQWxCLEVBQThCb0csUUFBOUIsRUFBd0NqQixRQUF4QyxFQUFrRDVFLE9BQWxELEVBQTJENkUsS0FBM0QsRUFBa0VtRSxXQUFsRSxDQUFoQjtBQUNIOztBQUVELFlBQUlELElBQUksQ0FBQ2hGLEVBQUwsQ0FBUSxNQUFNOEIsUUFBUSxDQUFDckUsTUFBZixHQUF3QixTQUFoQyxLQUE4Q3FFLFFBQVEsQ0FBQ25GLFlBQTNELEVBQXlFO0FBQ3JFeUIsZUFBSyxHQUFHLFFBQVI7QUFDQXlDLGtCQUFRLEdBQUd3RixRQUFRLENBQUMzSyxVQUFELEVBQWFvRyxRQUFiLEVBQXVCakIsUUFBdkIsRUFBaUM1RSxPQUFqQyxFQUEwQzZFLEtBQTFDLENBQW5CO0FBQ0g7O0FBRUQsWUFBSWtFLElBQUksQ0FBQ2hGLEVBQUwsQ0FBUSxNQUFNOEIsUUFBUSxDQUFDckUsTUFBZixHQUF3QixLQUFoQyxLQUEwQ3FFLFFBQVEsQ0FBQ2hHLFFBQXZELEVBQWlFO0FBQzdEc0MsZUFBSyxHQUFHLElBQVI7QUFDQXlDLGtCQUFRLEdBQUcrRixJQUFJLENBQUNsTCxVQUFELEVBQWFvRyxRQUFiLEVBQXVCakIsUUFBdkIsRUFBaUM1RSxPQUFqQyxFQUEwQzZFLEtBQTFDLENBQWY7QUFDSDs7QUFFRCxZQUFJa0UsSUFBSSxDQUFDaEYsRUFBTCxDQUFRLE1BQU04QixRQUFRLENBQUNyRSxNQUFmLEdBQXdCLE9BQWhDLEtBQTRDcUUsUUFBUSxDQUFDM0YsVUFBekQsRUFBcUU7QUFDakVpQyxlQUFLLEdBQUcsTUFBUjtBQUNBeUMsa0JBQVEsR0FBR2dHLE1BQU0sQ0FBQ25MLFVBQUQsRUFBYW9HLFFBQWIsRUFBdUJqQixRQUF2QixFQUFpQzVFLE9BQWpDLEVBQTBDNkUsS0FBMUMsQ0FBakI7QUFDSDs7QUFFRG9DLDZCQUFxQixDQUFDeEgsVUFBRCxFQUFhb0csUUFBYixFQUF1QixLQUF2QixFQUE4QjFELEtBQTlCLENBQXJCO0FBQ0EyQixTQUFDLENBQUN1SSxjQUFGO0FBQ0gsT0E3Q0wsRUFwSm1CLENBaU1YOztBQUVScEYsMkJBQXFCLENBQUN4SCxVQUFELEVBQWFvRyxRQUFiLEVBQXVCLElBQXZCLENBQXJCO0FBQ0ErQywrQkFBeUIsQ0FBQ25KLFVBQUQsRUFBYSxJQUFiLEVBQW1Cb0csUUFBbkIsQ0FBekIsQ0FwTW1CLENBc01uQjtBQUNBOztBQUNBLFVBQUlBLFFBQVEsQ0FBQ25ELHVCQUFiLEVBQXNDO0FBQ2xDLFlBQUk0SixLQUFLLEdBQUcsRUFBWjtBQUNBLFlBQUkxSCxRQUFRLEdBQUduRixVQUFVLENBQUNrRyxJQUFYLENBQWdCRSxRQUFRLENBQUNsRSxpQkFBekIsQ0FBZjtBQUNBaUQsZ0JBQVEsQ0FBQ08sSUFBVCxDQUFjLFVBQVVOLEtBQVYsRUFBaUI7QUFDM0IsY0FBSWtFLElBQUksR0FBR3hKLENBQUMsQ0FBQyxJQUFELENBQVo7QUFDQStNLGVBQUssQ0FBQ0MsSUFBTixDQUFXO0FBQ1BDLG9CQUFRLEVBQUVDLFVBQVUsQ0FBQzlJLGFBQWEsQ0FBQ29GLElBQUksQ0FBQ3BELElBQUwsQ0FBVUUsUUFBUSxDQUFDbkQsdUJBQW5CLENBQUQsQ0FBZCxDQURiO0FBRVAxQyxtQkFBTyxFQUFFK0k7QUFGRixXQUFYO0FBSUgsU0FORDs7QUFRQSxZQUFJMkQsTUFBTSxHQUFHLFNBQVRBLE1BQVMsQ0FBVUMsQ0FBVixFQUFhQyxDQUFiLEVBQWdCO0FBQ3pCLGlCQUFRRCxDQUFDLENBQUNILFFBQUYsR0FBYUksQ0FBQyxDQUFDSixRQUFmLEdBQTBCLENBQUMsQ0FBM0IsR0FBZ0NHLENBQUMsQ0FBQ0gsUUFBRixHQUFhSSxDQUFDLENBQUNKLFFBQWYsR0FBMEIsQ0FBMUIsR0FBOEIsQ0FBdEU7QUFDSCxTQUZEOztBQUdBRixhQUFLLENBQUNsSixJQUFOLENBQVdzSixNQUFYO0FBRUFuTixTQUFDLENBQUM0RixJQUFGLENBQU9tSCxLQUFQLEVBQWMsVUFBVXZHLFFBQVYsRUFBb0I4RyxNQUFwQixFQUE0QjtBQUN0QyxjQUFJQyxHQUFHLEdBQUcsRUFBVjtBQUNBdk4sV0FBQyxDQUFDcUYsUUFBRCxDQUFELENBQVlPLElBQVosQ0FBaUIsVUFBVU4sS0FBVixFQUFpQjtBQUM5QmlJLGVBQUcsQ0FBQ1AsSUFBSixDQUFTaE4sQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRaUUsSUFBUixDQUFhLElBQWIsQ0FBVDtBQUNILFdBRkQ7QUFJQSxjQUFJeEQsT0FBTyxHQUFHNk0sTUFBTSxDQUFDN00sT0FBckI7QUFDQSxjQUFJOEYsUUFBUSxHQUFHdkcsQ0FBQyxDQUFDd04sT0FBRixDQUFVL00sT0FBTyxDQUFDd0QsSUFBUixDQUFhLElBQWIsQ0FBVixFQUE4QnNKLEdBQTlCLENBQWY7O0FBRUEsY0FBSS9HLFFBQVEsS0FBS0QsUUFBakIsRUFBMkI7QUFDdkJsQixvQkFBUSxHQUFHc0YsTUFBTSxDQUFDekssVUFBRCxFQUFhb0csUUFBYixFQUF1QmpCLFFBQXZCLEVBQWlDNUUsT0FBakMsRUFBMEM4RixRQUExQyxFQUFvREMsUUFBcEQsQ0FBakI7QUFDQTNCLHlCQUFhLENBQUNwRSxPQUFPLENBQUMyRixJQUFSLENBQWFFLFFBQVEsQ0FBQ25ELHVCQUF0QixDQUFELEVBQWlEa0MsUUFBUSxDQUFDQyxLQUFULENBQWU3RSxPQUFmLENBQWpELENBQWI7QUFDSDtBQUNKLFNBYkQ7QUFjSCxPQXRPa0IsQ0FzT2pCOzs7QUFFRjZGLGNBQVEsQ0FBQ3pFLFVBQVQsQ0FBb0IzQixVQUFwQjtBQUVILEtBMU9ELEVBcnVCaUMsQ0ErOEI3Qjs7QUFFSixXQUFPLElBQVA7QUFDSCxHQWw5QkQsQ0FGVSxDQW85QlA7O0FBRU4sQ0F0OUJELEVBdTlCQ2dNLE1BdjlCRCxFIiwiZmlsZSI6ImpxdWVyeS5jb2xsZWN0aW9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIGpxdWVyeS5jb2xsZWN0aW9uLmpzXG4gKlxuICogQ29weXJpZ2h0IChjKSAyMDQyIGFsYWluIHRpZW1ibG8gPGFsYWluIGF0IGZ1eiBkb3Qgb3JnPlxuICpcbiAqIFBlcm1pc3Npb24gaXMgaGVyZWJ5IGdyYW50ZWQsIGZyZWUgb2YgY2hhcmdlLCB0byBhbnkgcGVyc29uIG9idGFpbmluZyBhIGNvcHkgb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGVcbiAqIFwiU29mdHdhcmVcIiksIHRvIGRlYWwgaW4gdGhlIFNvZnR3YXJlIHdpdGhvdXQgcmVzdHJpY3Rpb24sIGluY2x1ZGluZyB3aXRob3V0IGxpbWl0YXRpb24gdGhlIHJpZ2h0cyB0byB1c2UsIGNvcHksIG1vZGlmeSwgbWVyZ2UsIHB1Ymxpc2gsXG4gKiBkaXN0cmlidXRlLCBzdWJsaWNlbnNlLCBhbmQvb3Igc2VsbCBjb3BpZXMgb2YgdGhlIFNvZnR3YXJlLCBhbmQgdG8gcGVybWl0IHBlcnNvbnMgdG8gd2hvbSB0aGUgU29mdHdhcmUgaXMgZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZVxuICogZm9sbG93aW5nIGNvbmRpdGlvbnM6XG4gKlxuICogVGhlIGFib3ZlIGNvcHlyaWdodCBub3RpY2UgYW5kIHRoaXMgcGVybWlzc2lvbiBub3RpY2Ugc2hhbGwgYmUgaW5jbHVkZWQgaW4gYWxsIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG4gKlxuICogVEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTUyBPUiBJTVBMSUVELCBJTkNMVURJTkcgQlVUIE5PVCBMSU1JVEVEIFRPIFRIRSBXQVJSQU5USUVTIE9GXG4gKiBNRVJDSEFOVEFCSUxJVFksIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFIEFORCBOT05JTkZSSU5HRU1FTlQuIElOIE5PIEVWRU5UIFNIQUxMIFRIRSBBVVRIT1JTIE9SIENPUFlSSUdIVCBIT0xERVJTIEJFIExJQUJMRSBGT1IgQU5ZXG4gKiBDTEFJTSwgREFNQUdFUyBPUiBPVEhFUiBMSUFCSUxJVFksIFdIRVRIRVIgSU4gQU4gQUNUSU9OIE9GIENPTlRSQUNULCBUT1JUIE9SIE9USEVSV0lTRSwgQVJJU0lORyBGUk9NLCBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRVxuICogT1IgVEhFIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEUgU09GVFdBUkUuXG4gKi9cblxuO1xuKGZ1bmN0aW9uICgkKSB7XG5cbiAgICAkLmZuLmNvbGxlY3Rpb24gPSBmdW5jdGlvbiAob3B0aW9ucykge1xuXG4gICAgICAgIHZhciBkZWZhdWx0cyA9IHtcbiAgICAgICAgICAgIGNvbnRhaW5lcjogJ2JvZHknLFxuICAgICAgICAgICAgYWxsb3dfdXA6IHRydWUsXG4gICAgICAgICAgICB1cDogJzxhIGhyZWY9XCIjXCI+JiN4MjVCMjs8L2E+JyxcbiAgICAgICAgICAgIGJlZm9yZV91cDogZnVuY3Rpb24gKGNvbGxlY3Rpb24sIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhZnRlcl91cDogZnVuY3Rpb24gKGNvbGxlY3Rpb24sIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhbGxvd19kb3duOiB0cnVlLFxuICAgICAgICAgICAgZG93bjogJzxhIGhyZWY9XCIjXCI+JiN4MjVCQzs8L2E+JyxcbiAgICAgICAgICAgIGJlZm9yZV9kb3duOiBmdW5jdGlvbiAoY29sbGVjdGlvbiwgZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGFmdGVyX2Rvd246IGZ1bmN0aW9uIChjb2xsZWN0aW9uLCBlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYWxsb3dfYWRkOiB0cnVlLFxuICAgICAgICAgICAgYWRkOiAnPGEgaHJlZj1cIiNcIiBjbGFzcz1cImJ0biBidG4td2FybmluZyBtYi0zXCIgPkFkZDwvYT4nLFxuICAgICAgICAgICAgYmVmb3JlX2FkZDogZnVuY3Rpb24gKGNvbGxlY3Rpb24sIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhZnRlcl9hZGQ6IGZ1bmN0aW9uIChjb2xsZWN0aW9uLCBlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYWxsb3dfcmVtb3ZlOiB0cnVlLFxuICAgICAgICAgICAgcmVtb3ZlOiAnPGEgaHJlZj1cIiNcIiBjbGFzcz1cImJ0biBidG4tZGFuZ2VyIG1yLTIgdy0yNSBtYi0zXCI+RGVsZXRlPC9hPicsXG4gICAgICAgICAgICBiZWZvcmVfcmVtb3ZlOiBmdW5jdGlvbiAoY29sbGVjdGlvbiwgZWxlbWVudCkge1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIGNvbmZpcm0oJ0FyZSB5b3Ugc3VyZSB0byBkZWxldGUgdGhpcyBsaW5rPycpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGFmdGVyX3JlbW92ZTogZnVuY3Rpb24gKGNvbGxlY3Rpb24sIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhbGxvd19kdXBsaWNhdGU6IGZhbHNlLFxuICAgICAgICAgICAgZHVwbGljYXRlOiAnPGEgaHJlZj1cIiNcIj5bICMgXTwvYT4nLFxuICAgICAgICAgICAgYmVmb3JlX2R1cGxpY2F0ZTogZnVuY3Rpb24gKGNvbGxlY3Rpb24sIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhZnRlcl9kdXBsaWNhdGU6IGZ1bmN0aW9uIChjb2xsZWN0aW9uLCBlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYmVmb3JlX2luaXQ6IGZ1bmN0aW9uIChjb2xsZWN0aW9uKSB7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYWZ0ZXJfaW5pdDogZnVuY3Rpb24gKGNvbGxlY3Rpb24pIHtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBtaW46IDEsXG4gICAgICAgICAgICBtYXg6IDQsXG4gICAgICAgICAgICBhZGRfYXRfdGhlX2VuZDogdHJ1ZSxcbiAgICAgICAgICAgIHByZWZpeDogJ2NvbGxlY3Rpb24nLFxuICAgICAgICAgICAgcHJvdG90eXBlX25hbWU6ICdfX25hbWVfXycsXG4gICAgICAgICAgICBuYW1lX3ByZWZpeDogbnVsbCxcbiAgICAgICAgICAgIGVsZW1lbnRzX3NlbGVjdG9yOiAnPiBkaXYsID4gZmllbGRzZXQnLFxuICAgICAgICAgICAgZWxlbWVudHNfcGFyZW50X3NlbGVjdG9yOiAnJWlkJScsXG4gICAgICAgICAgICBjaGlsZHJlbjogbnVsbCxcbiAgICAgICAgICAgIGluaXRfd2l0aF9uX2VsZW1lbnRzOiAwLFxuICAgICAgICAgICAgaGlkZV91c2VsZXNzX2J1dHRvbnM6IHRydWUsXG4gICAgICAgICAgICBkcmFnX2Ryb3A6IHRydWUsXG4gICAgICAgICAgICBkcmFnX2Ryb3Bfb3B0aW9uczoge1xuICAgICAgICAgICAgICAgICdwbGFjZWhvbGRlcic6ICd1aS1zdGF0ZS1oaWdobGlnaHQnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZHJhZ19kcm9wX3N0YXJ0OiBmdW5jdGlvbiAoZXZlbnQsIHVpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZHJhZ19kcm9wX3VwZGF0ZTogZnVuY3Rpb24gKGV2ZW50LCB1aSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGN1c3RvbV9hZGRfbG9jYXRpb246IGZhbHNlLFxuICAgICAgICAgICAgYWN0aW9uX2NvbnRhaW5lcl90YWc6ICdkaXYnLFxuICAgICAgICAgICAgZmFkZV9pbjogdHJ1ZSxcbiAgICAgICAgICAgIGZhZGVfb3V0OiB0cnVlLFxuICAgICAgICAgICAgcG9zaXRpb25fZmllbGRfc2VsZWN0b3I6IG51bGwsXG4gICAgICAgICAgICBwcmVzZXJ2ZV9uYW1lczogZmFsc2VcbiAgICAgICAgfTtcblxuICAgICAgICAvLyB1c2VkIHRvIGdlbmVyYXRlIHJhbmRvbSBpZCBhdHRyaWJ1dGVzIHdoZW4gcmVxdWlyZWQgYW5kIG1pc3NpbmdcbiAgICAgICAgdmFyIHJhbmRvbU51bWJlciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciByYW5kID0gJycgKyBNYXRoLnJhbmRvbSgpICogMTAwMCAqIG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgICAgICAgcmV0dXJuIHJhbmQucmVwbGFjZSgnLicsICcnKS5zcGxpdCgnJykuc29ydChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIDAuNSAtIE1hdGgucmFuZG9tKCk7XG4gICAgICAgICAgICB9KS5qb2luKCcnKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvLyByZXR1cm4gYW4gZWxlbWVudCdzIGlkLCBhZnRlciBnZW5lcmF0aW5nIG9uZSB3aGVuIG1pc3NpbmdcbiAgICAgICAgdmFyIGdldE9yQ3JlYXRlSWQgPSBmdW5jdGlvbiAocHJlZml4LCBvYmopIHtcbiAgICAgICAgICAgIGlmICghb2JqLmF0dHIoJ2lkJykpIHtcbiAgICAgICAgICAgICAgICB2YXIgZ2VuZXJhdGVkX2lkO1xuICAgICAgICAgICAgICAgIGRvIHtcbiAgICAgICAgICAgICAgICAgICAgZ2VuZXJhdGVkX2lkID0gcHJlZml4ICsgJ18nICsgcmFuZG9tTnVtYmVyKCk7XG4gICAgICAgICAgICAgICAgfSB3aGlsZSAoJCgnIycgKyBnZW5lcmF0ZWRfaWQpLmxlbmd0aCA+IDApO1xuICAgICAgICAgICAgICAgIG9iai5hdHRyKCdpZCcsIGdlbmVyYXRlZF9pZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gb2JqLmF0dHIoJ2lkJyk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gcmV0dXJuIGEgZmllbGQgdmFsdWUgd2hhdGV2ZXIgdGhlIGZpZWxkIHR5cGVcbiAgICAgICAgdmFyIGdldEZpZWxkVmFsdWUgPSBmdW5jdGlvbiAoc2VsZWN0b3IpIHtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgdmFyIGpxRWxlbSA9ICQoc2VsZWN0b3IpO1xuICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGpxRWxlbS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoanFFbGVtLmlzKCdpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0nKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiAoanFFbGVtLnByb3AoJ2NoZWNrZWQnKSA9PT0gdHJ1ZSA/IHRydWUgOiBmYWxzZSk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGpxRWxlbS5pcygnaW5wdXRbdHlwZT1cInJhZGlvXCJdJykgJiYganFFbGVtLmF0dHIoJ25hbWUnKSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICQoJ2lucHV0W25hbWU9XCInICsganFFbGVtLmF0dHIoJ25hbWUnKSArICdcIl06Y2hlY2tlZCcpLnZhbCgpO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChqcUVsZW0ucHJvcCgndmFsdWUnKSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGpxRWxlbS52YWwoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGpxRWxlbS5odG1sKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gc2V0IGEgZmllbGQgdmFsdWUgaW4gYWNjb3JkYW5jZSB0byB0aGUgZmllbGQgdHlwZVxuICAgICAgICB2YXIgcHV0RmllbGRWYWx1ZSA9IGZ1bmN0aW9uIChzZWxlY3RvciwgdmFsdWUsIHBoeXNpY2FsKSB7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgIHZhciBqcUVsZW0gPSAkKHNlbGVjdG9yKTtcbiAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoanFFbGVtLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoanFFbGVtLmlzKCdpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0nKSkge1xuICAgICAgICAgICAgICAgIGlmICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICBqcUVsZW0uYXR0cignY2hlY2tlZCcsIHRydWUpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGpxRWxlbS5yZW1vdmVBdHRyKCdjaGVja2VkJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIGlmIChqcUVsZW0ucHJvcCgndmFsdWUnKSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgaWYgKHBoeXNpY2FsKSB7XG4gICAgICAgICAgICAgICAgICAgIGpxRWxlbS5hdHRyKCd2YWx1ZScsIHZhbHVlKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBqcUVsZW0udmFsKHZhbHVlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGpxRWxlbS5odG1sKHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAvLyBhIGNhbGxiYWNrIHNldCBpbiBhbiBldmVudCB3aWxsIGJlIGNvbnNpZGVyZWQgZmFpbGVkIGlmIGl0XG4gICAgICAgIC8vIHJldHVybnMgZmFsc2UsIG51bGwsIG9yIDAuXG4gICAgICAgIHZhciB0cnVlT3JVbmRlZmluZWQgPSBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgIHJldHVybiB1bmRlZmluZWQgPT09IHZhbHVlIHx8IHZhbHVlO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8vIHVzZWQgdG8gY2hhbmdlIGVsZW1lbnQgaW5kZXhlcyBpbiBhcmJpdGFyeSBpZCBhdHRyaWJ1dGVzXG4gICAgICAgIHZhciBwcmVnUXVvdGUgPSBmdW5jdGlvbiAoc3RyaW5nKSB7XG4gICAgICAgICAgICByZXR1cm4gKHN0cmluZyArICcnKS5yZXBsYWNlKC9bLj8qK14kW1xcXVxcXFwoKXt9fC1dL2csIFwiXFxcXCQmXCIpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8vIGlmIHdlIG5lZWQgdG8gY2hhbmdlIENvbGxlY3Rpb25UeXBlX2ZpZWxkXzQyX3ZhbHVlIHRvIENvbGxlY3Rpb25UeXBlX2ZpZWxkXzg0X3ZhbHVlLCB0aGlzIG1ldGhvZFxuICAgICAgICAvLyB3aWxsIGNoYW5nZSBpdCBpbiBpZD1cIkNvbGxlY3Rpb25UeXBlX2ZpZWxkXzQyX3ZhbHVlXCIsIGJ1dCBhbHNvIGRhdGEtaWQ9XCJDb2xsZWN0aW9uVHlwZV9maWVsZF80Ml92YWx1ZVwiXG4gICAgICAgIC8vIG9yIGFueXdoZXJlIGVsc2UganVzdCBpbiBjYXNlIGl0IGNvdWxkIGJlIHVzZWQgb3RoZXJ3aXNlLlxuICAgICAgICB2YXIgcmVwbGFjZUF0dHJEYXRhID0gZnVuY3Rpb24gKGVsZW1lbnRzLCBpbmRleCwgdG9SZXBsYWNlLCByZXBsYWNlV2l0aCkge1xuXG4gICAgICAgICAgICB2YXIgcmVwbGFjZUF0dHJEYXRhTm9kZSA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICAgICAgICAgICAgdmFyIGpxTm9kZSA9ICQobm9kZSk7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBub2RlID09PSAnb2JqZWN0JyAmJiAnYXR0cmlidXRlcycgaW4gbm9kZSkge1xuICAgICAgICAgICAgICAgICAgICAkLmVhY2gobm9kZS5hdHRyaWJ1dGVzLCBmdW5jdGlvbiAoaSwgYXR0cmliKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoJC50eXBlKGF0dHJpYi52YWx1ZSkgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAganFOb2RlLmF0dHIoYXR0cmliLm5hbWUucmVwbGFjZSh0b1JlcGxhY2UsIHJlcGxhY2VXaXRoKSwgYXR0cmliLnZhbHVlLnJlcGxhY2UodG9SZXBsYWNlLCByZXBsYWNlV2l0aCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKGpxTm9kZS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICQuZWFjaChqcU5vZGUuZGF0YSgpLCBmdW5jdGlvbiAobmFtZSwgdmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICgkLnR5cGUodmFsdWUpID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGpxTm9kZS5kYXRhKG5hbWUucmVwbGFjZSh0b1JlcGxhY2UsIHJlcGxhY2VXaXRoKSwgdmFsdWUucmVwbGFjZSh0b1JlcGxhY2UsIHJlcGxhY2VXaXRoKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIHZhciBlbGVtZW50ID0gZWxlbWVudHMuZXEoaW5kZXgpO1xuICAgICAgICAgICAgcmVwbGFjZUF0dHJEYXRhTm9kZShlbGVtZW50WzBdKTtcbiAgICAgICAgICAgIGVsZW1lbnQuZmluZCgnKicpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJlcGxhY2VBdHRyRGF0YU5vZGUodGhpcyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAvLyByZXBsYWNlIGVsZW1lbnQgbmFtZXMgYW5kIGluZGV4ZXMgaW4gdGhlIGNvbGxlY3Rpb24sIGluIFN5bWZvbnksIG5hbWVzIGFyZSBhbHdheXMgaW4gZm9ybWF0XG4gICAgICAgIC8vIENvbGxlY3Rpb25UeXBlW2ZpZWxkXVs0Ml1bdmFsdWVdIGFuZCBpZHMgYXJlIGluIGZvcm1hdCBDb2xsZWN0aW9uVHlwZV9maWVsZF80Ml92YWx1ZTtcbiAgICAgICAgLy8gc28gd2UgbmVlZCB0byBjaGFuZ2UgYm90aC5cbiAgICAgICAgdmFyIGNoYW5nZUVsZW1lbnRJbmRleCA9IGZ1bmN0aW9uIChjb2xsZWN0aW9uLCBlbGVtZW50cywgc2V0dGluZ3MsIGluZGV4LCBvbGRJbmRleCwgbmV3SW5kZXgpIHtcbiAgICAgICAgICAgIHZhciB0b1JlcGxhY2UgPSBuZXcgUmVnRXhwKHByZWdRdW90ZShzZXR0aW5ncy5uYW1lX3ByZWZpeCArICdbJyArIG9sZEluZGV4ICsgJ10nKSwgJ2cnKTtcbiAgICAgICAgICAgIHZhciByZXBsYWNlV2l0aCA9IHNldHRpbmdzLm5hbWVfcHJlZml4ICsgJ1snICsgbmV3SW5kZXggKyAnXSc7XG5cbiAgICAgICAgICAgIGlmIChzZXR0aW5ncy5jaGlsZHJlbikge1xuICAgICAgICAgICAgICAgICQuZWFjaChzZXR0aW5ncy5jaGlsZHJlbiwgZnVuY3Rpb24gKGtleSwgY2hpbGQpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGNoaWxkQ29sbGVjdGlvbiA9IGNvbGxlY3Rpb24uZmluZChjaGlsZC5zZWxlY3RvcikuZXEoaW5kZXgpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgY2hpbGRTZXR0aW5ncyA9IGNoaWxkQ29sbGVjdGlvbi5kYXRhKCdjb2xsZWN0aW9uLXNldHRpbmdzJyk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChjaGlsZFNldHRpbmdzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjaGlsZFNldHRpbmdzLm5hbWVfcHJlZml4ID0gY2hpbGRTZXR0aW5ncy5uYW1lX3ByZWZpeC5yZXBsYWNlKHRvUmVwbGFjZSwgcmVwbGFjZVdpdGgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgY2hpbGRDb2xsZWN0aW9uLmRhdGEoJ2NvbGxlY3Rpb24tc2V0dGluZ3MnLCBjaGlsZFNldHRpbmdzKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXBsYWNlQXR0ckRhdGEoZWxlbWVudHMsIGluZGV4LCB0b1JlcGxhY2UsIHJlcGxhY2VXaXRoKTtcblxuICAgICAgICAgICAgdG9SZXBsYWNlID0gbmV3IFJlZ0V4cChwcmVnUXVvdGUoY29sbGVjdGlvbi5hdHRyKCdpZCcpICsgJ18nICsgb2xkSW5kZXgpLCAnZycpO1xuICAgICAgICAgICAgcmVwbGFjZVdpdGggPSBjb2xsZWN0aW9uLmF0dHIoJ2lkJykgKyAnXycgKyBuZXdJbmRleDtcblxuICAgICAgICAgICAgaWYgKHNldHRpbmdzLmNoaWxkcmVuKSB7XG4gICAgICAgICAgICAgICAgJC5lYWNoKHNldHRpbmdzLmNoaWxkcmVuLCBmdW5jdGlvbiAoa2V5LCBjaGlsZCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgY2hpbGRDb2xsZWN0aW9uID0gY29sbGVjdGlvbi5maW5kKGNoaWxkLnNlbGVjdG9yKS5lcShpbmRleCk7XG4gICAgICAgICAgICAgICAgICAgIHZhciBjaGlsZFNldHRpbmdzID0gY2hpbGRDb2xsZWN0aW9uLmRhdGEoJ2NvbGxlY3Rpb24tc2V0dGluZ3MnKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNoaWxkU2V0dGluZ3MpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoaWxkU2V0dGluZ3MuZWxlbWVudHNfcGFyZW50X3NlbGVjdG9yID0gY2hpbGRTZXR0aW5ncy5lbGVtZW50c19wYXJlbnRfc2VsZWN0b3IucmVwbGFjZSh0b1JlcGxhY2UsIHJlcGxhY2VXaXRoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoaWxkU2V0dGluZ3MuZWxlbWVudHNfc2VsZWN0b3IgPSBjaGlsZFNldHRpbmdzLmVsZW1lbnRzX3NlbGVjdG9yLnJlcGxhY2UodG9SZXBsYWNlLCByZXBsYWNlV2l0aCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBjaGlsZFNldHRpbmdzLnByZWZpeCA9IGNoaWxkU2V0dGluZ3MucHJlZml4LnJlcGxhY2UodG9SZXBsYWNlLCByZXBsYWNlV2l0aCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBjaGlsZENvbGxlY3Rpb24uZGF0YSgnY29sbGVjdGlvbi1zZXR0aW5ncycsIGNoaWxkU2V0dGluZ3MpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJlcGxhY2VBdHRyRGF0YShlbGVtZW50cywgaW5kZXgsIHRvUmVwbGFjZSwgcmVwbGFjZVdpdGgpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8vIHNhbWUgYXMgYWJvdmUsIGJ1dCB3aWxsIHJlcGxhY2UgZWxlbWVudCBuYW1lcyBhbmQgaW5kZXhlcyBpbiBhbiBodG1sIHN0cmluZyBpbnN0ZWFkXG4gICAgICAgIC8vIG9mIGluIGEgZG9tIGVsZW1lbnQuXG4gICAgICAgIHZhciBjaGFuZ2VIdG1sSW5kZXggPSBmdW5jdGlvbiAoY29sbGVjdGlvbiwgc2V0dGluZ3MsIGh0bWwsIG9sZEluZGV4LCBuZXdJbmRleCwgb2xkS2V5LCBuZXdLZXkpIHtcbiAgICAgICAgICAgIHZhciB0b1JlcGxhY2UgPSBuZXcgUmVnRXhwKHByZWdRdW90ZShzZXR0aW5ncy5uYW1lX3ByZWZpeCArICdbJyArIG9sZEtleSArICddJyksICdnJyk7XG4gICAgICAgICAgICB2YXIgcmVwbGFjZVdpdGggPSBzZXR0aW5ncy5uYW1lX3ByZWZpeCArICdbJyArIG5ld0tleSArICddJztcbiAgICAgICAgICAgIGh0bWwgPSBodG1sLnJlcGxhY2UodG9SZXBsYWNlLCByZXBsYWNlV2l0aCk7XG5cbiAgICAgICAgICAgIHRvUmVwbGFjZSA9IG5ldyBSZWdFeHAocHJlZ1F1b3RlKGNvbGxlY3Rpb24uYXR0cignaWQnKSArICdfJyArIG9sZEluZGV4KSwgJ2cnKTtcbiAgICAgICAgICAgIHJlcGxhY2VXaXRoID0gY29sbGVjdGlvbi5hdHRyKCdpZCcpICsgJ18nICsgbmV3SW5kZXg7XG4gICAgICAgICAgICBodG1sID0gaHRtbC5yZXBsYWNlKHRvUmVwbGFjZSwgcmVwbGFjZVdpdGgpO1xuXG4gICAgICAgICAgICByZXR1cm4gaHRtbDtcbiAgICAgICAgfTtcblxuICAgICAgICAvLyBzb21ldGltZXMsIHNldHRpbmcgYSB2YWx1ZSB3aWxsIG9ubHkgYmUgbWFkZSBpbiBtZW1vcnkgYW5kIG5vdFxuICAgICAgICAvLyBwaHlzaWNhbGx5IGluIHRoZSBkb207IGFuZCB3ZSBuZWVkIHRoZSBmdWxsIGRvbSB3aGVuIHdlIHdhbnRcbiAgICAgICAgLy8gdG8gZHVwbGljYXRlIGEgZmllbGQuXG4gICAgICAgIHZhciBwdXRGaWVsZFZhbHVlc0luRG9tID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgICAgICAgICQoZWxlbWVudCkuZmluZCgnOmlucHV0JykuZWFjaChmdW5jdGlvbiAoaW5kZXgsIGlucHV0T2JqKSB7XG4gICAgICAgICAgICAgICAgcHV0RmllbGRWYWx1ZShpbnB1dE9iaiwgZ2V0RmllbGRWYWx1ZShpbnB1dE9iaiksIHRydWUpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gdGhpcyBtZXRob2QgZG9lcyB0aGUgd2hvbGUgbWFnaWM6IGluIGEgY29sbGVjdGlvbiwgaWYgd2Ugd2FudCB0b1xuICAgICAgICAvLyBtb3ZlIGVsZW1lbnRzIGFuZCBrZWVwIGVsZW1lbnQgcG9zaXRpb25zIGluIHRoZSBiYWNrZW5kLCB3ZSBzaG91bGRcbiAgICAgICAgLy8gZWl0aGVyIG1vdmUgZWxlbWVudCBuYW1lcyBvciBlbGVtZW50IGNvbnRlbnRzLCBidXQgbm90IGJvdGghIHRodXMsXG4gICAgICAgIC8vIGlmIHlvdSBqdXN0IG1vdmUgZWxlbWVudHMgaW4gdGhlIGRvbSwgeW91IGtlZXAgZmllbGQgbmFtZXMgYW5kIGRhdGFcbiAgICAgICAgLy8gYXR0YWNoZWQgYW5kIG5vdGhpbmcgd2lsbCBjaGFuZ2UgaW4gdGhlIGJhY2tlbmQuXG4gICAgICAgIHZhciBzd2FwRWxlbWVudHMgPSBmdW5jdGlvbiAoY29sbGVjdGlvbiwgZWxlbWVudHMsIG9sZEluZGV4LCBuZXdJbmRleCkge1xuXG4gICAgICAgICAgICB2YXIgc2V0dGluZ3MgPSBjb2xsZWN0aW9uLmRhdGEoJ2NvbGxlY3Rpb24tc2V0dGluZ3MnKTtcblxuICAgICAgICAgICAgaWYgKCFzZXR0aW5ncy5wb3NpdGlvbl9maWVsZF9zZWxlY3RvciAmJiAhc2V0dGluZ3MucHJlc2VydmVfbmFtZXMpIHtcbiAgICAgICAgICAgICAgICBjaGFuZ2VFbGVtZW50SW5kZXgoY29sbGVjdGlvbiwgZWxlbWVudHMsIHNldHRpbmdzLCBvbGRJbmRleCwgb2xkSW5kZXgsICdfX3N3YXBfXycpO1xuICAgICAgICAgICAgICAgIGNoYW5nZUVsZW1lbnRJbmRleChjb2xsZWN0aW9uLCBlbGVtZW50cywgc2V0dGluZ3MsIG5ld0luZGV4LCBuZXdJbmRleCwgb2xkSW5kZXgpO1xuICAgICAgICAgICAgICAgIGNoYW5nZUVsZW1lbnRJbmRleChjb2xsZWN0aW9uLCBlbGVtZW50cywgc2V0dGluZ3MsIG9sZEluZGV4LCAnX19zd2FwX18nLCBuZXdJbmRleCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGVsZW1lbnRzLmVxKG9sZEluZGV4KS5pbnNlcnRCZWZvcmUoZWxlbWVudHMuZXEobmV3SW5kZXgpKTtcbiAgICAgICAgICAgIGlmIChuZXdJbmRleCA+IG9sZEluZGV4KSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudHMuZXEobmV3SW5kZXgpLmluc2VydEJlZm9yZShlbGVtZW50cy5lcShvbGRJbmRleCkpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50cy5lcShuZXdJbmRleCkuaW5zZXJ0QWZ0ZXIoZWxlbWVudHMuZXEob2xkSW5kZXgpKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGNvbGxlY3Rpb24uZmluZChzZXR0aW5ncy5lbGVtZW50c19zZWxlY3Rvcik7XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gbW92aW5nIGFuIGVsZW1lbnQgZG93biBvZiAzIHJvd3MgbWVhbnMgaW5jcmVhc2luZyBpdHMgaW5kZXggb2YgMywgYW5kXG4gICAgICAgIC8vIGRlY3JlYXNpbmcgdGhlIDIgb25lcyBiZXR3ZWVuIG9mIDEuIEV4YW1wbGU6IDAtQSAxLUIgMi1DIDMtRDpcbiAgICAgICAgLy8gbW92aW5nIEIgdG8gMyBiZWNvbWVzIDAtQSAxLUMgMi1EIDMtQlxuICAgICAgICB2YXIgc3dhcEVsZW1lbnRzVXAgPSBmdW5jdGlvbiAoY29sbGVjdGlvbiwgZWxlbWVudHMsIHNldHRpbmdzLCBvbGRJbmRleCwgbmV3SW5kZXgpIHtcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSBvbGRJbmRleCArIDE7IChpIDw9IG5ld0luZGV4KTsgaSsrKSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudHMgPSBzd2FwRWxlbWVudHMoY29sbGVjdGlvbiwgZWxlbWVudHMsIGksIGkgLSAxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBjb2xsZWN0aW9uLmZpbmQoc2V0dGluZ3MuZWxlbWVudHNfc2VsZWN0b3IpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8vIG1vdmluZyBhbiBlbGVtZW50IHVwIG9mIDMgcm93cyBtZWFucyBkZWNyZWFzaW5nIGl0cyBpbmRleCBvZiAzLCBhbmRcbiAgICAgICAgLy8gaW5jcmVhc2luZyB0aGUgMiBvbmVzIGJldHdlZW4gb2YgMS4gRXhhbXBsZTogMC1BIDEtQiAyLUMgMy1EOlxuICAgICAgICAvLyBtb3ZpbmcgRCB0byAxIGJlY29tZXMgMC1BIDEtRCAyLUIgMy1DXG4gICAgICAgIHZhciBzd2FwRWxlbWVudHNEb3duID0gZnVuY3Rpb24gKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBzZXR0aW5ncywgb2xkSW5kZXgsIG5ld0luZGV4KSB7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gb2xkSW5kZXggLSAxOyAoaSA+PSBuZXdJbmRleCk7IGktLSkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gc3dhcEVsZW1lbnRzKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBpLCBpICsgMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gY29sbGVjdGlvbi5maW5kKHNldHRpbmdzLmVsZW1lbnRzX3NlbGVjdG9yKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvLyBpZiB3ZSBjcmVhdGUgYW4gZWxlbWVudCBhdCBwb3NpdGlvbiAyLCBhbGwgZWxlbWVudCBpbmRleGVzIGZyb20gMiB0byBOXG4gICAgICAgIC8vIHNob3VsZCBiZSBpbmNyZWFzZWQuIGZvciBleGFtcGxlLCBpbiAwLUEgMS1CIDItQyAzLUQsIGFkZGluZyBYIGF0IHBvc2l0aW9uXG4gICAgICAgIC8vIDEgd2lsbCBjcmVhdGUgMC1BIDEtWCAyLUIgMy1DIDQtRFxuICAgICAgICB2YXIgc2hpZnRFbGVtZW50c1VwID0gZnVuY3Rpb24gKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBzZXR0aW5ncywgaW5kZXgpIHtcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSBpbmRleCArIDE7IGkgPCBlbGVtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gc3dhcEVsZW1lbnRzKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBpIC0gMSwgaSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gY29sbGVjdGlvbi5maW5kKHNldHRpbmdzLmVsZW1lbnRzX3NlbGVjdG9yKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvLyBpZiB3ZSByZW1vdmUgYW4gZWxlbWVudCBhdCBwb3NpdGlvbiAzLCBhbGwgZWxlbWVudCBpbmRleGVzIGZyb20gMyB0byBOXG4gICAgICAgIC8vIHNob3VsZCBiZSBkZWNyZWFzZWQuIGZvciBleGFtcGxlLCBpbiAwLUEgMS1CIDItQyAzLUQsIHJlbW92aW5nIEIgd2lsbCBjcmVhdGVcbiAgICAgICAgLy8gMC1BIDEtQyAyLURcbiAgICAgICAgdmFyIHNoaWZ0RWxlbWVudHNEb3duID0gZnVuY3Rpb24gKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBzZXR0aW5ncywgaW5kZXgpIHtcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSBlbGVtZW50cy5sZW5ndGggLSAyOyBpID4gaW5kZXg7IGktLSkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gc3dhcEVsZW1lbnRzKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBpICsgMSwgaSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gY29sbGVjdGlvbi5maW5kKHNldHRpbmdzLmVsZW1lbnRzX3NlbGVjdG9yKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvLyB0aGlzIG1ldGhvZCBjcmVhdGVzIGJ1dHRvbnMgZm9yIGVhY2ggYWN0aW9uLCBhY2NvcmRpbmcgdG8gYWxsIG9wdGlvbnMgc2V0XG4gICAgICAgIC8vIChidXR0b25zIGVuYWJsZWQsIG1pbmltdW0vbWF4aW11bSBvZiBlbGVtZW50cyBub3QgeWV0IHJlYWNoZWQsIHJlc2N1ZVxuICAgICAgICAvLyBidXR0b24gY3JlYXRpb24gd2hlbiBubyBtb3JlIGVsZW1lbnRzIGFyZSByZW1haW5pbmcuLi4pXG4gICAgICAgIHZhciBkdW1wQ29sbGVjdGlvbkFjdGlvbnMgPSBmdW5jdGlvbiAoY29sbGVjdGlvbiwgc2V0dGluZ3MsIGlzSW5pdGlhbGl6YXRpb24sIGV2ZW50KSB7XG4gICAgICAgICAgICB2YXIgZWxlbWVudHNQYXJlbnQgPSAkKHNldHRpbmdzLmVsZW1lbnRzX3BhcmVudF9zZWxlY3Rvcik7XG4gICAgICAgICAgICB2YXIgaW5pdCA9IGVsZW1lbnRzUGFyZW50LmZpbmQoJy4nICsgc2V0dGluZ3MucHJlZml4ICsgJy10bXAnKS5sZW5ndGggPT09IDA7XG4gICAgICAgICAgICB2YXIgZWxlbWVudHMgPSBjb2xsZWN0aW9uLmZpbmQoc2V0dGluZ3MuZWxlbWVudHNfc2VsZWN0b3IpO1xuXG4gICAgICAgICAgICAvLyBhZGQgYSByZXNjdWUgYnV0dG9uIHRoYXQgd2lsbCBhcHBlYXIgb25seSBpZiBjb2xsZWN0aW9uIGlzIGVtcHRpZWRcbiAgICAgICAgICAgIGlmIChzZXR0aW5ncy5hbGxvd19hZGQpIHtcbiAgICAgICAgICAgICAgICBpZiAoaW5pdCkge1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50c1BhcmVudC5hcHBlbmQoJzxzcGFuIGNsYXNzPVwiJyArIHNldHRpbmdzLnByZWZpeCArICctdG1wXCI+PC9zcGFuPicpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoc2V0dGluZ3MuYWRkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50c1BhcmVudC5hcHBlbmQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJChzZXR0aW5ncy5hZGQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcyhzZXR0aW5ncy5wcmVmaXggKyAnLWFjdGlvbiAnICsgc2V0dGluZ3MucHJlZml4ICsgJy1yZXNjdWUtYWRkJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmRhdGEoJ2NvbGxlY3Rpb24nLCBjb2xsZWN0aW9uLmF0dHIoJ2lkJykpXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBpbml0aWFsaXplcyB0aGUgY29sbGVjdGlvbiB3aXRoIGEgbWluaW1hbCBudW1iZXIgb2YgZWxlbWVudHNcbiAgICAgICAgICAgIGlmIChpc0luaXRpYWxpemF0aW9uKSB7XG4gICAgICAgICAgICAgICAgY29sbGVjdGlvbi5kYXRhKCdjb2xsZWN0aW9uLW9mZnNldCcsIGVsZW1lbnRzLmxlbmd0aCk7XG5cbiAgICAgICAgICAgICAgICB2YXIgY29udGFpbmVyID0gJChzZXR0aW5ncy5jb250YWluZXIpO1xuICAgICAgICAgICAgICAgIHZhciBidXR0b24gPSBjb2xsZWN0aW9uLmZpbmQoJy4nICsgc2V0dGluZ3MucHJlZml4ICsgJy1hZGQsIC4nICsgc2V0dGluZ3MucHJlZml4ICsgJy1yZXNjdWUtYWRkLCAuJyArIHNldHRpbmdzLnByZWZpeCArICctZHVwbGljYXRlJykuZmlyc3QoKTtcbiAgICAgICAgICAgICAgICB2YXIgc2VjdXJlID0gMDtcbiAgICAgICAgICAgICAgICB3aGlsZSAoZWxlbWVudHMubGVuZ3RoIDwgc2V0dGluZ3MuaW5pdF93aXRoX25fZWxlbWVudHMpIHtcbiAgICAgICAgICAgICAgICAgICAgc2VjdXJlKys7XG4gICAgICAgICAgICAgICAgICAgIHZhciBlbGVtZW50ID0gZWxlbWVudHMubGVuZ3RoID4gMCA/IGVsZW1lbnRzLmxhc3QoKSA6IHVuZGVmaW5lZDtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGluZGV4ID0gZWxlbWVudHMubGVuZ3RoIC0gMTtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudHMgPSBkb0FkZChjb250YWluZXIsIGJ1dHRvbiwgY29sbGVjdGlvbiwgc2V0dGluZ3MsIGVsZW1lbnRzLCBlbGVtZW50LCBpbmRleCwgZmFsc2UpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoc2VjdXJlID4gc2V0dGluZ3MuaW5pdF93aXRoX25fZWxlbWVudHMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0luZmluaXRlIGxvb3AsIGVsZW1lbnQgc2VsZWN0b3IgKCcgKyBzZXR0aW5ncy5lbGVtZW50c19zZWxlY3RvciArICcpIG5vdCBmb3VuZCAhJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGNvbGxlY3Rpb24uZGF0YSgnY29sbGVjdGlvbi1vZmZzZXQnLCBlbGVtZW50cy5sZW5ndGgpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBtYWtlIGJ1dHRvbnMgYXBwZWFyL2Rpc2FwcGVhciBpbiBlYWNoIGVsZW1lbnRzIG9mIHRoZSBjb2xsZWN0aW9uIGFjY29yZGluZyB0byBvcHRpb25zXG4gICAgICAgICAgICAvLyAoZW5hYmxlZCwgbWluL21heC4uLikgYW5kIGxvZ2ljIChmb3IgZXhhbXBsZSwgZG8gbm90IHB1dCBhIG1vdmUgdXAgYnV0dG9uIG9uIHRoZSBmaXJzdFxuICAgICAgICAgICAgLy8gZWxlbWVudCBvZiB0aGUgY29sbGVjdGlvbilcbiAgICAgICAgICAgIGVsZW1lbnRzLmVhY2goZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgICAgICAgICAgICAgdmFyIGVsZW1lbnQgPSAkKHRoaXMpO1xuXG4gICAgICAgICAgICAgICAgaWYgKGlzSW5pdGlhbGl6YXRpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5kYXRhKCdpbmRleCcsIGluZGV4KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB2YXIgYWN0aW9ucyA9IGVsZW1lbnQuZmluZCgnLicgKyBzZXR0aW5ncy5wcmVmaXggKyAnLWFjdGlvbnMnKS5hZGRCYWNrKCkuZmlsdGVyKCcuJyArIHNldHRpbmdzLnByZWZpeCArICctYWN0aW9ucycpO1xuICAgICAgICAgICAgICAgIGlmIChhY3Rpb25zLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICBhY3Rpb25zID0gJCgnPCcgKyBzZXR0aW5ncy5hY3Rpb25fY29udGFpbmVyX3RhZyArICcgY2xhc3M9XCInICsgc2V0dGluZ3MucHJlZml4ICsgJy1hY3Rpb25zXCI+PC8nICsgc2V0dGluZ3MuYWN0aW9uX2NvbnRhaW5lcl90YWcgKyAnPicpO1xuXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuYXBwZW5kKGFjdGlvbnMpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHZhciBkZWx0YSA9IDA7XG4gICAgICAgICAgICAgICAgaWYgKGV2ZW50ID09PSAncmVtb3ZlJyAmJiBzZXR0aW5ncy5mYWRlX291dCkge1xuICAgICAgICAgICAgICAgICAgICBkZWx0YSA9IDE7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdmFyIGJ1dHRvbnMgPSBbXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICdlbmFibGVkJzogc2V0dGluZ3MuYWxsb3dfcmVtb3ZlLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ3NlbGVjdG9yJzogc2V0dGluZ3MucHJlZml4ICsgJy1yZW1vdmUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ2h0bWwnOiBzZXR0aW5ncy5yZW1vdmUsXG4gICAgICAgICAgICAgICAgICAgICAgICAnY29uZGl0aW9uJzogZWxlbWVudHMubGVuZ3RoIC0gZGVsdGEgPiBzZXR0aW5ncy5taW5cbiAgICAgICAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgICAgICAgICAgJ2VuYWJsZWQnOiBzZXR0aW5ncy5hbGxvd191cCxcbiAgICAgICAgICAgICAgICAgICAgICAgICdzZWxlY3Rvcic6IHNldHRpbmdzLnByZWZpeCArICctdXAnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ2h0bWwnOiBzZXR0aW5ncy51cCxcbiAgICAgICAgICAgICAgICAgICAgICAgICdjb25kaXRpb24nOiBlbGVtZW50cy5sZW5ndGggLSBkZWx0YSA+IDEgJiYgZWxlbWVudHMuaW5kZXgoZWxlbWVudCkgLSBkZWx0YSA+IDBcbiAgICAgICAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgICAgICAgICAgJ2VuYWJsZWQnOiBzZXR0aW5ncy5hbGxvd19kb3duLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ3NlbGVjdG9yJzogc2V0dGluZ3MucHJlZml4ICsgJy1kb3duJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdodG1sJzogc2V0dGluZ3MuZG93bixcbiAgICAgICAgICAgICAgICAgICAgICAgICdjb25kaXRpb24nOiBlbGVtZW50cy5sZW5ndGggLSBkZWx0YSA+IDEgJiYgZWxlbWVudHMuaW5kZXgoZWxlbWVudCkgIT09IGVsZW1lbnRzLmxlbmd0aCAtIDFcbiAgICAgICAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgICAgICAgICAgJ2VuYWJsZWQnOiBzZXR0aW5ncy5hbGxvd19hZGQgJiYgIXNldHRpbmdzLmFkZF9hdF90aGVfZW5kICYmICFzZXR0aW5ncy5jdXN0b21fYWRkX2xvY2F0aW9uLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ3NlbGVjdG9yJzogc2V0dGluZ3MucHJlZml4ICsgJy1hZGQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ2h0bWwnOiBzZXR0aW5ncy5hZGQsXG4gICAgICAgICAgICAgICAgICAgICAgICAnY29uZGl0aW9uJzogZWxlbWVudHMubGVuZ3RoIC0gZGVsdGEgPCBzZXR0aW5ncy5tYXhcbiAgICAgICAgICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgICAgICAgICAgJ2VuYWJsZWQnOiBzZXR0aW5ncy5hbGxvd19kdXBsaWNhdGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAnc2VsZWN0b3InOiBzZXR0aW5ncy5wcmVmaXggKyAnLWR1cGxpY2F0ZScsXG4gICAgICAgICAgICAgICAgICAgICAgICAnaHRtbCc6IHNldHRpbmdzLmR1cGxpY2F0ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICdjb25kaXRpb24nOiBlbGVtZW50cy5sZW5ndGggLSBkZWx0YSA8IHNldHRpbmdzLm1heFxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgXTtcblxuICAgICAgICAgICAgICAgICQuZWFjaChidXR0b25zLCBmdW5jdGlvbiAoaSwgYnV0dG9uKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChidXR0b24uZW5hYmxlZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGFjdGlvbiA9IGVsZW1lbnQuZmluZCgnLicgKyBidXR0b24uc2VsZWN0b3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFjdGlvbi5sZW5ndGggPT09IDAgJiYgYnV0dG9uLmh0bWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb24gPSAkKGJ1dHRvbi5odG1sKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYXBwZW5kVG8oYWN0aW9ucylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKGJ1dHRvbi5zZWxlY3Rvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoYnV0dG9uLmNvbmRpdGlvbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbi5yZW1vdmVDbGFzcyhzZXR0aW5ncy5wcmVmaXggKyAnLWFjdGlvbi1kaXNhYmxlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZXR0aW5ncy5oaWRlX3VzZWxlc3NfYnV0dG9ucykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb24uY3NzKCdkaXNwbGF5JywgJycpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uLmFkZENsYXNzKHNldHRpbmdzLnByZWZpeCArICctYWN0aW9uLWRpc2FibGVkJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNldHRpbmdzLmhpZGVfdXNlbGVzc19idXR0b25zKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbi5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hZGRDbGFzcyhzZXR0aW5ncy5wcmVmaXggKyAnLWFjdGlvbicpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmRhdGEoJ2NvbGxlY3Rpb24nLCBjb2xsZWN0aW9uLmF0dHIoJ2lkJykpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmRhdGEoJ2VsZW1lbnQnLCBnZXRPckNyZWF0ZUlkKGNvbGxlY3Rpb24uYXR0cignaWQnKSArICdfJyArIGluZGV4LCBlbGVtZW50KSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmZpbmQoJy4nICsgYnV0dG9uLnNlbGVjdG9yKS5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIH0pOyAvLyBlbGVtZW50cy5lYWNoXG5cbiAgICAgICAgICAgIC8vIG1ha2UgdGhlIHJlc2N1ZSBidXR0b24gYXBwZWFyIC8gZGlzYXBwZWFyIGFjY29yZGluZyB0byBvcHRpb25zIChhZGRfYXRfdGhlX2VuZCkgYW5kXG4gICAgICAgICAgICAvLyBsb2dpYyAobm8gbW9yZSBlbGVtZW50cyBvbiB0aGUgY29sbGVjdGlvbilcbiAgICAgICAgICAgIGlmIChzZXR0aW5ncy5hbGxvd19hZGQpIHtcblxuICAgICAgICAgICAgICAgIHZhciBkZWx0YSA9IDA7XG4gICAgICAgICAgICAgICAgaWYgKGV2ZW50ID09PSAncmVtb3ZlJyAmJiBzZXR0aW5ncy5mYWRlX291dCkge1xuICAgICAgICAgICAgICAgICAgICBkZWx0YSA9IDE7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdmFyIHJlc2N1ZUFkZCA9IGNvbGxlY3Rpb24uZmluZCgnLicgKyBzZXR0aW5ncy5wcmVmaXggKyAnLXJlc2N1ZS1hZGQnKS5jc3MoJ2Rpc3BsYXknLCAnJykucmVtb3ZlQ2xhc3Moc2V0dGluZ3MucHJlZml4ICsgJy1hY3Rpb24tZGlzYWJsZWQnKTtcbiAgICAgICAgICAgICAgICB2YXIgYWRkcyA9IGNvbGxlY3Rpb24uZmluZCgnLicgKyBzZXR0aW5ncy5wcmVmaXggKyAnLWFkZCcpO1xuICAgICAgICAgICAgICAgIGlmICghc2V0dGluZ3MuYWRkX2F0X3RoZV9lbmQgJiYgYWRkcy5sZW5ndGggPiBkZWx0YSB8fCBzZXR0aW5ncy5jdXN0b21fYWRkX2xvY2F0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc2N1ZUFkZC5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZXZlbnQgPT09ICdyZW1vdmUnICYmIHNldHRpbmdzLmZhZGVfb3V0KSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc2N1ZUFkZC5jc3MoJ2Rpc3BsYXknLCAnbm9uZScpO1xuICAgICAgICAgICAgICAgICAgICByZXNjdWVBZGQuZmFkZUluKCdmYXN0Jyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmIChlbGVtZW50cy5sZW5ndGggLSBkZWx0YSA+PSBzZXR0aW5ncy5tYXgpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzY3VlQWRkLmFkZENsYXNzKHNldHRpbmdzLnByZWZpeCArICctYWN0aW9uLWRpc2FibGVkJyk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzZXR0aW5ncy5oaWRlX3VzZWxlc3NfYnV0dG9ucykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sbGVjdGlvbi5maW5kKCcuJyArIHNldHRpbmdzLnByZWZpeCArICctYWRkLCAuJyArIHNldHRpbmdzLnByZWZpeCArICctcmVzY3VlLWFkZCwgLicgKyBzZXR0aW5ncy5wcmVmaXggKyAnLWR1cGxpY2F0ZScpLmNzcygnZGlzcGxheScsICdub25lJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfTsgLy8gZHVtcENvbGxlY3Rpb25BY3Rpb25zXG5cbiAgICAgICAgLy8gdGhpcyBwbHVnaW4gc3VwcG9ydHMgbmVzdGVkIGNvbGxlY3Rpb25zLCBhbmQgdGhpcyBtZXRob2QgZW5hYmxlcyB0aGVtIHdoZW4gdGhlXG4gICAgICAgIC8vIHBhcmVudCBjb2xsZWN0aW9uIGlzIGluaXRpYWxpemVkLiBzZWVcbiAgICAgICAgLy8gaHR0cDovL3N5bWZvbnktY29sbGVjdGlvbi5mdXoub3JnL3N5bWZvbnkzL2FkdmFuY2VkL2NvbGxlY3Rpb25PZkNvbGxlY3Rpb25zXG4gICAgICAgIHZhciBlbmFibGVDaGlsZHJlbkNvbGxlY3Rpb25zID0gZnVuY3Rpb24gKGNvbGxlY3Rpb24sIGVsZW1lbnQsIHNldHRpbmdzKSB7XG4gICAgICAgICAgICBpZiAoc2V0dGluZ3MuY2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgICAkLmVhY2goc2V0dGluZ3MuY2hpbGRyZW4sIGZ1bmN0aW9uIChpbmRleCwgY2hpbGRyZW5TZXR0aW5ncykge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWNoaWxkcmVuU2V0dGluZ3Muc2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwianF1ZXJ5LmNvbGxlY3Rpb24uanM6IGdpdmVuIGNvbGxlY3Rpb24gXCIgKyBjb2xsZWN0aW9uLmF0dHIoJ2lkJykgKyBcIiBoYXMgY2hpbGRyZW4gY29sbGVjdGlvbnMsIGJ1dCBjaGlsZHJlbidzIHJvb3Qgc2VsZWN0b3IgaXMgdW5kZWZpbmVkLlwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChlbGVtZW50ICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmZpbmQoY2hpbGRyZW5TZXR0aW5ncy5zZWxlY3RvcikuY29sbGVjdGlvbihjaGlsZHJlblNldHRpbmdzKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbGxlY3Rpb24uZmluZChjaGlsZHJlblNldHRpbmdzLnNlbGVjdG9yKS5jb2xsZWN0aW9uKGNoaWxkcmVuU2V0dGluZ3MpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gdGhpcyBtZXRob2QgaGFuZGxlcyBhIGNsaWNrIG9uIFwiYWRkXCIgYnV0dG9ucywgaXQgaW5jcmVhc2VzIGFsbCBmb2xsb3dpbmcgZWxlbWVudCBpbmRleGVzIG9mXG4gICAgICAgIC8vIDEgcG9zaXRpb24gYW5kIGluc2VydCBhIG5ldyBvbmUgaW4gdGhlIGluZGV4IHRoYXQgYmVjb21lcyBmcmVlLiBpZiBjbGljayBoYXMgYmVlbiBtYWRlIG9uIGFcbiAgICAgICAgLy8gXCJkdXBsaWNhdGVcIiBidXR0b24sIGFsbCBlbGVtZW50IHZhbHVlcyBhcmUgdGhlbiBpbnNlcnRlZC4gZmluYWxseSwgY2FsbGJhY2tzIGxldCB1c2VyIGNhbmNlbFxuICAgICAgICAvLyB0aG9zZSBhY3Rpb25zIGlmIG5lZWRlZC5cbiAgICAgICAgdmFyIGRvQWRkID0gZnVuY3Rpb24gKGNvbnRhaW5lciwgdGhhdCwgY29sbGVjdGlvbiwgc2V0dGluZ3MsIGVsZW1lbnRzLCBlbGVtZW50LCBpbmRleCwgaXNEdXBsaWNhdGUpIHtcbiAgICAgICAgICAgIGlmIChlbGVtZW50cy5sZW5ndGggPCBzZXR0aW5ncy5tYXggJiYgKGlzRHVwbGljYXRlICYmIHRydWVPclVuZGVmaW5lZChzZXR0aW5ncy5iZWZvcmVfZHVwbGljYXRlKGNvbGxlY3Rpb24sIGVsZW1lbnQpKSB8fCB0cnVlT3JVbmRlZmluZWQoc2V0dGluZ3MuYmVmb3JlX2FkZChjb2xsZWN0aW9uLCBlbGVtZW50KSkpKSB7XG4gICAgICAgICAgICAgICAgdmFyIHByb3RvdHlwZSA9IGNvbGxlY3Rpb24uZGF0YSgncHJvdG90eXBlJyk7XG4gICAgICAgICAgICAgICAgdmFyIGZyZWVJbmRleCA9IGNvbGxlY3Rpb24uZGF0YSgnY29sbGVjdGlvbi1vZmZzZXQnKTtcblxuICAgICAgICAgICAgICAgIGNvbGxlY3Rpb24uZGF0YSgnY29sbGVjdGlvbi1vZmZzZXQnLCBmcmVlSW5kZXggKyAxKTtcblxuICAgICAgICAgICAgICAgIGlmIChpbmRleCA9PT0gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgaW5kZXggPSBlbGVtZW50cy5sZW5ndGggLSAxO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB2YXIgcmVnZXhwID0gbmV3IFJlZ0V4cChwcmVnUXVvdGUoc2V0dGluZ3MucHJvdG90eXBlX25hbWUpLCAnZycpO1xuICAgICAgICAgICAgICAgIHZhciBmcmVlS2V5ID0gZnJlZUluZGV4O1xuXG4gICAgICAgICAgICAgICAgaWYgKHNldHRpbmdzLnByZXNlcnZlX25hbWVzKSB7XG4gICAgICAgICAgICAgICAgICAgIGZyZWVLZXkgPSBjb2xsZWN0aW9uLmRhdGEoJ2NvbGxlY3Rpb24tZnJlZS1rZXknKTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAoZnJlZUtleSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmcmVlS2V5ID0gZmluZEZyZWVOdW1lcmljS2V5KHNldHRpbmdzLCBlbGVtZW50cyk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBjb2xsZWN0aW9uLmRhdGEoJ2NvbGxlY3Rpb24tZnJlZS1rZXknLCBmcmVlS2V5ICsgMSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdmFyIGNvZGUgPSAkKHByb3RvdHlwZS5yZXBsYWNlKHJlZ2V4cCwgZnJlZUtleSkpLmRhdGEoJ2luZGV4JywgZnJlZUluZGV4KTtcbiAgICAgICAgICAgICAgICBzZXRSaWdodFByZWZpeChzZXR0aW5ncywgY29kZSk7XG5cbiAgICAgICAgICAgICAgICB2YXIgZWxlbWVudHNQYXJlbnQgPSAkKHNldHRpbmdzLmVsZW1lbnRzX3BhcmVudF9zZWxlY3Rvcik7XG4gICAgICAgICAgICAgICAgdmFyIHRtcCA9IGVsZW1lbnRzUGFyZW50LmZpbmQoJz4gLicgKyBzZXR0aW5ncy5wcmVmaXggKyAnLXRtcCcpO1xuICAgICAgICAgICAgICAgIHZhciBpZCA9ICQoY29kZSkuZmluZCgnW2lkXScpLmZpcnN0KCkuYXR0cignaWQnKTtcblxuICAgICAgICAgICAgICAgIGlmIChpc0R1cGxpY2F0ZSkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgb2xkRWxlbWVudCA9IGVsZW1lbnRzLmVxKGluZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgcHV0RmllbGRWYWx1ZXNJbkRvbShvbGRFbGVtZW50KTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIG9sZEh0bWwgPSAkKFwiPGRpdi8+XCIpLmFwcGVuZChvbGRFbGVtZW50LmNsb25lKCkpLmh0bWwoKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIG9sZEluZGV4ID0gc2V0dGluZ3MucHJlc2VydmVfbmFtZXMgfHwgc2V0dGluZ3MucG9zaXRpb25fZmllbGRfc2VsZWN0b3IgPyBvbGRFbGVtZW50LmRhdGEoJ2luZGV4JykgOiBpbmRleDtcbiAgICAgICAgICAgICAgICAgICAgdmFyIG9sZEtleSA9IHNldHRpbmdzLnByZXNlcnZlX25hbWVzID8gZ2V0RWxlbWVudEtleShzZXR0aW5ncywgb2xkRWxlbWVudCkgOiBvbGRJbmRleDtcbiAgICAgICAgICAgICAgICAgICAgdmFyIG5ld0h0bWwgPSBjaGFuZ2VIdG1sSW5kZXgoY29sbGVjdGlvbiwgc2V0dGluZ3MsIG9sZEh0bWwsIG9sZEluZGV4LCBmcmVlSW5kZXgsIG9sZEtleSwgZnJlZUtleSk7XG5cbiAgICAgICAgICAgICAgICAgICAgY29kZSA9ICQoJzxkaXYvPicpLmh0bWwobmV3SHRtbCkuY29udGVudHMoKS5kYXRhKCdpbmRleCcsIGZyZWVJbmRleCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzZXR0aW5ncy5mYWRlX2luKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb2RlLmhpZGUoKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB0bXAuYmVmb3JlKGNvZGUpLmZpbmQoc2V0dGluZ3MucHJlZml4ICsgJy1hY3Rpb25zJykucmVtb3ZlKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNldHRpbmdzLmZhZGVfaW4pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGUuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgdG1wLmJlZm9yZShjb2RlKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBlbGVtZW50cyA9IGNvbGxlY3Rpb24uZmluZChzZXR0aW5ncy5lbGVtZW50c19zZWxlY3Rvcik7XG5cbiAgICAgICAgICAgICAgICB2YXIgYWN0aW9uID0gY29kZS5maW5kKCcuJyArIHNldHRpbmdzLnByZWZpeCArICctYWRkLCAuJyArIHNldHRpbmdzLnByZWZpeCArICctZHVwbGljYXRlJyk7XG4gICAgICAgICAgICAgICAgaWYgKGFjdGlvbi5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbi5hZGRDbGFzcyhzZXR0aW5ncy5wcmVmaXggKyAnLWFjdGlvbicpLmRhdGEoJ2NvbGxlY3Rpb24nLCBjb2xsZWN0aW9uLmF0dHIoJ2lkJykpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICghc2V0dGluZ3MuYWRkX2F0X3RoZV9lbmQgJiYgaW5kZXggKyAxICE9PSBmcmVlSW5kZXgpIHtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudHMgPSBkb01vdmUoY29sbGVjdGlvbiwgc2V0dGluZ3MsIGVsZW1lbnRzLCBjb2RlLCBmcmVlSW5kZXgsIGluZGV4ICsgMSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgZHVtcENvbGxlY3Rpb25BY3Rpb25zKGNvbGxlY3Rpb24sIHNldHRpbmdzLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgZW5hYmxlQ2hpbGRyZW5Db2xsZWN0aW9ucyhjb2xsZWN0aW9uLCBjb2RlLCBzZXR0aW5ncyk7XG5cbiAgICAgICAgICAgICAgICBpZiAoKGlzRHVwbGljYXRlICYmICF0cnVlT3JVbmRlZmluZWQoc2V0dGluZ3MuYWZ0ZXJfZHVwbGljYXRlKGNvbGxlY3Rpb24sIGNvZGUpKSkgfHwgIXRydWVPclVuZGVmaW5lZChzZXR0aW5ncy5hZnRlcl9hZGQoY29sbGVjdGlvbiwgY29kZSkpKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChpbmRleCAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gc2hpZnRFbGVtZW50c1VwKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBzZXR0aW5ncywgaW5kZXggKyAxKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjb2RlLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKGNvZGUgIT09IHVuZGVmaW5lZCAmJiBzZXR0aW5ncy5mYWRlX2luKSB7XG4gICAgICAgICAgICAgICAgY29kZS5mYWRlSW4oJ2Zhc3QnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzZXR0aW5ncy5wb3NpdGlvbl9maWVsZF9zZWxlY3Rvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgZG9SZXdyaXRlUG9zaXRpb25zKHNldHRpbmdzLCBlbGVtZW50cyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaWYgKHNldHRpbmdzLnBvc2l0aW9uX2ZpZWxkX3NlbGVjdG9yKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBkb1Jld3JpdGVQb3NpdGlvbnMoc2V0dGluZ3MsIGVsZW1lbnRzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBlbGVtZW50cztcbiAgICAgICAgfTtcblxuICAgICAgICAvLyByZW1vdmVzIHRoZSBjdXJyZW50IGVsZW1lbnQgd2hlbiBjbGlja2luZyBvbiBhIFwiZGVsZXRlXCIgYnV0dG9uIGFuZCBkZWNyZWFzZSBhbGwgZm9sbG93aW5nXG4gICAgICAgIC8vIGluZGV4ZXMgZnJvbSAxIHBvc2l0aW9uLlxuICAgICAgICB2YXIgZG9EZWxldGUgPSBmdW5jdGlvbiAoY29sbGVjdGlvbiwgc2V0dGluZ3MsIGVsZW1lbnRzLCBlbGVtZW50LCBpbmRleCkge1xuICAgICAgICAgICAgaWYgKGVsZW1lbnRzLmxlbmd0aCA+IHNldHRpbmdzLm1pbiAmJiB0cnVlT3JVbmRlZmluZWQoc2V0dGluZ3MuYmVmb3JlX3JlbW92ZShjb2xsZWN0aW9uLCBlbGVtZW50KSkpIHtcbiAgICAgICAgICAgICAgICB2YXIgZGVsZXRpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciB0b0RlbGV0ZSA9IGVsZW1lbnQ7XG4gICAgICAgICAgICAgICAgICAgIGlmICghc2V0dGluZ3MucHJlc2VydmVfbmFtZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gc2hpZnRFbGVtZW50c1VwKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBzZXR0aW5ncywgaW5kZXgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9EZWxldGUgPSBlbGVtZW50cy5sYXN0KCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdmFyIGJhY2t1cCA9IHRvRGVsZXRlLmNsb25lKHt3aXRoRGF0YUFuZEV2ZW50czogdHJ1ZX0pLnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgdG9EZWxldGUucmVtb3ZlKCk7XG4gICAgICAgICAgICAgICAgICAgIGlmICghdHJ1ZU9yVW5kZWZpbmVkKHNldHRpbmdzLmFmdGVyX3JlbW92ZShjb2xsZWN0aW9uLCBiYWNrdXApKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGVsZW1lbnRzUGFyZW50ID0gJChzZXR0aW5ncy5lbGVtZW50c19wYXJlbnRfc2VsZWN0b3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudHNQYXJlbnQuZmluZCgnPiAuJyArIHNldHRpbmdzLnByZWZpeCArICctdG1wJykuYmVmb3JlKGJhY2t1cCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50cyA9IGNvbGxlY3Rpb24uZmluZChzZXR0aW5ncy5lbGVtZW50c19zZWxlY3Rvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50cyA9IHNoaWZ0RWxlbWVudHNEb3duKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBzZXR0aW5ncywgaW5kZXggLSAxKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZiAoc2V0dGluZ3MucG9zaXRpb25fZmllbGRfc2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvUmV3cml0ZVBvc2l0aW9ucyhzZXR0aW5ncywgZWxlbWVudHMpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBpZiAoc2V0dGluZ3MuZmFkZV9vdXQpIHtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudC5mYWRlT3V0KCdmYXN0JywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGVsZXRpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgZGVsZXRpb24oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBlbGVtZW50cztcbiAgICAgICAgfTtcblxuICAgICAgICAvLyByZXZlcnNlIGN1cnJlbnQgZWxlbWVudCBhbmQgdGhlIHByZXZpb3VzIG9uZSAoc28gdGhlIGN1cnJlbnQgZWxlbWVudFxuICAgICAgICAvLyBhcHBlYXJzIG9uZSBwbGFjZSBoaWdoZXIpXG4gICAgICAgIHZhciBkb1VwID0gZnVuY3Rpb24gKGNvbGxlY3Rpb24sIHNldHRpbmdzLCBlbGVtZW50cywgZWxlbWVudCwgaW5kZXgpIHtcbiAgICAgICAgICAgIGlmIChpbmRleCAhPT0gMCAmJiB0cnVlT3JVbmRlZmluZWQoc2V0dGluZ3MuYmVmb3JlX3VwKGNvbGxlY3Rpb24sIGVsZW1lbnQpKSkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gc3dhcEVsZW1lbnRzKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBpbmRleCwgaW5kZXggLSAxKTtcbiAgICAgICAgICAgICAgICBpZiAoIXRydWVPclVuZGVmaW5lZChzZXR0aW5ncy5hZnRlcl91cChjb2xsZWN0aW9uLCBlbGVtZW50KSkpIHtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudHMgPSBzd2FwRWxlbWVudHMoY29sbGVjdGlvbiwgZWxlbWVudHMsIGluZGV4IC0gMSwgaW5kZXgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHNldHRpbmdzLnBvc2l0aW9uX2ZpZWxkX3NlbGVjdG9yKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGRvUmV3cml0ZVBvc2l0aW9ucyhzZXR0aW5ncywgZWxlbWVudHMpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gZWxlbWVudHM7XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gcmV2ZXJzZSB0aGUgY3VycmVudCBlbGVtZW50IGFuZCB0aGUgbmV4dCBvbmUgKHNvIHRoZSBjdXJyZW50IGVsZW1lbnRcbiAgICAgICAgLy8gYXBwZWFycyBvbmUgcGxhY2UgbG93ZXIpXG4gICAgICAgIHZhciBkb0Rvd24gPSBmdW5jdGlvbiAoY29sbGVjdGlvbiwgc2V0dGluZ3MsIGVsZW1lbnRzLCBlbGVtZW50LCBpbmRleCkge1xuICAgICAgICAgICAgaWYgKGluZGV4ICE9PSAoZWxlbWVudHMubGVuZ3RoIC0gMSkgJiYgdHJ1ZU9yVW5kZWZpbmVkKHNldHRpbmdzLmJlZm9yZV9kb3duKGNvbGxlY3Rpb24sIGVsZW1lbnQpKSkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gc3dhcEVsZW1lbnRzKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBpbmRleCwgaW5kZXggKyAxKTtcbiAgICAgICAgICAgICAgICBpZiAoIXRydWVPclVuZGVmaW5lZChzZXR0aW5ncy5hZnRlcl9kb3duKGNvbGxlY3Rpb24sIGVsZW1lbnRzKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudHMgPSBzd2FwRWxlbWVudHMoY29sbGVjdGlvbiwgZWxlbWVudHMsIGluZGV4ICsgMSwgaW5kZXgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHNldHRpbmdzLnBvc2l0aW9uX2ZpZWxkX3NlbGVjdG9yKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGRvUmV3cml0ZVBvc2l0aW9ucyhzZXR0aW5ncywgZWxlbWVudHMpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gZWxlbWVudHM7XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gbW92ZSBhbiBlbGVtZW50IGZyb20gYSBwb3NpdGlvbiB0byBhbiBhcmJpdHJhcnkgbmV3IHBvc2l0aW9uXG4gICAgICAgIHZhciBkb01vdmUgPSBmdW5jdGlvbiAoY29sbGVjdGlvbiwgc2V0dGluZ3MsIGVsZW1lbnRzLCBlbGVtZW50LCBvbGRJbmRleCwgbmV3SW5kZXgpIHtcbiAgICAgICAgICAgIGlmICgxID09PSBNYXRoLmFicyhuZXdJbmRleCAtIG9sZEluZGV4KSkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gc3dhcEVsZW1lbnRzKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBvbGRJbmRleCwgbmV3SW5kZXgpO1xuICAgICAgICAgICAgICAgIGlmICghKG5ld0luZGV4IC0gb2xkSW5kZXggPiAwID8gdHJ1ZU9yVW5kZWZpbmVkKHNldHRpbmdzLmFmdGVyX3VwKGNvbGxlY3Rpb24sIGVsZW1lbnQpKSA6IHRydWVPclVuZGVmaW5lZChzZXR0aW5ncy5hZnRlcl9kb3duKGNvbGxlY3Rpb24sIGVsZW1lbnQpKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudHMgPSBzd2FwRWxlbWVudHMoY29sbGVjdGlvbiwgZWxlbWVudHMsIG5ld0luZGV4LCBvbGRJbmRleCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBpZiAob2xkSW5kZXggPCBuZXdJbmRleCkge1xuICAgICAgICAgICAgICAgICAgICBlbGVtZW50cyA9IHN3YXBFbGVtZW50c1VwKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBzZXR0aW5ncywgb2xkSW5kZXgsIG5ld0luZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEobmV3SW5kZXggLSBvbGRJbmRleCA+IDAgPyB0cnVlT3JVbmRlZmluZWQoc2V0dGluZ3MuYWZ0ZXJfdXAoY29sbGVjdGlvbiwgZWxlbWVudCkpIDogdHJ1ZU9yVW5kZWZpbmVkKHNldHRpbmdzLmFmdGVyX2Rvd24oY29sbGVjdGlvbiwgZWxlbWVudCkpKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudHMgPSBzd2FwRWxlbWVudHNEb3duKGNvbGxlY3Rpb24sIGVsZW1lbnRzLCBzZXR0aW5ncywgbmV3SW5kZXgsIG9sZEluZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gc3dhcEVsZW1lbnRzRG93bihjb2xsZWN0aW9uLCBlbGVtZW50cywgc2V0dGluZ3MsIG9sZEluZGV4LCBuZXdJbmRleCk7XG4gICAgICAgICAgICAgICAgICAgIGlmICghKG5ld0luZGV4IC0gb2xkSW5kZXggPiAwID8gdHJ1ZU9yVW5kZWZpbmVkKHNldHRpbmdzLmFmdGVyX3VwKGNvbGxlY3Rpb24sIGVsZW1lbnQpKSA6IHRydWVPclVuZGVmaW5lZChzZXR0aW5ncy5hZnRlcl9kb3duKGNvbGxlY3Rpb24sIGVsZW1lbnQpKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gc3dhcEVsZW1lbnRzVXAoY29sbGVjdGlvbiwgZWxlbWVudHMsIHNldHRpbmdzLCBuZXdJbmRleCwgb2xkSW5kZXgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZHVtcENvbGxlY3Rpb25BY3Rpb25zKGNvbGxlY3Rpb24sIHNldHRpbmdzLCBmYWxzZSk7XG5cbiAgICAgICAgICAgIGlmIChzZXR0aW5ncy5wb3NpdGlvbl9maWVsZF9zZWxlY3Rvcikge1xuICAgICAgICAgICAgICAgIHJldHVybiBkb1Jld3JpdGVQb3NpdGlvbnMoc2V0dGluZ3MsIGVsZW1lbnRzKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGVsZW1lbnRzO1xuICAgICAgICB9O1xuXG4gICAgICAgIHZhciBkb1Jld3JpdGVQb3NpdGlvbnMgPSBmdW5jdGlvbiAoc2V0dGluZ3MsIGVsZW1lbnRzKSB7XG4gICAgICAgICAgICAkKGVsZW1lbnRzKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB2YXIgZWxlbWVudCA9ICQodGhpcyk7XG4gICAgICAgICAgICAgICAgcHV0RmllbGRWYWx1ZShlbGVtZW50LmZpbmQoc2V0dGluZ3MucG9zaXRpb25fZmllbGRfc2VsZWN0b3IpLCBlbGVtZW50cy5pbmRleChlbGVtZW50KSk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgcmV0dXJuIGVsZW1lbnRzO1xuICAgICAgICB9O1xuXG4gICAgICAgIHZhciBnZXRFbGVtZW50S2V5ID0gZnVuY3Rpb24gKHNldHRpbmdzLCBlbGVtZW50KSB7XG4gICAgICAgICAgICB2YXIgbmFtZSA9IGVsZW1lbnQuZmluZCgnOmlucHV0W25hbWVePVwiJyArIHNldHRpbmdzLm5hbWVfcHJlZml4ICsgJ1tcIl0nKS5hdHRyKCduYW1lJyk7XG5cbiAgICAgICAgICAgIHJldHVybiBuYW1lLnNsaWNlKHNldHRpbmdzLm5hbWVfcHJlZml4Lmxlbmd0aCArIDEpLnNwbGl0KCddJywgMSlbMF07XG4gICAgICAgIH07XG5cbiAgICAgICAgdmFyIGZpbmRGcmVlTnVtZXJpY0tleSA9IGZ1bmN0aW9uIChzZXR0aW5ncywgZWxlbWVudHMpIHtcbiAgICAgICAgICAgIHZhciBmcmVlS2V5ID0gMDtcblxuICAgICAgICAgICAgZWxlbWVudHMuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIGtleSA9IGdldEVsZW1lbnRLZXkoc2V0dGluZ3MsICQodGhpcykpO1xuXG4gICAgICAgICAgICAgICAgaWYgKC9eMHxbMS05XVxcZCokLy50ZXN0KGtleSkgJiYga2V5ID49IGZyZWVLZXkpIHtcbiAgICAgICAgICAgICAgICAgICAgZnJlZUtleSA9IE51bWJlcihrZXkpICsgMTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgcmV0dXJuIGZyZWVLZXk7XG4gICAgICAgIH07XG5cbiAgICAgICAgdmFyIHNldFJpZ2h0UHJlZml4ID0gZnVuY3Rpb24gKHNldHRpbmdzLCBjb250YWluZXIpIHtcbiAgICAgICAgICAgIHZhciBzdWZmaXhlcyA9IFtcbiAgICAgICAgICAgICAgICAnLWFjdGlvbicsXG4gICAgICAgICAgICAgICAgJy1hY3Rpb24tZGlzYWJsZWQnLFxuICAgICAgICAgICAgICAgICctYWN0aW9ucycsXG4gICAgICAgICAgICAgICAgJy1hZGQnLFxuICAgICAgICAgICAgICAgICctZG93bicsXG4gICAgICAgICAgICAgICAgJy1kdXBsaWNhdGUnLFxuICAgICAgICAgICAgICAgICctcmVtb3ZlJyxcbiAgICAgICAgICAgICAgICAnLXJlc2N1ZS1hZGQnLFxuICAgICAgICAgICAgICAgICctdG1wJyxcbiAgICAgICAgICAgICAgICAnLXVwJ1xuICAgICAgICAgICAgXTtcblxuICAgICAgICAgICAgJC5lYWNoKHN1ZmZpeGVzLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIHN1ZmZpeCA9IHRoaXM7XG4gICAgICAgICAgICAgICAgY29udGFpbmVyLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgdGhhdCA9ICQodGhpcyk7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGF0Lmhhc0NsYXNzKHNldHRpbmdzLnVzZXJfcHJlZml4ICsgc3VmZml4KSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC5hZGRDbGFzcyhzZXR0aW5ncy5wcmVmaXggKyBzdWZmaXgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRoYXQuZmluZCgnKicpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGhlcmUgPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGhlcmUuaGFzQ2xhc3Moc2V0dGluZ3MudXNlcl9wcmVmaXggKyBzdWZmaXgpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVyZS5hZGRDbGFzcyhzZXR0aW5ncy5wcmVmaXggKyBzdWZmaXgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8vIHdlJ3JlIGluIGEgJC5mbi4sIHNvIGluICQoJy5jb2xsZWN0aW9uJykuY29sbGVjdGlvbigpLCAkKHRoaXMpIGVxdWFscyAkKCcuY29sbGVjdGlvbicpXG4gICAgICAgIHZhciBlbGVtcyA9ICQodGhpcyk7XG5cbiAgICAgICAgLy8gYXQgbGVhc3Qgb25lLCBidXQgd2h5IG5vdCBzZXZlcmFsIGNvbGxlY3Rpb25zIHNob3VsZCBiZSByYWlzZWRcbiAgICAgICAgaWYgKGVsZW1zLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJqcXVlcnkuY29sbGVjdGlvbi5qczogZ2l2ZW4gY29sbGVjdGlvbiBzZWxlY3RvciBkb2VzIG5vdCBleGlzdC5cIik7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICBlbGVtcy5lYWNoKGZ1bmN0aW9uICgpIHtcblxuICAgICAgICAgICAgdmFyIHNldHRpbmdzID0gJC5leHRlbmQodHJ1ZSwge30sIGRlZmF1bHRzLCBvcHRpb25zKTtcblxuICAgICAgICAgICAgLy8gdXNhZ2Ugb2YgJC5mbi5vbiBldmVudHMgdXNpbmcgYSBzdGF0aWMgY29udGFpbmVyIGp1c3QgaW4gY2FzZSB0aGVyZSB3b3VsZCBiZSBzb21lXG4gICAgICAgICAgICAvLyBhamF4IGludGVyYWN0aW9ucyBpbnNpZGUgdGhlIGNvbGxlY3Rpb25cbiAgICAgICAgICAgIGlmICgkKHNldHRpbmdzLmNvbnRhaW5lcikubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJqcXVlcnkuY29sbGVjdGlvbi5qczogYSBjb250YWluZXIgc2hvdWxkIGV4aXN0IHRvIGhhbmRsZSBldmVudHMgKGJhc2ljYWxseSwgYSA8Ym9keT4gdGFnKS5cIik7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBpdCBpcyBwb3NzaWJsZSB0byB1c2UgdGhpcyBwbHVnaW4gd2l0aCBhIHNlbGVjdG9yIHRoYXQgd2lsbCBjb250YWluIHRoZSBjb2xsZWN0aW9uIGlkXG4gICAgICAgICAgICAvLyBpbiBhIGRhdGEgYXR0cmlidXRlXG4gICAgICAgICAgICB2YXIgZWxlbSA9ICQodGhpcyk7XG4gICAgICAgICAgICBpZiAoZWxlbS5kYXRhKCdjb2xsZWN0aW9uJykgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHZhciBjb2xsZWN0aW9uID0gJCgnIycgKyBlbGVtLmRhdGEoJ2NvbGxlY3Rpb24nKSk7XG4gICAgICAgICAgICAgICAgaWYgKGNvbGxlY3Rpb24ubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwianF1ZXJ5LmNvbGxlY3Rpb24uanM6IGdpdmVuIGNvbGxlY3Rpb24gaWQgZG9lcyBub3QgZXhpc3QuXCIpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGNvbGxlY3Rpb24gPSBlbGVtO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29sbGVjdGlvbiA9ICQoY29sbGVjdGlvbik7XG5cbiAgICAgICAgICAgIC8vIHdoZW4gYWRkaW5nIGVsZW1lbnRzIHRvIGEgY29sbGVjdGlvbiwgd2Ugc2hvdWxkIGJlIGF3YXJlIG9mIHRoZSBub2RlIHRoYXQgd2lsbCBjb250YWluIHRoZW1cbiAgICAgICAgICAgIHNldHRpbmdzLmVsZW1lbnRzX3BhcmVudF9zZWxlY3RvciA9IHNldHRpbmdzLmVsZW1lbnRzX3BhcmVudF9zZWxlY3Rvci5yZXBsYWNlKCclaWQlJywgJyMnICsgZ2V0T3JDcmVhdGVJZCgnJywgY29sbGVjdGlvbikpO1xuICAgICAgICAgICAgaWYgKCFzZXR0aW5ncy5lbGVtZW50c19wYXJlbnRfc2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5lbGVtZW50c19wYXJlbnRfc2VsZWN0b3IgPSAnIycgKyBnZXRPckNyZWF0ZUlkKCcnLCBjb2xsZWN0aW9uKTtcbiAgICAgICAgICAgICAgICBpZiAoJChzZXR0aW5ncy5lbGVtZW50c19wYXJlbnRfc2VsZWN0b3IpLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImpxdWVyeS5jb2xsZWN0aW9uLmpzOiBnaXZlbiBlbGVtZW50cyBwYXJlbnQgc2VsZWN0b3IgZG9lcyBub3QgcmV0dXJuIGFueSBvYmplY3QuXCIpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIE9uIG5lc3RlZCBjb2xsZWN0aW9ucywgcHJlZml4IGlzIHRoZSBzYW1lIGZvciBhbGwgY2hpbGRyZW4gbGVhZGluZyB0byB2ZXJ5XG4gICAgICAgICAgICAvLyByYW5kb20gYW5kIHVuZXhlcGN0ZWQgaXNzdWVzLCBzbyB3ZSBtZXJnZSBwcmVmaXggd2l0aCBjdXJyZW50IGNvbGxlY3Rpb24gaWQuXG4gICAgICAgICAgICBzZXR0aW5ncy51c2VyX3ByZWZpeCA9IHNldHRpbmdzLnByZWZpeDtcbiAgICAgICAgICAgIHNldHRpbmdzLnByZWZpeCA9IGNvbGxlY3Rpb24uYXR0cignaWQnKSArICctJyArIHNldHRpbmdzLnVzZXJfcHJlZml4O1xuICAgICAgICAgICAgc2V0UmlnaHRQcmVmaXgoc2V0dGluZ3MsIGNvbGxlY3Rpb24pO1xuXG4gICAgICAgICAgICAvLyBlbmZvcmNpbmcgbG9naWMgYmV0d2VlbiBvcHRpb25zXG4gICAgICAgICAgICBpZiAoIXNldHRpbmdzLmFsbG93X2FkZCkge1xuICAgICAgICAgICAgICAgIHNldHRpbmdzLmFsbG93X2R1cGxpY2F0ZSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIHNldHRpbmdzLmFkZF9hdF90aGVfZW5kID0gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoc2V0dGluZ3MuaW5pdF93aXRoX25fZWxlbWVudHMgPiBzZXR0aW5ncy5tYXgpIHtcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5pbml0X3dpdGhfbl9lbGVtZW50cyA9IHNldHRpbmdzLm1heDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChzZXR0aW5ncy5taW4gJiYgKCFzZXR0aW5ncy5pbml0X3dpdGhfbl9lbGVtZW50cyB8fCBzZXR0aW5ncy5pbml0X3dpdGhfbl9lbGVtZW50cyA8IHNldHRpbmdzLm1pbikpIHtcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5pbml0X3dpdGhfbl9lbGVtZW50cyA9IHNldHRpbmdzLm1pbjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCFzZXR0aW5ncy5hY3Rpb25fY29udGFpbmVyX3RhZykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwianF1ZXJ5LmNvbGxlY3Rpb24uanM6IGFjdGlvbl9jb250YWluZXJfdGFnIG5lZWRzIHRvIGJlIHNldC5cIik7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIHVzZXIgY2FsbGJhY2tcbiAgICAgICAgICAgIHNldHRpbmdzLmJlZm9yZV9pbml0KGNvbGxlY3Rpb24pO1xuXG4gICAgICAgICAgICAvLyBwcm90b3R5cGUgcmVxdWlyZWQgdG8gY3JlYXRlIG5ldyBlbGVtZW50cyBpbiB0aGUgY29sbGVjdGlvblxuICAgICAgICAgICAgaWYgKGNvbGxlY3Rpb24uZGF0YSgncHJvdG90eXBlJykgPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImpxdWVyeS5jb2xsZWN0aW9uLmpzOiBnaXZlbiBjb2xsZWN0aW9uIGZpZWxkIGhhcyBubyBwcm90b3R5cGUsIGNoZWNrIHRoYXQgeW91ciBmaWVsZCBoYXMgdGhlIHByb3RvdHlwZSBvcHRpb24gc2V0IHRvIHRydWUuXCIpO1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBhbGwgdGhlIGZvbGxvd2luZyBkYXRhIGF0dHJpYnV0ZXMgYXJlIGF1dG9tYXRpY2FsbHkgYXZhaWxhYmxlIHRoYW5rcyB0b1xuICAgICAgICAgICAgLy8ganF1ZXJ5LmNvbGxlY3Rpb24uaHRtbC50d2lnIGZvcm0gdGhlbWVcbiAgICAgICAgICAgIGlmIChjb2xsZWN0aW9uLmRhdGEoJ3Byb3RvdHlwZS1uYW1lJykgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHNldHRpbmdzLnByb3RvdHlwZV9uYW1lID0gY29sbGVjdGlvbi5kYXRhKCdwcm90b3R5cGUtbmFtZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNvbGxlY3Rpb24uZGF0YSgnYWxsb3ctYWRkJykgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHNldHRpbmdzLmFsbG93X2FkZCA9IGNvbGxlY3Rpb24uZGF0YSgnYWxsb3ctYWRkJyk7XG4gICAgICAgICAgICAgICAgc2V0dGluZ3MuYWxsb3dfZHVwbGljYXRlID0gY29sbGVjdGlvbi5kYXRhKCdhbGxvdy1hZGQnKSA/IHNldHRpbmdzLmFsbG93X2R1cGxpY2F0ZSA6IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNvbGxlY3Rpb24uZGF0YSgnYWxsb3ctcmVtb3ZlJykgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHNldHRpbmdzLmFsbG93X3JlbW92ZSA9IGNvbGxlY3Rpb24uZGF0YSgnYWxsb3ctcmVtb3ZlJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoY29sbGVjdGlvbi5kYXRhKCduYW1lLXByZWZpeCcpICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5uYW1lX3ByZWZpeCA9IGNvbGxlY3Rpb24uZGF0YSgnbmFtZS1wcmVmaXgnKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gcHJvdG90eXBlLW5hbWUgcmVxdWlyZWQgZm9yIG5lc3RlZCBjb2xsZWN0aW9ucywgd2hlcmUgY29sbGVjdGlvbiBpZCBwcmVmaXhcbiAgICAgICAgICAgIC8vIGlzbid0IGd1ZXNzYWJsZSAoc2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9zeW1mb255L3N5bWZvbnkvaXNzdWVzLzEzODM3KVxuICAgICAgICAgICAgaWYgKCFzZXR0aW5ncy5uYW1lX3ByZWZpeCkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwianF1ZXJ5LmNvbGxlY3Rpb24uanM6IHRoZSBwcmVmaXggdXNlZCBpbiBkZXNjZW5kYW50IGZpZWxkIG5hbWVzIGlzIG1hbmRhdG9yeSwgeW91IGNhbiBzZXQgaXQgdXNpbmcgMiB3YXlzOlwiKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImpxdWVyeS5jb2xsZWN0aW9uLmpzOiAtIHVzZSB0aGUgZm9ybSB0aGVtZSBnaXZlbiB3aXRoIHRoaXMgcGx1Z2luIHNvdXJjZVwiKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImpxdWVyeS5jb2xsZWN0aW9uLmpzOiAtIHNldCBuYW1lX3ByZWZpeCBvcHRpb24gdG8gICd7eyBmb3JtVmlldy5teUNvbGxlY3Rpb25GaWVsZC52YXJzLmZ1bGxfbmFtZSB9fSdcIik7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIGlmIHByZXNlcnZlX25hbWVzIG9wdGlvbiBpcyBzZXQsIHdlIHNob3VsZCBlbmZvcmNlIG1hbnkgb3B0aW9ucyB0byBhdm9pZFxuICAgICAgICAgICAgLy8gaGF2aW5nIGluY29uc2lzdGVuY2llcyBiZXR3ZWVuIHRoZSBVSSBhbmQgdGhlIFN5bWZvbnkgcmVzdWx0XG4gICAgICAgICAgICBpZiAoc2V0dGluZ3MucHJlc2VydmVfbmFtZXMpIHtcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5hbGxvd191cCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIHNldHRpbmdzLmFsbG93X2Rvd24gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5kcmFnX2Ryb3AgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5hZGRfYXRfdGhlX2VuZCA9IHRydWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIGRyYWcgJiBkcm9wIHN1cHBvcnQ6IHRoaXMgaXMgYSBiaXQgbW9yZSBjb21wbGV4IHRoYW4gcHJlc3NpbmcgXCJ1cFwiIG9yXG4gICAgICAgICAgICAvLyBcImRvd25cIiBidXR0b25zIGJlY2F1c2Ugd2UgY2FuIG1vdmUgZWxlbWVudHMgbW9yZSB0aGFuIG9uZSBwbGFjZSBhaGVhZFxuICAgICAgICAgICAgLy8gb3IgYmVsb3cuLi5cbiAgICAgICAgICAgIGlmICgodHlwZW9mIGpRdWVyeS51aSAhPT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIGpRdWVyeS51aS5zb3J0YWJsZSAhPT0gJ3VuZGVmaW5lZCcpXG4gICAgICAgICAgICAgICAgJiYgY29sbGVjdGlvbi5kYXRhKCdzb3J0YWJsZScpKSB7XG4gICAgICAgICAgICAgICAgY29sbGVjdGlvbi5zb3J0YWJsZSgnZGlzYWJsZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHNldHRpbmdzLmRyYWdfZHJvcCAmJiBzZXR0aW5ncy5hbGxvd191cCAmJiBzZXR0aW5ncy5hbGxvd19kb3duKSB7XG4gICAgICAgICAgICAgICAgdmFyIG9sZFBvc2l0aW9uO1xuICAgICAgICAgICAgICAgIHZhciBuZXdQb3NpdGlvbjtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGpRdWVyeS51aSA9PT0gJ3VuZGVmaW5lZCcgfHwgdHlwZW9mIGpRdWVyeS51aS5zb3J0YWJsZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3MuZHJhZ19kcm9wID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgY29sbGVjdGlvbi5zb3J0YWJsZSgkLmV4dGVuZCh0cnVlLCB7fSwge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhcnQ6IGZ1bmN0aW9uIChldmVudCwgdWkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgZWxlbWVudHMgPSBjb2xsZWN0aW9uLmZpbmQoc2V0dGluZ3MuZWxlbWVudHNfc2VsZWN0b3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlbGVtZW50ID0gdWkuaXRlbTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdGhhdCA9ICQodGhpcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCF0cnVlT3JVbmRlZmluZWQoc2V0dGluZ3MuZHJhZ19kcm9wX3N0YXJ0KGV2ZW50LCB1aSwgZWxlbWVudHMsIGVsZW1lbnQpKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGF0LnNvcnRhYmxlKFwiY2FuY2VsXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVpLnBsYWNlaG9sZGVyLmhlaWdodCh1aS5pdGVtLmhlaWdodCgpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1aS5wbGFjZWhvbGRlci53aWR0aCh1aS5pdGVtLndpZHRoKCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9sZFBvc2l0aW9uID0gZWxlbWVudHMuaW5kZXgoZWxlbWVudCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgdXBkYXRlOiBmdW5jdGlvbiAoZXZlbnQsIHVpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGVsZW1lbnRzID0gY29sbGVjdGlvbi5maW5kKHNldHRpbmdzLmVsZW1lbnRzX3NlbGVjdG9yKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgZWxlbWVudCA9IHVpLml0ZW07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRoYXQgPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuc29ydGFibGUoXCJjYW5jZWxcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZhbHNlID09PSBzZXR0aW5ncy5kcmFnX2Ryb3BfdXBkYXRlKGV2ZW50LCB1aSwgZWxlbWVudHMsIGVsZW1lbnQpIHx8ICEobmV3UG9zaXRpb24gLSBvbGRQb3NpdGlvbiA+IDAgPyB0cnVlT3JVbmRlZmluZWQoc2V0dGluZ3MuYmVmb3JlX3VwKGNvbGxlY3Rpb24sIGVsZW1lbnQpKSA6IHRydWVPclVuZGVmaW5lZChzZXR0aW5ncy5iZWZvcmVfZG93bihjb2xsZWN0aW9uLCBlbGVtZW50KSkpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV3UG9zaXRpb24gPSBlbGVtZW50cy5pbmRleChlbGVtZW50KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50cyA9IGNvbGxlY3Rpb24uZmluZChzZXR0aW5ncy5lbGVtZW50c19zZWxlY3Rvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9Nb3ZlKGNvbGxlY3Rpb24sIHNldHRpbmdzLCBlbGVtZW50cywgZWxlbWVudCwgb2xkUG9zaXRpb24sIG5ld1Bvc2l0aW9uKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSwgc2V0dGluZ3MuZHJhZ19kcm9wX29wdGlvbnMpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGNvbGxlY3Rpb24uZGF0YSgnY29sbGVjdGlvbi1zZXR0aW5ncycsIHNldHRpbmdzKTtcblxuICAgICAgICAgICAgLy8gZXZlbnRzIG9uIGJ1dHRvbnMgdXNpbmcgYSBcInN0YXRpY1wiIGNvbnRhaW5lciBzbyBldmVuIG5ld2x5XG4gICAgICAgICAgICAvLyBjcmVhdGVkL2FqYXggZG93bmxvYWRlZCBidXR0b25zIGRvZXNuJ3QgbmVlZCBmdXJ0aGVyIGluaXRpYWxpemF0aW9uXG4gICAgICAgICAgICB2YXIgY29udGFpbmVyID0gJChzZXR0aW5ncy5jb250YWluZXIpO1xuICAgICAgICAgICAgY29udGFpbmVyXG4gICAgICAgICAgICAgICAgLm9mZignY2xpY2snLCAnLicgKyBzZXR0aW5ncy5wcmVmaXggKyAnLWFjdGlvbicpXG4gICAgICAgICAgICAgICAgLm9uKCdjbGljaycsICcuJyArIHNldHRpbmdzLnByZWZpeCArICctYWN0aW9uJywgZnVuY3Rpb24gKGUpIHtcblxuICAgICAgICAgICAgICAgICAgICB2YXIgdGhhdCA9ICQodGhpcyk7XG5cbiAgICAgICAgICAgICAgICAgICAgdmFyIGNvbGxlY3Rpb24gPSAkKCcjJyArIHRoYXQuZGF0YSgnY29sbGVjdGlvbicpKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHNldHRpbmdzID0gY29sbGVjdGlvbi5kYXRhKCdjb2xsZWN0aW9uLXNldHRpbmdzJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHVuZGVmaW5lZCA9PT0gc2V0dGluZ3MpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjb2xsZWN0aW9uID0gJCgnIycgKyB0aGF0LmRhdGEoJ2NvbGxlY3Rpb24nKSkuZmluZCgnLicgKyB0aGF0LmRhdGEoJ2NvbGxlY3Rpb24nKSArICctY29sbGVjdGlvbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHNldHRpbmdzID0gY29sbGVjdGlvbi5kYXRhKCdjb2xsZWN0aW9uLXNldHRpbmdzJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodW5kZWZpbmVkID09PSBzZXR0aW5ncykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IFwiQ2FuJ3QgZmluZCBjb2xsZWN0aW9uOiBcIiArIHRoYXQuZGF0YSgnY29sbGVjdGlvbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgdmFyIGVsZW1lbnRzID0gY29sbGVjdGlvbi5maW5kKHNldHRpbmdzLmVsZW1lbnRzX3NlbGVjdG9yKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVsZW1lbnQgPSB0aGF0LmRhdGEoJ2VsZW1lbnQnKSA/ICQoJyMnICsgdGhhdC5kYXRhKCdlbGVtZW50JykpIDogdW5kZWZpbmVkO1xuICAgICAgICAgICAgICAgICAgICB2YXIgaW5kZXggPSBlbGVtZW50ICYmIGVsZW1lbnQubGVuZ3RoID8gZWxlbWVudHMuaW5kZXgoZWxlbWVudCkgOiAtMTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGV2ZW50ID0gbnVsbDtcblxuICAgICAgICAgICAgICAgICAgICB2YXIgaXNEdXBsaWNhdGUgPSB0aGF0LmlzKCcuJyArIHNldHRpbmdzLnByZWZpeCArICctZHVwbGljYXRlJyk7XG4gICAgICAgICAgICAgICAgICAgIGlmICgodGhhdC5pcygnLicgKyBzZXR0aW5ncy5wcmVmaXggKyAnLWFkZCcpIHx8IHRoYXQuaXMoJy4nICsgc2V0dGluZ3MucHJlZml4ICsgJy1yZXNjdWUtYWRkJykgfHwgaXNEdXBsaWNhdGUpICYmIHNldHRpbmdzLmFsbG93X2FkZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnQgPSAnYWRkJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gZG9BZGQoY29udGFpbmVyLCB0aGF0LCBjb2xsZWN0aW9uLCBzZXR0aW5ncywgZWxlbWVudHMsIGVsZW1lbnQsIGluZGV4LCBpc0R1cGxpY2F0ZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpZiAodGhhdC5pcygnLicgKyBzZXR0aW5ncy5wcmVmaXggKyAnLXJlbW92ZScpICYmIHNldHRpbmdzLmFsbG93X3JlbW92ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnQgPSAncmVtb3ZlJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gZG9EZWxldGUoY29sbGVjdGlvbiwgc2V0dGluZ3MsIGVsZW1lbnRzLCBlbGVtZW50LCBpbmRleCk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpZiAodGhhdC5pcygnLicgKyBzZXR0aW5ncy5wcmVmaXggKyAnLXVwJykgJiYgc2V0dGluZ3MuYWxsb3dfdXApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50ID0gJ3VwJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRzID0gZG9VcChjb2xsZWN0aW9uLCBzZXR0aW5ncywgZWxlbWVudHMsIGVsZW1lbnQsIGluZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGF0LmlzKCcuJyArIHNldHRpbmdzLnByZWZpeCArICctZG93bicpICYmIHNldHRpbmdzLmFsbG93X2Rvd24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50ID0gJ2Rvd24nO1xuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudHMgPSBkb0Rvd24oY29sbGVjdGlvbiwgc2V0dGluZ3MsIGVsZW1lbnRzLCBlbGVtZW50LCBpbmRleCk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBkdW1wQ29sbGVjdGlvbkFjdGlvbnMoY29sbGVjdGlvbiwgc2V0dGluZ3MsIGZhbHNlLCBldmVudCk7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICB9KTsgLy8gLm9uXG5cbiAgICAgICAgICAgIGR1bXBDb2xsZWN0aW9uQWN0aW9ucyhjb2xsZWN0aW9uLCBzZXR0aW5ncywgdHJ1ZSk7XG4gICAgICAgICAgICBlbmFibGVDaGlsZHJlbkNvbGxlY3Rpb25zKGNvbGxlY3Rpb24sIG51bGwsIHNldHRpbmdzKTtcblxuICAgICAgICAgICAgLy8gaWYgY29sbGVjdGlvbiBlbGVtZW50cyBhcmUgZ2l2ZW4gaW4gdGhlIHdyb25nIG9yZGVyLCBwbHVnaW5cbiAgICAgICAgICAgIC8vIG11c3QgcmVvcmRlciB0aGVtIGdyYXBoaWNhbGx5XG4gICAgICAgICAgICBpZiAoc2V0dGluZ3MucG9zaXRpb25fZmllbGRfc2VsZWN0b3IpIHtcbiAgICAgICAgICAgICAgICB2YXIgYXJyYXkgPSBbXTtcbiAgICAgICAgICAgICAgICB2YXIgZWxlbWVudHMgPSBjb2xsZWN0aW9uLmZpbmQoc2V0dGluZ3MuZWxlbWVudHNfc2VsZWN0b3IpO1xuICAgICAgICAgICAgICAgIGVsZW1lbnRzLmVhY2goZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciB0aGF0ID0gJCh0aGlzKTtcbiAgICAgICAgICAgICAgICAgICAgYXJyYXkucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcGFyc2VGbG9hdChnZXRGaWVsZFZhbHVlKHRoYXQuZmluZChzZXR0aW5ncy5wb3NpdGlvbl9maWVsZF9zZWxlY3RvcikpKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnQ6IHRoYXRcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICB2YXIgc29ydGVyID0gZnVuY3Rpb24gKGEsIGIpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChhLnBvc2l0aW9uIDwgYi5wb3NpdGlvbiA/IC0xIDogKGEucG9zaXRpb24gPiBiLnBvc2l0aW9uID8gMSA6IDApKTtcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIGFycmF5LnNvcnQoc29ydGVyKTtcblxuICAgICAgICAgICAgICAgICQuZWFjaChhcnJheSwgZnVuY3Rpb24gKG5ld0luZGV4LCBvYmplY3QpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGlkcyA9IFtdO1xuICAgICAgICAgICAgICAgICAgICAkKGVsZW1lbnRzKS5lYWNoKGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWRzLnB1c2goJCh0aGlzKS5hdHRyKCdpZCcpKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgdmFyIGVsZW1lbnQgPSBvYmplY3QuZWxlbWVudDtcbiAgICAgICAgICAgICAgICAgICAgdmFyIG9sZEluZGV4ID0gJC5pbkFycmF5KGVsZW1lbnQuYXR0cignaWQnKSwgaWRzKTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAobmV3SW5kZXggIT09IG9sZEluZGV4KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50cyA9IGRvTW92ZShjb2xsZWN0aW9uLCBzZXR0aW5ncywgZWxlbWVudHMsIGVsZW1lbnQsIG9sZEluZGV4LCBuZXdJbmRleCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBwdXRGaWVsZFZhbHVlKGVsZW1lbnQuZmluZChzZXR0aW5ncy5wb3NpdGlvbl9maWVsZF9zZWxlY3RvciksIGVsZW1lbnRzLmluZGV4KGVsZW1lbnQpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSAvLyBpZiAoc2V0dGluZ3MucG9zaXRpb25fZmllbGRfc2VsZWN0b3IpIHtcblxuICAgICAgICAgICAgc2V0dGluZ3MuYWZ0ZXJfaW5pdChjb2xsZWN0aW9uKTtcblxuICAgICAgICB9KTsgLy8gZWxlbS5lYWNoXG5cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfTsgLy8gJC5mbi5jb2xsZWN0aW9uXG5cbn0pXG4oalF1ZXJ5KTtcbiJdLCJzb3VyY2VSb290IjoiIn0=