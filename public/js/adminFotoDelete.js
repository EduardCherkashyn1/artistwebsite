$(document).ready(function() {
    $('.foto-delete').on('click', function(e) {
        e.preventDefault();
        itemId = $(this).attr('id');
        var $link = $(e.currentTarget);
        $.ajax({
            method: 'POST',
            data: {'entityId':itemId},
            url: $link.attr('href'),
            beforeSend:function(){
                return confirm("Do you want to delete this foto?");
            },
        }).done(function () {
            $(e.currentTarget).css('display','none');
        })

    });
});

$(document).ready(function() {
    $('.linkDelete').on('click', function(e) {
        e.preventDefault();
        itemId = $(this).attr('id');
        var $link = $(e.currentTarget);
        $.ajax({
            method: 'POST',
            data: {'entityId':itemId},
            url: $link.attr('href'),
            beforeSend:function(){
                return confirm("Do you want to delete this link?");
            },
        }).done(function () {
            location.reload()
        });

        });
});

