1. Clone the repo to your computer

git clone https://EduardCherkashyn1@bitbucket.org/EduardCherkashyn1/artistwebsite.git


1. Run composer install to install required dependencies

composer install


1. Adjust .env line 16


1. Create a schema in your database

php bin/console doctrine:database:create


1. Create tables in your schema

php bin/console doctrine:migrations:migrate


1. Load fixtures to your database

php bin/console doctrine:fixtures:load

1. Start server

php bin/console server:run 0.0.0.0:8000





# Docker

```
docker-compose build
docker-compose up -d
docker-compose exec best_artists_php sh
```

inside container

for dev

.env APP_ENV=dev

```
composer install
bin/console doctrine:migrations:migrate -n
bin/console doctrine:fixtures:load -n
```

for prod

.env APP_ENV=prod

```
composer install --no-dev
bin/console doctrine:migrations:migrate -n
bin/console doctrine:fixtures:load -n
```

Project is up on port 1521
